@extends('layouts.app')
@section("header")
    <h1>Reports
        <small>Package Reports</small>
    </h1>
@endsection
@push('styles')
@endpush
@section("content")
<div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Package Title</th>
                  <th>Client Name</th>
                  <th>Total Price</th>
                  <th>Usage Percentage</th>
                  <th>Total Package Words</th>
                  <th>Words Consumed</th>
                  <th>Expiery Date</th>
                </tr>
                </thead>
                <tbody>
                  @foreach($packages as $package)
                  <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$package->package_title}}</td>
                    <td>{{$package->client->fullname}}</td>
                    <td>$ {{$package->package_price}}</td>
                    <td>{{$package->GetUsagePercentage()}} %</td>
                    <td>{{$package->word_count}} </td>
                    <td>{{$package->GetWordsConsumed()}} </td>
                    <td>{{$package->expire_date}} </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
        <!-- /.col -->
      </div>
@endsection
@push('scripts')
    <script>
         $(function () {
            $('#example1').DataTable()
            // $('#example2').DataTable({
            //     'paging'      : true,
            //     'ordering'    : true,
            //     'info'        : true,
            //     'buttons': [
            //     'copy', 'excel', 'pdf'
            // ],
            // 'dom': 'Bfrtip',

            // })

            $('#example2').DataTable({
              dom: 'Bfrtip',
              buttons: [
                   'excel', 'pdf',
              ],
        
          });
        })
    </script>
@endpush
