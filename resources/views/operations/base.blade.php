@extends('operations.layouts.master')
@section('title','Admin Area')
@push('styles')

@endpush

@section('content')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12 text-right" style="margin-bottom:12px">
        <a type="button" class="btn btn-success btn-sm" style="color: white;" href="./client-add.html">Add New
            Client</a>

        <a type="button" class="btn btn-success btn-sm" style="color: white;" href="./project-add.html">Add New
            Project</a>
    </div>
</div>

<!--Begin::Section-->
<div class="row">
    <div class="col-xl-12">
        <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
            <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Incoming Requests
                    </h3>
                </div>

            </div>
            <div class="kt-portlet__body">

                <!--begin: Datatable -->
                <div class="kt-datatable" id="incoming_requests"></div>
                <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
                    <thead>
                        <tr>
                            <th>Project ID</th>
                            <!-- <th>Project Name</th> -->
                            <th>Source Language</th>
                            <th>Target Language</th>
                            <th>Service Type</th>
                            <th>Client Name</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>#</td>
                        </tr>
                    </tbody>
                </table>
                <!--end: Datatable -->
            </div>
        </div>
    </div>

</div>

@endsection

@push('scripts')
<script>
var KTDatatablesDataSourceHtml = function() {

    var initTable1 = function() {
        var table = $('#kt_table_1');

        // begin first table
        table.DataTable({
            responsive: true,

        });

    };

    return {

        //main function to initiate the module
        init: function() {
            initTable1();
        },

    };

}();

jQuery(document).ready(function() {
    KTDatatablesDataSourceHtml.init();


});
</script>


@endpush
