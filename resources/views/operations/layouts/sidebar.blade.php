<div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_aside_menu_wrapper">
					<div id="kt_aside_menu" class="kt-aside-menu " data-ktmenu-vertical="1" data-ktmenu-scroll="1"
						data-ktmenu-dropdown-timeout="500">
						<ul class="kt-menu__nav ">
							<li class="kt-menu__item  kt-menu__item--active" aria-haspopup="true"><a href="{{route('clients.index')}}"
									class="kt-menu__link "><span class="kt-menu__link-icon">
										<i class="fas fa-home"></i>
									</span><span class="kt-menu__link-text">Home</span></a></li>
							<li class="kt-menu__section ">
								<h4 class="kt-menu__section-text">Main</h4>
								<i class="kt-menu__section-icon flaticon-more-v2"></i>
							</li>
							<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true"
								data-ktmenu-submenu-toggle="hover">
								<a href="{{route('projects.index')}}" class="kt-menu__link kt-menu__toggle">
									<span class="kt-menu__link-icon">
										<i class="fas fa-th-large"></i>
									</span>
									<span class="kt-menu__link-text">Projects</span></a>

							</li>

                            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true"
                                data-ktmenu-submenu-toggle="hover"><a href="javascript:;"
                                    class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-icon">
                                        <i class="fas fa-exchange-alt"></i>
                                    @php
                                        $incoming_requests_count = \App\ProjectRequest::whereHas('notes',function($q){
                                                  $q->where('assigned_to',\Auth::guard('manager')->user()->id);
                                            })->count();
                                        $outgoing_requests_count = \App\ProjectRequest::where('assigned_by',\Auth::guard('manager')->user()->id)->count();
                                    @endphp
                                    </span><span class="kt-menu__link-text">Requests</span><i
                                        class="kt-menu__ver-arrow la la-angle-right"></i></a>
                                <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                                    <ul class="kt-menu__subnav">
                                        <li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true"><span
                                                class="kt-menu__link"><span
                                                    class="kt-menu__link-text">Requests</span></span></li>
                                        <li class="kt-menu__item " aria-haspopup="true"><a
                                                href="{{route('operations.incoming_requests')}}" class="kt-menu__link "><i
                                                    class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                                    class="kt-menu__link-text">Incoming Requests</span> <span class="trans-number-req">{{$incoming_requests_count}}</span></a></li>
                                        <li class="kt-menu__item " aria-haspopup="true"><a
                                                href="{{route('operations.outgoing_requests')}}" class="kt-menu__link "><i
                                                    class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                                    class="kt-menu__link-text">Outgoing Requests</span> <span class="trans-number-req">{{$outgoing_requests_count}}</span></a></li>
                                    </ul>
                                </div>
                            </li>
							<!-- <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true"
								data-ktmenu-submenu-toggle="hover">
								<a href="./inbox.html" class="kt-menu__link kt-menu__toggle">
									<span class="kt-menu__link-icon">
										<i class="fas fa-inbox"></i>
									</span>
									<span class="kt-menu__link-text">Inbox</span></a>

							</li> -->
							<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true"
								data-ktmenu-submenu-toggle="hover">
								<a href="{{route('clients.index')}}" class="kt-menu__link kt-menu__toggle">
									<span class="kt-menu__link-icon">
										<i class="fas fa-users"></i>
									</span>
									<span class="kt-menu__link-text">Clients</span></a>

							</li>
							<!-- <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true"
								data-ktmenu-submenu-toggle="hover">
								<a href="./projects.html" class="kt-menu__link kt-menu__toggle">
									<span class="kt-menu__link-icon">
										<i class="fas fa-pause-circle"></i>
									</span>
									<span class="kt-menu__link-text">Trainings</span></a>

							</li>
							<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
								<a href="./projects.html" class="kt-menu__link kt-menu__toggle">
								<span class="kt-menu__link-icon">
									<i class="fas fa-file"></i>
							    </span>
							    <span class="kt-menu__link-text">Reports</span></a>

							</li> -->
						</ul>
					</div>
				</div>
