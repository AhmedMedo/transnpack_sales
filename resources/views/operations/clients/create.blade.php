@extends('operations.layouts.master')
@section('title','Admin Area')
@push('styles')

@endpush

@section('content')

<div class="kt-portlet">
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                Create Client
            </h3>
        </div>
    </div>

    <!--begin::Form-->
    <form class="kt-form kt-form--label-right" id="kt_form_1" method="POST" action="{{route('clients.store')}}">
        @csrf
        <div class="kt-portlet__body">
            <div class="kt-form__content">
                <div class="kt-alert m-alert--icon alert alert-danger kt-hidden" role="alert" id="kt_form_1_msg">
                    <div class="kt-alert__icon">
                        <i class="la la-warning"></i>
                    </div>
                    <div class="kt-alert__text">
                        Oh snap! Change a few things up and try submitting again.
                    </div>
                    <div class="kt-alert__close">
                        <button type="button" class="close" data-close="alert" aria-label="Close">
                        </button>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-3 col-sm-12">Client name*</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <div class="kt-typeahead">
                        <input name="name"  class="form-control" type="text" placeholder="Client name">
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-3 col-sm-12">Agent Email*</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <div class="kt-typeahead">
                        <input name="agent_email"  class="form-control" type="text" placeholder="Agent name">
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-3 col-sm-12">Agent name*</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <div class="kt-typeahead">
                        <input name="agent_name"  class="form-control" type="text" placeholder="Agent name">
                        <span class="cursor--pointer" style="text-decoration: underline;"
                            onclick="addmoreWithEmail('agent','Agent name','Agent Email')">Add more +</span>
                    </div>
                </div>
            </div>
            <div id="agent_container" class="appended-section display--none"></div>

            <div class="form-group row">
                <label class="col-form-label col-lg-3 col-sm-12">LSP *</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <select  class="form-control kt-select2" id="kt_select2" name="lsp">
                        <option value="1">Yes</option>
                        <option value="0">No</option>
                    </select>
                    <span class="form-text text-muted">Select an option</span>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-3 col-sm-12">Country*</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <select name="country_id" class="form-control kt-select2" id="kt_select2">
                        @foreach($countries as $country)
                        <option value="{{$country->id}}">{{$country->name}}</option>
                        @endforeach
                    </select>
                    <span class="form-text text-muted">Select an option</span>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-form-label col-lg-3 col-sm-12">Address Line 1</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <div class="kt-typeahead">
                        <textarea name="address_1"  class="form-control" type="text" placeholder="Address Line 1"></textarea>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-3 col-sm-12">Address Line 2</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <div class="kt-typeahead">
                        <textarea name="address_2"  class="form-control" type="text" placeholder="Address Line 2"></textarea>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-3 col-sm-12">Zip Code</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <div class="kt-typeahead">
                        <input name="zip_code"  class="form-control" type="text" placeholder="Zip code">
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-3 col-sm-12">Time Zone</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <select name="timezone" class="form-control kt-select2" id="kt_select2">
                        @foreach($timezones as $timezone)
                        <option value="{{$timezone}}">{{$timezone}}</option>

                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-3 col-sm-12">Email*</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <div class="kt-typeahead">
                        <input name="client_email"  class="form-control" type="text" placeholder="Email">
                        <span class="cursor--pointer" style="text-decoration: underline;"
                            onclick="addmore('email','Email')">Add more +</span>
                    </div>
                </div>
            </div>
            <div id="email_container" class="appended-section display--none"></div>

            <div class="form-group row">
                <label class="col-form-label col-lg-3 col-sm-12">Phone number</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <div class="kt-typeahead">
                        <input name="client_mobile"  class="form-control" type="text" placeholder="Phone number">
                        <span class="cursor--pointer" style="text-decoration: underline;"
                            onclick="addmore('mobile','Phone number')">Add more +</span>
                    </div>

                </div>
            </div>
            <div id="mobile_container" class="appended-section display--none">

            </div>

            <div class="form-group row">
                <label class="col-form-label col-lg-3 col-sm-12">Fax number</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <div class="kt-typeahead">
                        <input  name="client_fax" class="form-control" type="text" placeholder="Fax number">
                        <span class="cursor--pointer" style="text-decoration: underline;"
                            onclick="addmore('fax','Fax number')">Add more +</span>
                    </div>

                </div>
            </div>
            <div id="fax_container" class="appended-section display--none">

            </div>

        </div>
        <div class="kt-portlet__foot">
            <div class=" ">
                <div class="row">
                    <div class="col-lg-9 ml-lg-auto">
                        <button type="submit" class="btn btn-success">Save</button>
                        <button type="reset" class="btn btn-secondary">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <!--end::Form-->
</div>


@endsection

@push('scripts')
<script src="{{asset('assets/plugins/custom/fullcalendar/fullcalendar.bundle.js')}}" type="text/javascript"></script>

<!--end::Page Vendors -->

<!--begin::Page Scripts(used by this page) -->
<script src="{{asset('assets/js/pages/crud/forms/widgets/bootstrap-datepicker.js')}}" type="text/javascript"></script>

<script>
function addmore(ref, title) {

    $(`#${ref}_container`).append(
        `<div class="form-group row">
                            <label class="col-form-label col-lg-3 col-sm-12">${title}</label>
                            <div class="col-lg-4 col-md-9 col-sm-12">
                                <div class="kt-typeahead">
                                    <input  class="form-control" name="${ref}[]" type="text"  placeholder="${title}">
                                </div>

                            </div>
                        </div>
    `
    )
    if ($(`#${ref}_container`).children().length > 0) {
        $(`#${ref}_container`).removeClass('display--none');
    }
}


function addmoreWithEmail(ref, title,email_title) {
    var i=1;
    $(`#${ref}_container`).append(
        `<div class="form-group row">
                        <div class="col-lg-6 col-lg-offset-4 col-md-12 col-sm-12">
                            <div class="kt-typeahead">
                                <input  class="form-control" type="text" name="agents[${i}][name]" placeholder="${title}">
                            </div>

                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12">
                            <div class="kt-typeahead">
                                <input  class="form-control" type="email" name="agents[${i}][email]"  placeholder="${email_title}">
                            </div>

                        </div>
                    </div>
`
    )
    i++;
    if ($(`#${ref}_container`).children().length > 0) {
        $(`#${ref}_container`).removeClass('display--none');
    }
}

function addMoreData() {
    $("#extra-data").append(`
        <hr>
        <div class="form-group row">
                                    <label class="col-form-label col-lg-3 col-sm-12">Email*</label>
                                    <div class="col-lg-4 col-md-9 col-sm-12">
                                        <div class="kt-typeahead">
                                            <input class="form-control" type="text"  placeholder="Email">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-form-label col-lg-3 col-sm-12">Phone number</label>
                                    <div class="col-lg-4 col-md-9 col-sm-12">
                                        <div class="kt-typeahead">
                                            <input class="form-control" type="text"  placeholder="Phone number">
                                        </div>
                                    </div>
                                </div>
        `)
}
</script>
<script>
$('#kt_sweetalert_trans').click(function(e) {
    swal.fire("Success!", "The New Client has been added successfully", "success")
        .then(result => {
            if (result.value) {
                window.location.replace('clients.html')
            }
        })
});
</script>


@endpush
