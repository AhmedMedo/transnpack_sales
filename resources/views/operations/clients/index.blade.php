@extends('operations.layouts.master')
@section('title','Admin Area')
@push('styles')

@endpush

@section('content')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12 text-right" style="margin-bottom:12px">
        <a type="button" class="btn btn-success btn-sm" style="color: white;" href="{{route('clients.create')}}">Add New
            Client</a>
    </div>
</div>
<div class="row">
    <div class="col">
        <div class="alert alert-light alert-elevate fade show" role="alert">
            <div class="alert-text">
                <div class="kt-portlet__head-label">
                    <h5 class="kt-portlet__head-title">
                        Find Client
                    </h5>
                </div>
                <form method="GET" action="{{route('clients.index')}}">
                    @csrf
                    <div class="row">
                        <div class="col-md-3 col-sm-3 col-xs-12">
                            <div class="form-group">
                                <label>Client ID</label>
                                <input name="client_id" type="text" class="form-control" placeholder="Client ID">
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-12">
                            <div class="form-group">
                                <label>Client Name</label>
                                <input name="name" type="text" class="form-control" placeholder="Client Name">
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-12">
                            <div class="form-group row">
                                <label>From Date</label>
                                <input name="from_date" type="text" class="form-control" id="kt_datepicker_1" readonly
                                    placeholder="Select date" />
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-12">
                            <label>To Date</label>
                            <input name="to_date" type="text" class="form-control" id="kt_datepicker_1" readonly
                                placeholder="Select date" />
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 text-right">
                            <button type="submit" class="btn btn-primary btn-sm">Filter</button>
                        </div>
                        @if(request()->input())
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <button onclick="location.href='{{route('clients.index')}}'" type="button"
                                class="btn btn-danger btn-sm">Reset</button>
                        </div>
                        @endif
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>
<!--Begin::Section-->
<div class="row">
    <div class="col-xl-12">
        <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
            <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Clients
                    </h3>
                </div>

            </div>
            <div class="kt-portlet__body">

                <!--begin: Datatable -->
                <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
                    <thead>
                        <tr>
                            <th>Record ID</th>
                            <th>Client ID</th>
                            <th>Client Name</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($clients as $client)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$client->clientID}}</td>
                            <td>{{$client->name}}</td>

                            <td>
                                <div class="dropdown">
                                    <a href="javascript:;" class="btn btn-sm btn-clean btn-icon btn-icon-md"
                                        data-toggle="dropdown">
                                        <i class="flaticon-more-1"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <ul class="kt-nav">
                                            <li class="kt-nav__item">
                                                <a href="#" class="kt-nav__link">
                                                    <i class="kt-nav__link-icon flaticon2-expand"></i>
                                                    <span class="kt-nav__link-text">View</span>
                                                </a>
                                            </li>
                                            <li class="kt-nav__item">
                                                <a href="{{route('clients.edit',$client->id)}}" class="kt-nav__link">
                                                    <i class="kt-nav__link-icon flaticon2-contract"></i>
                                                    <span class="kt-nav__link-text">Edit</span>
                                                </a>
                                            </li>
                                            <li class="kt-nav__item">
                                                <a href="#" onclick='$("#Delete_client_{{$client->id}}").submit();return false;' class="kt-nav__link">
                                                    <form id="Delete_client_{{$client->id}}" action="{{route('clients.destroy',$client->id) }}"
                                                        method="post">
                                                        <!-- <input class="btn btn-default" type="submit" value="Delete" /> -->
                                                        <input type="hidden" name="_method" value="delete" />
                                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                        <i class="kt-nav__link-icon flaticon2-trash"></i>
                                                        <span class="kt-nav__link-text">Delete</span>

                                                    </form>

                                                </a>
                                            </li>

                                        </ul>
                                    </div>
                                </div>

                            </td>
                        </tr>


                        @endforeach
                    </tbody>
                </table>
                <!--end: Datatable -->
            </div>
        </div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12 text-right" style="margin-bottom:12px">
        <button type="button" class="btn btn-primary btn-sm">view Selected </button>
    </div>
</div>


@endsection

@push('scripts')
<script src="{{asset('assets/plugins/custom/fullcalendar/fullcalendar.bundle.js')}}" type="text/javascript"></script>

<!--end::Page Vendors -->

<!--begin::Page Scripts(used by this page) -->
<script src="{{asset('assets/js/pages/crud/forms/widgets/bootstrap-datepicker.js')}}" type="text/javascript"></script>
<script>
var KTDatatablesDataSourceHtml = function() {

    var initTable1 = function() {
        var table = $('#kt_table_1');

        // begin first table
        table.DataTable({
            responsive: true,

        });

    };

    return {

        //main function to initiate the module
        init: function() {
            initTable1();
        },

    };

}();

jQuery(document).ready(function() {
    KTDatatablesDataSourceHtml.init();
});
</script>


@endpush
