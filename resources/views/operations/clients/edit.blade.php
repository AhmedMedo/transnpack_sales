@extends('operations.layouts.master')
@section('title','Admin Area')
@push('styles')

@endpush

@section('content')

<div class="kt-portlet">
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                Edit Client
            </h3>
        </div>
    </div>

    <!--begin::Form-->
    <form class="kt-form kt-form--label-right" id="kt_form_1" method="POST"
        action="{{route('clients.update',$client->id)}}">
        {{ method_field('PUT') }}
        @csrf
        <div class="kt-portlet__body">
            <div class="kt-form__content">
                <div class="kt-alert m-alert--icon alert alert-danger kt-hidden" role="alert" id="kt_form_1_msg">
                    <div class="kt-alert__icon">
                        <i class="la la-warning"></i>
                    </div>
                    <div class="kt-alert__text">
                        Oh snap! Change a few things up and try submitting again.
                    </div>
                    <div class="kt-alert__close">
                        <button type="button" class="close" data-close="alert" aria-label="Close">
                        </button>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-3 col-sm-12">Client name*</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <div class="kt-typeahead">
                        <input name="name" value="{{$client->name}}" class="form-control" type="text"
                            placeholder="Client name">
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-3 col-sm-12">Agent Email*</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <div class="kt-typeahead">
                        <input name="agent_email" value="{{$client->agent_email}}" class="form-control" type="text"
                            placeholder="Agent name">
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-3 col-sm-12">Agent name*</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <div class="kt-typeahead">
                        <input name="agent_name" value="{{$client->agent_name}}" class="form-control" type="text"
                            placeholder="Agent name">
                        <span class="cursor--pointer" style="text-decoration: underline;"
                            onclick="addmoreWithEmail('agent','Agent name','Agent Email','{{count($client->agents)}}')">Add
                            more +</span>
                    </div>
                </div>
            </div>
            <div id="agent_container" class="appended-section @if(count($client->agents) == 0) display--none @endif">
                @if(count($client->agents))
                @foreach($client->agents as $agent)
                <div class="form-group row" id="agent_{{$loop->iteration}}">
                    <div class="col-lg-5 col-lg-offset-4 col-md-12 col-sm-12">
                        <div class="kt-typeahead">
                            <input class="form-control" type="text" value="{{$agent->name}}"
                                name="agents[{{$loop->iteration}}][name]" placeholder="Agent Name">
                        </div>

                    </div>
                    <div class="col-lg-5 col-md-12 col-sm-12">
                        <div class="kt-typeahead">
                            <input class="form-control" type="email" value="{{$agent->email}}"
                                name="agents[{{$loop->iteration}}][email]" placeholder="Agent Email">
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-12 col-sm-12">
                        <div class="kt-typeahead">
                            <i class="fas fa-times-circle fa-lg" onclick="removeRow('agent_{{$loop->iteration}}')"></i>
                        </div>
                    </div>
                </div>
                @endforeach


                @endif
            </div>


            <div class="form-group row">
                <label class="col-form-label col-lg-3 col-sm-12">LSP *</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <select class="form-control kt-select2" id="kt_select2" name="lsp">
                        <option value="1" @if($client->lsp == 1) selected @endif>Yes</option>
                        <option value="0" @if($client->lsp == 0) selected @endif >No</option>
                    </select>
                    <span class="form-text text-muted">Select an option</span>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-3 col-sm-12">Country*</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <select name="country_id" class="form-control kt-select2" id="kt_select2">
                        @foreach($countries as $country)
                        <option value="{{$country->id}}" @if($client->country_id == $country->id) selected
                            @endif>{{$country->name}}</option>
                        @endforeach
                    </select>
                    <span class="form-text text-muted">Select an option</span>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-form-label col-lg-3 col-sm-12">Address Line 1</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <div class="kt-typeahead">
                        <textarea name="address_1" value="{{$client->address_1}}" class="form-control" type="text"
                            placeholder="Address Line 1"></textarea>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-3 col-sm-12">Address Line 2</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <div class="kt-typeahead">
                        <textarea name="address_2" value="{{$client->address_2}}" class="form-control" type="text"
                            placeholder="Address Line 2"></textarea>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-3 col-sm-12">Zip Code</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <div class="kt-typeahead">
                        <input name="zip_code" value="{{$client->zip_code}}" class="form-control" type="text"
                            placeholder="Zip code">
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-3 col-sm-12">Time Zone</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <select name="timezone" class="form-control kt-select2" id="kt_select2">
                        @foreach($timezones as $timezone)
                        <option value="{{$timezone}}">{{$timezone}}</option>

                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-3 col-sm-12">Email*</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <div class="kt-typeahead">
                        <input name="client_email" value="{{$client->email}}" class="form-control" type="text"
                            placeholder="Email">
                        <span class="cursor--pointer" style="text-decoration: underline;"
                            onclick="addmore('email','Email','{{count($client->emails)}}')">Add more +</span>
                    </div>
                </div>
            </div>
            <div id="email_container" class="appended-section @if(count($client->emails) == 0) display--none @endif">
                @if(count($client->emails))
                @foreach($client->emails as $email)
                <div class="form-group row" id="email_{{$loop->iteration}}">
                    <label class="col-form-label col-lg-3 col-sm-12">Email</label>
                    <div class="col-lg-4 col-md-9 col-sm-12">
                        <div class="kt-typeahead">
                            <input class="form-control" value="{{$email->email}}" name="email[]" type="text"
                                placeholder="Email">
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-12 col-sm-12">
                        <div class="kt-typeahead">
                            <i class="fas fa-times-circle fa-2x" onclick="removeRow('email_{{$loop->iteration}}')"></i>
                        </div>
                    </div>
                </div>
                @endforeach
                @endif

            </div>
        </div>

        <div class="form-group row">
            <label class="col-form-label col-lg-3 col-sm-12">Phone number</label>
            <div class="col-lg-4 col-md-9 col-sm-12">
                <div class="kt-typeahead">
                    <input name="client_mobile" value="{{$client->phone}}" class="form-control" type="text"
                        placeholder="Phone number">
                    <span class="cursor--pointer" style="text-decoration: underline;"
                        onclick="addmore('phone','Phone number','{{count($client->phones)}}')">Add more +</span>
                </div>

            </div>
        </div>
        <div id="phone_container" class="appended-section @if(count($client->phones) == 0)  display--none @endif">
            @if(count($client->phones))
            @foreach($client->phones as $phone)
            <div class="form-group row" id="phone_{{$loop->iteration}}">
                <label class="col-form-label col-lg-3 col-sm-12">phone</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <div class="kt-typeahead">
                        <input class="form-control" value="{{$phone->phone}}" name="phone[]" type="text"
                            placeholder="Phone">
                    </div>
                </div>
                <div class="col-lg-2 col-md-12 col-sm-12">
                    <div class="kt-typeahead">
                        <i class="fas fa-times-circle fa-2x" onclick="removeRow('phone_{{$loop->iteration}}')"></i>
                    </div>
                </div>
            </div>
            @endforeach
            @endif
        </div>

        <div class="form-group row">
            <label class="col-form-label col-lg-3 col-sm-12">Fax number</label>
            <div class="col-lg-4 col-md-9 col-sm-12">
                <div class="kt-typeahead">
                    <input name="client_fax" value="{{$client->fax}}" class="form-control" type="text"
                        placeholder="Fax number">
                    <span class="cursor--pointer" style="text-decoration: underline;"
                        onclick="addmore('fax','Fax number','{{count($client->faxes)}}')">Add
                        more +</span>
                </div>

            </div>
        </div>
        <div id="fax_container" class="appended-section @if(count($client->faxes) == 0)  display--none @endif">
            @if(count($client->faxes))
            @foreach($client->faxes as $fax)
            <div class="form-group row" id="fax_{{$loop->iteration}}">
                <label class="col-form-label col-lg-3 col-sm-12">fax</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <div class="kt-typeahead">
                        <input class="form-control" value="{{$fax->fax}}" name="fax[]" type="text" placeholder="fax">
                    </div>
                </div>
                <div class="col-lg-2 col-md-12 col-sm-12">
                    <div class="kt-typeahead">
                        <i class="fas fa-times-circle fa-2x" onclick="removeRow('fax_{{$loop->iteration}}')"></i>
                    </div>
                </div>
            </div>
            @endforeach
            @endif
        </div>

</div>


</div>
<div class="kt-portlet__foot">
    <div class=" ">
        <div class="row">
            <div class="col-lg-9 ml-lg-auto">
                <button type="submit" class="btn btn-success">Save</button>
                <button type="reset" class="btn btn-secondary">Cancel</button>
            </div>
        </div>
    </div>
</div>
</form>

<!--end::Form-->
</div>


@endsection

@push('scripts')
<script src="{{asset('assets/plugins/custom/fullcalendar/fullcalendar.bundle.js')}}" type="text/javascript"></script>

<!--end::Page Vendors -->

<!--begin::Page Scripts(used by this page) -->
<script src="{{asset('assets/js/pages/crud/forms/widgets/bootstrap-datepicker.js')}}" type="text/javascript"></script>

<script>
function addmore(ref, title, count) {
    var i = parseInt(count) + 1;
    $(`#${ref}_container`).append(
        `<div class="form-group row" id="${ref}_${i}">
                            <label class="col-form-label col-lg-3 col-sm-12">${title}</label>
                            <div class="col-lg-4 col-md-9 col-sm-12">
                                <div class="kt-typeahead">
                                    <input  class="form-control" name="${ref}[]" type="text"  placeholder="${title}">
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-12 col-sm-12">
                                <div class="kt-typeahead">
                                <i class="fas fa-times-circle fa-2x" onclick="removeRow('${ref}_${i}')"></i>                               </div>
                                </div>
                            </div>
                        </div>
    `
    )
    i++;
    if ($(`#${ref}_container`).children().length > 0) {
        $(`#${ref}_container`).removeClass('display--none');
    }
}


function addmoreWithEmail(ref, title, email_title, count) {
    var i = parseInt(count) + 1;
    $(`#${ref}_container`).append(
        `<div class="form-group row" id="agent_${i}">
                        <div class="col-lg-5 col-lg-offset-4 col-md-12 col-sm-12">
                            <div class="kt-typeahead">
                                <input  class="form-control" type="text" name="agents[${i}][name]" placeholder="${title}">
                            </div>

                        </div>
                        <div class="col-lg-5 col-md-12 col-sm-12">
                            <div class="kt-typeahead">
                                <input  class="form-control" type="email" name="agents[${i}][email]"  placeholder="${email_title}">
                            </div>

                        </div>
                        <div class="col-lg-2 col-md-12 col-sm-12">
                            <div class="kt-typeahead">
                            <i class="fas fa-times-circle fa-2x" onclick="removeRow('agent_${i}')"></i>                               </div>
                            </div>
                        </div>
                    </div>
`
    )
    i++;
    if ($(`#${ref}_container`).children().length > 0) {
        $(`#${ref}_container`).removeClass('display--none');
    }
}

function addMoreData() {
    $("#extra-data").append(`
        <hr>
        <div class="form-group row">
                                    <label class="col-form-label col-lg-3 col-sm-12">Email*</label>
                                    <div class="col-lg-4 col-md-9 col-sm-12">
                                        <div class="kt-typeahead">
                                            <input class="form-control" type="text"  placeholder="Email">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-form-label col-lg-3 col-sm-12">Phone number</label>
                                    <div class="col-lg-4 col-md-9 col-sm-12">
                                        <div class="kt-typeahead">
                                            <input class="form-control" type="text"  placeholder="Phone number">
                                        </div>
                                    </div>
                                </div>
        `)
}
</script>
<script>
$('#kt_sweetalert_trans').click(function(e) {
    swal.fire("Success!", "The New Client has been added successfully", "success")
        .then(result => {
            if (result.value) {
                window.location.replace('clients.html')
            }
        })
});

function removeRow(div) {
    $("#" + div).remove();

}
</script>


@endpush
