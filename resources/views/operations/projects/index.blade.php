@extends('operations.layouts.master')
@section('title','Admin Area')
@push('styles')

@endpush

@section('content')

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12 text-right" style="margin-bottom:12px">
        <a type="button" class="btn btn-success btn-sm" style="color: white;" href="{{route('projects.create')}}">Add
            New
            Project</a>
    </div>
</div>

<div class="row">
    <div class="col">
        <div class="alert alert-light alert-elevate fade show" role="alert">
            <div class="alert-text">
                <div class="kt-portlet__head-label">
                    <h5 class="kt-portlet__head-title">
                        Find a project
                    </h5>
                </div>
                <form method="GET" action="{{route('projects.index')}}">
                    @csrf
                    <div class="row">

                        <div class="col-md-3 col-sm-3 col-xs-12">
                            <div class="form-group">
                                <label>Project ID</label>
                                <input name="project_id" type="text" class="form-control" placeholder="Project ID">
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-12">
                            <div class="form-group">
                                <label>Source Language</label>
                                <select class="form-control" name="from_lang_id">
                                    <option value="">Select Language...</option>
                                    @foreach($languages as $language)
                                    <option value="{{$language->id}}">{{$language->name}}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-3 col-xs-12">
                            <div class="form-group">
                                <label>Target Language</label>
                                <select class="form-control" name="to_lang_id">
                                    <option value="">Select Language...</option>
                                    @foreach($languages as $language)
                                    <option value="{{$language->id}}">{{$language->name}}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-3 col-xs-12">
                            <div class="form-group">
                                <label>Service Type</label>
                                <select name="service" class="form-control">
                                    @foreach($services as $service)

                                    <option value="{{$service->id}}">{{$service->name}}

                                        @endforeach
                                </select>
                            </div>
                        </div>


                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <label>From Date</label>
                            <input name="form_date" type="text" class="form-control" id="kt_datepicker_1" readonly
                                placeholder="Select date" />
                        </div>

                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <label>To Date</label>
                            <input name="from_date" type="text" class="form-control" id="kt_datepicker_1" readonly
                                placeholder="Select date" />
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="form-group">
                                <label>Client Name</label>
                                <input name="client_name" type="text" class="form-control" placeholder="Client Name">
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12 text-right">
                            <button type="submit" class="btn btn-primary btn-sm">Filter</button>
                        </div>
                        @if(request()->input())
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <button onclick="location.href='{{route('projects.index')}}'" type="button"
                                class="btn btn-danger btn-sm">Reset</button>
                        </div>
                        @endif
                    </div>
                </form>

            </div>
        </div>
    </div>

</div>
<!--Begin::Section-->

<div class="row">
    <div class="col-xl-12">
        <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
            <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                <div class="kt-portlet__head-label">
                    <h3 style="padding-right:12px">Projects</h3>
                </div>
                <div>
                    <div class="kt-checkbox-inline" style="padding-top:19px">
                        <label class="kt-checkbox trans-bold">
                            <input onchange="filterme()" name="type" value="pending" type="checkbox"> Pending projects
                            <span></span>
                        </label>

                        <label class="kt-checkbox trans-bold">
                            <input onchange="filterme()" name="type" value="assigned" type="checkbox"> Assigned projects
                            <span></span>
                        </label>

                        <label class="kt-checkbox trans-bold">
                            <input onchange="filterme()" name="type" value="delivered" type="checkbox"> Delivered projects
                            <span></span>
                        </label>

                        <label class="kt-checkbox trans-bold">
                            <input onchange="filterme()" name="type" value="completed" type="checkbox"> Completed projects
                            <span></span>
                        </label>
                    </div>
                </div>


            </div>
            <div class="kt-portlet__body">

                <!--begin: Datatable -->
                <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
                    <thead>
                        <tr>
                            <th>Project ID</th>
                            <!-- <th>Project Name</th> -->
                            <th>Source Language</th>
                            <th>Target Language</th>
                            <th>Service Type</th>
                            <th>Client Name</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($projects as $project)
                        <tr>
                            <td><a href="{{route('projects.show',$project->id)}}">{{$project->projectID}}</a></td>
                            <td>{{@$project->Fromlanguage->name}}</td>
                            <td>{{@$project->Tolanguage->name}}</td>
                            <td>{{@$project->service->name}}</td>
                            <td>{{@$project->client->name}}</td>
                            <td>{{@$project->status}}</td>
                            <td>
                                <div class="dropdown">
                                    <a href="javascript:;" class="btn btn-sm btn-clean btn-icon btn-icon-md"
                                        data-toggle="dropdown">
                                        <i class="flaticon-more-1"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <ul class="kt-nav">
                                            <li class="kt-nav__item">
                                                <a href="{{route('projects.show',$project->id)}}" class="kt-nav__link">
                                                    <i class="kt-nav__link-icon flaticon2-expand"></i>
                                                    <span class="kt-nav__link-text">View</span>
                                                </a>
                                            </li>
                                            <li class="kt-nav__item">
                                                <a href="{{route('projects.edit',$project->id)}}" class="kt-nav__link">
                                                    <i class="kt-nav__link-icon flaticon2-contract"></i>
                                                    <span class="kt-nav__link-text">Edit</span>
                                                </a>
                                            </li>
                                            <!-- <li class="kt-nav__item">
                                                <a href="#"
                                                    onclick='$("#Delete_project_{{$project->id}}").submit();return false;'
                                                    class="kt-nav__link">
                                                    <form id="Delete_project_{{$project->id}}"
                                                        action="{{route('projects.destroy',$project->id) }}"
                                                        method="post">
                                                        <input type="hidden" name="_method" value="delete" />
                                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                        <i class="kt-nav__link-icon flaticon2-trash"></i>
                                                        <span class="kt-nav__link-text">Delete</span>

                                                    </form>

                                                </a>
                                            </li> -->

                                        </ul>
                                    </div>
                                </div>

                            </td>

                        </tr>


                        @endforeach
                    </tbody>
                </table>
                <!--end: Datatable -->
            </div>
        </div>
    </div>
    <!-- <div class="col-md-12 col-sm-12 col-xs-12 text-right" style="margin-bottom:12px">
        <a href="./project-view.html" type="button" class="btn btn-primary btn-sm">view Selected </a>
    </div> -->
</div>

@endsection

@push('scripts')
<script src="{{asset('assets/plugins/custom/fullcalendar/fullcalendar.bundle.js')}}" type="text/javascript"></script>

<!--end::Page Vendors -->

<!--begin::Page Scripts(used by this page) -->
<script src="{{asset('assets/js/pages/crud/forms/widgets/bootstrap-datepicker.js')}}" type="text/javascript"></script>
<script>
var KTDatatablesDataSourceHtml = function() {

    var initTable1 = function() {
        var table = $('#kt_table_1');

        // begin first table
        table.DataTable({
            responsive: true,

        });

    };

    return {

        //main function to initiate the module
        init: function() {
            initTable1();
        },

    };

}();

jQuery(document).ready(function() {
    KTDatatablesDataSourceHtml.init();


});
</script>

<script>
function filterme() {
    var table = $('#kt_table_1').dataTable();
    //build a regex filter string with an or(|) condition
    var types = $('input:checkbox[name="type"]:checked').map(function() {
        return '^' + this.value + '\$';
    }).get().join('|');
    console.log(types);
    //filter in column 0, with an regex, no smart filtering, no inputbox,not case sensitive
    table.fnFilter(types, 5, true, false, false, false);
}
</script>





@endpush
