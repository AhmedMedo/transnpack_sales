@extends('operations.layouts.master')
@section('title','Admin Area')
@push('styles')

@endpush

@section('content')
<div class="kt-portlet">
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                Complete to submit request as completed
            </h3>
        </div>
    </div>

    <!--begin::Form-->
    <form class="kt-form kt-form--label-right" method="POST" action="{{route('operations.submit_project_compelete',$id)}}">
        @csrf
        <div class="kt-portlet__body">
            <div class="kt-form__content">
                <div class="kt-alert m-alert--icon alert alert-danger kt-hidden" role="alert" id="kt_form_1_msg">
                    <div class="kt-alert__icon">
                        <i class="la la-warning"></i>
                    </div>
                    <div class="kt-alert__text">
                        Oh snap! Change a few things up and try submitting again.
                    </div>
                    <div class="kt-alert__close">
                        <button type="button" class="close" data-close="alert" aria-label="Close">
                        </button>
                    </div>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-form-label col-lg-3 col-sm-12">Delivery quality
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <select name="delivery_quality" class="form-control kt-select2" id="kt_select2" name="select2">
                        <span class="form-text text-muted">Select an option</span>
                        <option value="50"> 50%</option>
                        <option value="60"> 60%</option>
                        <option value="70"> 70%</option>
                        <option value="80"> 80%</option>
                        <option value="90"> 90%</option>
                    </select>
                </div>
            </div>


            <div class="form-group row">
                <label class="col-form-label col-lg-3 col-sm-12">Deadline met correctly?
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <select name="is_deadline_met" class="form-control kt-select2" id="kt_select2" name="select2">
                        <span class="form-text text-muted">Select an option</span>
                        <option value="0">No</option>
                        <option value="1">Yes</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-3 col-sm-12">Comments if any
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <div class="kt-typeahead">
                        <textarea name="comments" class="form-control" type="text" placeholder="Comments"></textarea>
                    </div>
                </div>
            </div>

        </div>
        <div class="kt-portlet__foot">
            <div class=" ">
                <div class="row">
                    <div class="col-lg-9 ml-lg-auto">
                        <button type="submit" class="btn btn-success">Save</button>
                        <button type="reset" class="btn btn-secondary">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <!--end::Form-->
</div>
@endsection

@push('scripts')
<script>

</script>


@endpush
