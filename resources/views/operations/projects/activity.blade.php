@extends('operations.layouts.master')
@section('title','Admin Area')
@push('styles')

@endpush

@section('content')
<!--Begin::Section-->
<div class="row">
    <div class="col-xl-12">
        <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
            <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Project Acitivty
                    </h3>
                </div>

            </div>
            <div class="kt-portlet__body">

                <!--begin: Datatable -->
                <div class="kt-datatable" id="incoming_requests"></div>
                <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
                    <thead>
                        <tr>
                            <th>Action</th>
                            <!-- <th>Project Name</th> -->
                            <th>User</th>
                            <th>Updates</th>
                            <th>Date</th>
                            <th>Time</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($activites as $activity)
                        <tr>
                            <td>{{$activity->description}}</td>
                            <td>{{$activity->causer->name}}</td>
                            <td>
                                @php
                                    $updates=[];
                                    if(!is_null($activity->properties))
                                    {
                                        $updates = json_decode($activity->properties,true);
                                    }
                                @endphp
                                @if(count($updates))
                                    New Data:<br>
                                    @foreach($updates['attributes'] as $key => $value)
                                        {{$key}}  => {{$value}}
                                        <br>
                                    @endforeach
                                    Old Data:<br>
                                    @if(array_key_exists('old',$updates))
                                    @foreach($updates['old'] as $key => $value)
                                        {{$key}}  => {{$value}}
                                        <br>
                                    @endforeach
                                    @endif
                                @endif
                            </td>
                            <td>{{\Carbon\Carbon::parse($activity->created_at)->format('Y-m-d')}}</td>
                            <td>{{\Carbon\Carbon::parse($activity->created_at)->format('H:i:s')}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <!--end: Datatable -->
            </div>
        </div>
    </div>

</div>

@endsection

@push('scripts')
<script>
var KTDatatablesDataSourceHtml = function() {

    var initTable1 = function() {
        var table = $('#kt_table_1');

        // begin first table
        table.DataTable({
            responsive: true,

        });

    };

    return {

        //main function to initiate the module
        init: function() {
            initTable1();
        },

    };

}();

jQuery(document).ready(function() {
    KTDatatablesDataSourceHtml.init();


});
</script>


@endpush
