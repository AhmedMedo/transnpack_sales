@extends('operations.layouts.master')
@section('title','Admin Area')
@push('styles')
<link href="{{asset('assets/css/pages/inbox/inbox.css')}}" rel="stylesheet" type="text/css" />

@endpush

@section('content')

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12" style="text-align: end;
                            margin-bottom: 12px;">
        <a type="button" class="btn btn-success btn-sm" style="color: white;" href="./mail-new.html">Add New Thread</a>
    </div>
</div>

<!--Begin::Inbox-->
<div class="kt-grid kt-grid--desktop kt-grid--ver-desktop  kt-inbox" id="kt_inbox">

    <!--Begin::Aside Mobile Toggle-->
    <button class="kt-inbox__aside-close" id="kt_inbox_aside_close">
        <i class="la la-close"></i>
    </button>
    <!--Begin:: Inbox List-->
    <div class="kt-grid__item kt-grid__item--fluid    kt-portlet    kt-inbox__list kt-inbox__list--shown"
        id="kt_inbox_list">
        <div class="kt-portlet__head">
            <div class="kt-inbox__toolbar kt-inbox__toolbar--extended">
                <div class="kt-inbox__actions kt-inbox__actions--expanded">
                    <div class="kt-inbox__check">
                        <label class="kt-checkbox kt-checkbox--single kt-checkbox--tick kt-checkbox--brand">
                            <input type="checkbox">
                            <span></span>
                        </label>
                        <div class="btn-group">
                            <button type="button" class="kt-inbox__icon kt-inbox__icon--sm kt-inbox__icon--light"
                                data-toggle="dropdown">
                                <i class="flaticon2-down"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-left dropdown-menu-fit dropdown-menu-xs">
                                <ul class="kt-nav">
                                    <li class="kt-nav__item kt-nav__item--active">
                                        <a href="#" class="kt-nav__link">
                                            <span class="kt-nav__link-text">All</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <span class="kt-nav__link-text">Read</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <span class="kt-nav__link-text">Unread</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <span class="kt-nav__link-text">Starred</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <span class="kt-nav__link-text">Unstarred</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                    </div>
                    <div class="kt-inbox__panel">


                        <button class="kt-inbox__icon" data-toggle="kt-tooltip" title="Delete">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <rect x="0" y="0" width="24" height="24" />
                                    <path
                                        d="M6,8 L6,20.5 C6,21.3284271 6.67157288,22 7.5,22 L16.5,22 C17.3284271,22 18,21.3284271 18,20.5 L18,8 L6,8 Z"
                                        fill="#000000" fill-rule="nonzero" />
                                    <path
                                        d="M14,4.5 L14,4 C14,3.44771525 13.5522847,3 13,3 L11,3 C10.4477153,3 10,3.44771525 10,4 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z"
                                        fill="#000000" opacity="0.3" />
                                </g>
                            </svg> </button>


                    </div>
                </div>
                <div class="kt-inbox__search">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search">
                        <div class="input-group-append">
                            <span class="input-group-text">

                                <!--<i class="la la-group"></i>-->
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                    width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect x="0" y="0" width="24" height="24" />
                                        <path
                                            d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z"
                                            fill="#000000" fill-rule="nonzero" opacity="0.3" />
                                        <path
                                            d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z"
                                            fill="#000000" fill-rule="nonzero" />
                                    </g>
                                </svg>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="kt-inbox__controls">
                    <div class="kt-inbox__pages" data-toggle="kt-tooltip" title="Records per page">
                        <span class="kt-inbox__perpage" data-toggle="dropdown">1 - 50 of 235</span>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-fit dropdown-menu-xs">
                            <ul class="kt-nav">
                                <li class="kt-nav__item">
                                    <a href="#" class="kt-nav__link">
                                        <span class="kt-nav__link-text">20 per page</span>
                                    </a>
                                </li>
                                <li class="kt-nav__item kt-nav__item--active">
                                    <a href="#" class="kt-nav__link">
                                        <span class="kt-nav__link-text">50 par page</span>
                                    </a>
                                </li>
                                <li class="kt-nav__item">
                                    <a href="#" class="kt-nav__link">
                                        <span class="kt-nav__link-text">100 per page</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <button class="kt-inbox__icon" data-toggle="kt-tooltip" title="Previose page">
                        <i class="flaticon2-left-arrow"></i>
                    </button>
                    <button class="kt-inbox__icon" data-toggle="kt-tooltip" title="Next page">
                        <i class="flaticon2-right-arrow"></i>
                    </button>
                    <div class="kt-inbox__sort" data-toggle="kt-tooltip" title="Sort">
                        <button type="button" class="kt-inbox__icon" data-toggle="dropdown">
                            <i class="flaticon2-console"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-fit dropdown-menu-xs">
                            <ul class="kt-nav">
                                <li class="kt-nav__item kt-nav__item--active">
                                    <a href="#" class="kt-nav__link">
                                        <span class="kt-nav__link-text">Newest</span>
                                    </a>
                                </li>
                                <li class="kt-nav__item">
                                    <a href="#" class="kt-nav__link">
                                        <span class="kt-nav__link-text">Olders</span>
                                    </a>
                                </li>
                                <li class="kt-nav__item">
                                    <a href="#" class="kt-nav__link">
                                        <span class="kt-nav__link-text">Unread</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="kt-portlet__body kt-portlet__body--fit-x">
            <div class="kt-inbox__items" data-type="inbox">
                <div class="kt-inbox__item kt-inbox__item--unread" data-id="1" data-type="inbox">
                    <div class="kt-inbox__info">
                        <div class="kt-inbox__actions">
                            <label class="kt-checkbox kt-checkbox--single kt-checkbox--tick kt-checkbox--brand">
                                <input type="checkbox">
                                <span></span>
                            </label>

                        </div>
                        <div class="kt-inbox__sender" data-toggle="view">
                            <span class="kt-media kt-media--sm kt-media--danger"
                                style="background-image: url('assets/media/users/100_13.jpg')">
                                <span></span>
                            </span>
                            <a href="#" class="kt-inbox__author">Sean Paul</a>
                        </div>
                    </div>
                    <div class="kt-inbox__details" data-toggle="view">
                        <div class="kt-inbox__message">
                            <span class="kt-inbox__subject">Digital PPV Customer Confirmation - </span>
                            <span class="kt-inbox__summary">Thank you for ordering UFC 240 Holloway vs Edgar Alternate
                                camera angles...</span>
                        </div>
                        <div class="kt-inbox__labels">
                            <span
                                class="kt-inbox__label kt-badge kt-badge--unified-brand kt-badge--bold kt-badge--inline">inbox</span>
                            <span
                                class="kt-inbox__label kt-badge kt-badge--unified-brand kt-badge--bold kt-badge--inline">YTG123</span>
                        </div>
                    </div>
                    <div class="kt-inbox__datetime" data-toggle="view">
                        8:30 PM
                    </div>
                </div>
                <div class="kt-inbox__item kt-inbox__item--unread" data-id="2" data-type="inbox">
                    <div class="kt-inbox__info">
                        <div class="kt-inbox__actions">
                            <label class="kt-checkbox kt-checkbox--single kt-checkbox--tick kt-checkbox--brand">
                                <input type="checkbox">
                                <span></span>
                            </label>

                        </div>
                        <div class="kt-inbox__sender" data-toggle="view">
                            <span class="kt-media kt-media--sm kt-media--danger">
                                <span>OJ</span>
                            </span>
                            <a href="#" class="kt-inbox__author">Oliver Jake</a>
                        </div>
                    </div>
                    <div class="kt-inbox__details" data-toggle="view">
                        <div class="kt-inbox__message">
                            <span class="kt-inbox__subject">Your iBuy.com grocery shopping confirmation - </span>
                            <span class="kt-inbox__summary">Please make sure that you have one of the following cards
                                with you when we deliver your order...</span>
                        </div>
                        <div class="kt-inbox__labels">
                            <span
                                class="kt-inbox__label kt-badge kt-badge--unified-brand kt-badge--bold kt-badge--inline">YTG123</span>
                        </div>
                    </div>
                    <div class="kt-inbox__datetime" data-toggle="view">
                        day ago
                    </div>
                </div>
                <div class="kt-inbox__item" data-id="3" data-type="inbox">
                    <div class="kt-inbox__info">
                        <div class="kt-inbox__actions">
                            <label class="kt-checkbox kt-checkbox--single kt-checkbox--tick kt-checkbox--brand">
                                <input type="checkbox">
                                <span></span>
                            </label>

                        </div>
                        <div class="kt-inbox__sender" data-toggle="view">
                            <span class="kt-media kt-media--sm kt-media--brand">
                                <span>EF</span>
                            </span>
                            <a href="#" class="kt-inbox__author">Enrico Fermi</a>
                        </div>
                    </div>
                    <div class="kt-inbox__details" data-toggle="view">
                        <div class="kt-inbox__message">
                            <span class="kt-inbox__subject">Your Order #224820998666029 has been Confirmed - </span>
                            <span class="kt-inbox__summary">Your Order #224820998666029 has been placed on Saturday, 29
                                June, 2019 10:02:41 via Online Banking...</span>
                        </div>
                        <div class="kt-inbox__labels">
                            <span
                                class="kt-inbox__label kt-badge kt-badge--unified-brand kt-badge--bold kt-badge--inline">YTG123</span>
                        </div>
                    </div>
                    <div class="kt-inbox__datetime" data-toggle="view">
                        11:20PM
                    </div>
                </div>
                <div class="kt-inbox__item" data-id="4" data-type="inbox">
                    <div class="kt-inbox__info">
                        <div class="kt-inbox__actions">
                            <label class="kt-checkbox kt-checkbox--single kt-checkbox--tick kt-checkbox--brand">
                                <input type="checkbox">
                                <span></span>
                            </label>

                        </div>
                        <div class="kt-inbox__sender" data-toggle="view">
                            <span class="kt-media kt-media--sm kt-media--brand"
                                style="background-image: url('assets/media/users/100_11.jpg')">
                                <span></span>
                            </span>
                            <a href="#" class="kt-inbox__author">Jane Goodall</a>
                        </div>
                    </div>
                    <div class="kt-inbox__details" data-toggle="view">
                        <div class="kt-inbox__message">
                            <span class="kt-inbox__subject">Payment Notification DLOP2329KD - </span>
                            <span class="kt-inbox__summary">Your payment of 4500USD to AirCar has been authorized and
                                confirmed, thank you...</span>
                        </div>
                        <div class="kt-inbox__labels">
                            <span
                                class="kt-inbox__label kt-badge kt-badge--unified-brand kt-badge--bold kt-badge--inline">YTG123</span>
                        </div>
                    </div>
                    <div class="kt-inbox__datetime" data-toggle="view">
                        2 days ago
                    </div>
                </div>
                <div class="kt-inbox__item" data-id="5" data-type="inbox">
                    <div class="kt-inbox__info">
                        <div class="kt-inbox__actions">
                            <label class="kt-checkbox kt-checkbox--single kt-checkbox--tick kt-checkbox--brand">
                                <input type="checkbox">
                                <span></span>
                            </label>

                        </div>
                        <div class="kt-inbox__sender" data-toggle="view">
                            <span class="kt-media kt-media--sm kt-media--success">
                                <span>MP</span>
                            </span>
                            <a href="#" class="kt-inbox__author">Max O'Brien Planck</a>
                        </div>
                    </div>
                    <div class="kt-inbox__details" data-toggle="view">
                        <div class="kt-inbox__message">
                            <span class="kt-inbox__subject">Congratulations on your iRun Coach subscription - </span>
                            <span class="kt-inbox__summary">Congratulations on your iRun Coach subscription. You made no
                                space for excuses and you decided on a healthier and happier life...</span>
                        </div>
                        <div class="kt-inbox__labels">
                            <span
                                class="kt-inbox__label kt-badge kt-badge--unified-brand kt-badge--bold kt-badge--inline">YTG123</span>
                        </div>
                    </div>
                    <div class="kt-inbox__datetime" data-toggle="view">
                        Jul 25
                    </div>
                </div>
                <div class="kt-inbox__item" data-id="6" data-type="inbox">
                    <div class="kt-inbox__info">
                        <div class="kt-inbox__actions">
                            <label class="kt-checkbox kt-checkbox--single kt-checkbox--tick kt-checkbox--brand">
                                <input type="checkbox">
                                <span></span>
                            </label>

                        </div>
                        <div class="kt-inbox__sender" data-toggle="view">
                            <span class="kt-media kt-media--sm kt-media--brand"
                                style="background-image: url('assets/media/users/100_7.jpg')">
                                <span></span>
                            </span>
                            <a href="#" class="kt-inbox__author">Rita Levi-Montalcini</a>
                        </div>
                    </div>
                    <div class="kt-inbox__details" data-toggle="view">
                        <div class="kt-inbox__message">
                            <span class="kt-inbox__subject">Pay bills & win up to 600$ Cashback! - </span>
                            <span class="kt-inbox__summary">Please make sure that you have one of the following cards
                                with you when we deliver your order...</span>
                        </div>
                        <div class="kt-inbox__labels">
                            <span
                                class="kt-inbox__label kt-badge kt-badge--unified-brand kt-badge--bold kt-badge--inline">YTG123</span>
                        </div>
                    </div>
                    <div class="kt-inbox__datetime" data-toggle="view">
                        July 24
                    </div>
                </div>
                <div class="kt-inbox__item kt-inbox__item--unread" data-id="7" data-type="inbox">
                    <div class="kt-inbox__info">
                        <div class="kt-inbox__actions">
                            <label class="kt-checkbox kt-checkbox--single kt-checkbox--tick kt-checkbox--brand">
                                <input type="checkbox">
                                <span></span>
                            </label>

                        </div>
                        <div class="kt-inbox__sender" data-toggle="view">
                            <span class="kt-media kt-media--sm kt-media--brand"
                                style="background-image: url('assets/media/users/100_8.jpg')">
                                <span></span>
                            </span>
                            <a href="#" class="kt-inbox__author">Stephen Hawking</a>
                        </div>
                    </div>
                    <div class="kt-inbox__details" data-toggle="view">
                        <div class="kt-inbox__message">
                            <span class="kt-inbox__subject">Activate your LIPO Account today - </span>
                            <span class="kt-inbox__summary">Thank you for creating a LIPO Account. Please click the link
                                below to activate your account. This link will expire in 24 hours...</span>
                        </div>
                        <div class="kt-inbox__labels">
                            <span
                                class="kt-inbox__label kt-badge kt-badge--unified-brand kt-badge--bold kt-badge--inline">YTG123</span>
                        </div>
                    </div>
                    <div class="kt-inbox__datetime" data-toggle="view">
                        Jun 13
                    </div>
                </div>
                <div class="kt-inbox__item" data-id="8" data-type="inbox">
                    <div class="kt-inbox__info">
                        <div class="kt-inbox__actions">
                            <label class="kt-checkbox kt-checkbox--single kt-checkbox--tick kt-checkbox--brand">
                                <input type="checkbox">
                                <span></span>
                            </label>

                        </div>
                        <div class="kt-inbox__sender" data-toggle="view">
                            <span class="kt-media kt-media--sm kt-media--dark">
                                <span>WE</span>
                            </span>
                            <a href="#" class="kt-inbox__author">Wolfgang Ernst Pauli</a>
                        </div>
                    </div>
                    <div class="kt-inbox__details" data-toggle="view">
                        <div class="kt-inbox__message">
                            <span class="kt-inbox__subject">About your request for PalmLake - </span>
                            <span class="kt-inbox__summary">What you requested can't be arranged ahead of time but
                                PalmLake said they'll do their best to accommodate you upon arrival....</span>
                        </div>
                        <div class="kt-inbox__labels">
                            <span
                                class="kt-inbox__label kt-badge kt-badge--unified-brand kt-badge--bold kt-badge--inline">YTG123</span>
                        </div>
                    </div>
                    <div class="kt-inbox__datetime" data-toggle="view">
                        25 May
                    </div>
                </div>
                <div class="kt-inbox__item" data-id="9" data-type="inbox">
                    <div class="kt-inbox__info">
                        <div class="kt-inbox__actions">
                            <label class="kt-checkbox kt-checkbox--single kt-checkbox--tick kt-checkbox--brand">
                                <input type="checkbox">
                                <span></span>
                            </label>

                        </div>
                        <div class="kt-inbox__sender" data-toggle="view">
                            <span class="kt-media kt-media--sm"
                                style="background-image: url('assets/media/users/100_12.jpg')">
                                <span></span>
                            </span>
                            <a href="#" class="kt-inbox__author">Sarah Boysen</a>
                        </div>
                    </div>
                    <div class="kt-inbox__details" data-toggle="view">
                        <div class="kt-inbox__message">
                            <span class="kt-inbox__subject">Verification of your card transaction - </span>
                            <span class="kt-inbox__summary">This is to confirm that you have used your credit/debit card
                                for the booking. If you did not make this booking, please contact us
                                immediately....</span>
                        </div>
                        <div class="kt-inbox__labels">
                            <span
                                class="kt-inbox__label kt-badge kt-badge--unified-brand kt-badge--bold kt-badge--inline">YTG123</span>
                        </div>
                    </div>
                    <div class="kt-inbox__datetime" data-toggle="view">
                        May 23
                    </div>
                </div>
                <div class="kt-inbox__item" data-id="10" data-type="inbox">
                    <div class="kt-inbox__info">
                        <div class="kt-inbox__actions">
                            <label class="kt-checkbox kt-checkbox--single kt-checkbox--tick kt-checkbox--brand">
                                <input type="checkbox">
                                <span></span>
                            </label>

                        </div>
                        <div class="kt-inbox__sender" data-toggle="view">
                            <span class="kt-media kt-media--sm kt-media--brand"
                                style="background-image: url('assets/media/users/100_14.jpg')">
                                <span></span>
                            </span>
                            <a href="#" class="kt-inbox__author">Max Born</a>
                        </div>
                    </div>
                    <div class="kt-inbox__details" data-toggle="view">
                        <div class="kt-inbox__message">
                            <span class="kt-inbox__subject">Payment Notification (DE223232034) - </span>
                            <span class="kt-inbox__summary">Your payment of 4500USD to AirCar has been authorized and
                                confirmed, thank you....</span>
                        </div>
                        <div class="kt-inbox__labels">
                            <span
                                class="kt-inbox__label kt-badge kt-badge--unified-brand kt-badge--bold kt-badge--inline">YTG123</span>
                        </div>
                    </div>
                    <div class="kt-inbox__datetime" data-toggle="view">
                        Apr 12
                    </div>
                </div>
                <div class="kt-inbox__item" data-id="11" data-type="inbox">
                    <div class="kt-inbox__info">
                        <div class="kt-inbox__actions">
                            <label class="kt-checkbox kt-checkbox--single kt-checkbox--tick kt-checkbox--brand">
                                <input type="checkbox">
                                <span></span>
                            </label>

                        </div>
                        <div class="kt-inbox__sender" data-toggle="view">
                            <span class="kt-media kt-media--sm kt-media--brand"
                                style="background-image: url('assets/media/users/100_5.jpg')">
                                <span></span>
                            </span>
                            <a href="#" class="kt-inbox__author">Patty Jo Watson</a>
                        </div>
                    </div>
                    <div class="kt-inbox__details" data-toggle="view">
                        <div class="kt-inbox__message">
                            <span class="kt-inbox__subject">Welcome, Patty - </span>
                            <span class="kt-inbox__summary">Discover interesting ideas and unique perspectives. Read,
                                explore and follow your interests. Get personalized recommendations delivered to
                                you....</span>
                        </div>
                        <div class="kt-inbox__labels">
                            <span
                                class="kt-inbox__label kt-badge kt-badge--unified-brand kt-badge--bold kt-badge--inline">YTG123</span>
                        </div>
                    </div>
                    <div class="kt-inbox__datetime" data-toggle="view">
                        Mar 1
                    </div>
                </div>
                <div class="kt-inbox__item" data-id="12" data-type="inbox">
                    <div class="kt-inbox__info">
                        <div class="kt-inbox__actions">
                            <label class="kt-checkbox kt-checkbox--single kt-checkbox--tick kt-checkbox--brand">
                                <input type="checkbox">
                                <span></span>
                            </label>

                        </div>
                        <div class="kt-inbox__sender" data-toggle="view">
                            <span class="kt-media kt-media--sm kt-media--warning">
                                <span>RW</span>
                            </span>
                            <a href="#" class="kt-inbox__author">Roberts O'Neill Wilson</a>
                        </div>
                    </div>
                    <div class="kt-inbox__details" data-toggle="view">
                        <div class="kt-inbox__message">
                            <span class="kt-inbox__subject">Optimize with Recommendations, now used by most advertisers
                                - </span>
                            <span class="kt-inbox__summary">Your weekly report is a good way to track your performance.
                                See what’s working so far and explore new opportunities for improvement....</span>
                        </div>
                        <div class="kt-inbox__labels">
                            <span
                                class="kt-inbox__label kt-badge kt-badge--unified-brand kt-badge--bold kt-badge--inline">YTG123</span>
                        </div>
                    </div>
                    <div class="kt-inbox__datetime" data-toggle="view">
                        Feb 11
                    </div>
                </div>
                <div class="kt-inbox__item" data-id="13" data-type="inbox">
                    <div class="kt-inbox__info">
                        <div class="kt-inbox__actions">
                            <label class="kt-checkbox kt-checkbox--single kt-checkbox--tick kt-checkbox--brand">
                                <input type="checkbox">
                                <span></span>
                            </label>

                        </div>
                        <div class="kt-inbox__sender" data-toggle="view">
                            <span class="kt-media kt-media--sm kt-media--brand"
                                style="background-image: url('assets/media/users/100_12.jpg')">
                                <span></span>
                            </span>
                            <a href="#" class="kt-inbox__author">Blaise Pascal</a>
                        </div>
                    </div>
                    <div class="kt-inbox__details" data-toggle="view">
                        <div class="kt-inbox__message">
                            <span class="kt-inbox__subject">Free Video Marketing Guide - </span>
                            <span class="kt-inbox__summary">Video has rolled into every marketing platform or channel,
                                leaving...</span>
                        </div>
                        <div class="kt-inbox__labels">
                            <span
                                class="kt-inbox__label kt-badge kt-badge--unified-brand kt-badge--bold kt-badge--inline">YTG123</span>
                        </div>
                    </div>
                    <div class="kt-inbox__datetime">
                        Jan 24
                    </div>
                </div>
                <div class="kt-inbox__item kt-inbox__item--unread" data-id="14" data-type="marked">
                    <div class="kt-inbox__info">
                        <div class="kt-inbox__actions">
                            <label class="kt-checkbox kt-checkbox--single kt-checkbox--tick kt-checkbox--brand">
                                <input type="checkbox">
                                <span></span>
                            </label>

                        </div>
                        <div class="kt-inbox__sender" data-toggle="view">
                            <span class="kt-media kt-media--sm kt-media--brand"
                                style="background-image: url('assets/media/users/100_3.jpg')">
                                <span></span>
                            </span>
                            <a href="#" class="kt-inbox__author">Pascal Moor</a>
                        </div>
                    </div>
                    <div class="kt-inbox__details" data-toggle="view">
                        <div class="kt-inbox__message">
                            <span class="kt-inbox__subject">Pro Article Marketing Guide - </span>
                            <span class="kt-inbox__summary">Video has rolled into every marketing platform or channel,
                                leaving...</span>
                        </div>
                        <div class="kt-inbox__labels">
                            <span
                                class="kt-inbox__label kt-badge kt-badge--unified-brand kt-badge--bold kt-badge--inline">YTG123</span>
                        </div>
                    </div>
                    <div class="kt-inbox__datetime">
                        Jan 24
                    </div>
                </div>
                <div class="kt-inbox__item" data-id="15" data-type="marked">
                    <div class="kt-inbox__info">
                        <div class="kt-inbox__actions">
                            <label class="kt-checkbox kt-checkbox--single kt-checkbox--tick kt-checkbox--brand">
                                <input type="checkbox">
                                <span></span>
                            </label>

                        </div>
                        <div class="kt-inbox__sender" data-toggle="view">
                            <span class="kt-media kt-media--sm kt-media--brand"
                                style="background-image: url('assets/media/users/100_12.jpg')">
                                <span></span>
                            </span>
                            <a href="#" class="kt-inbox__author">Blaise Pascal</a>
                        </div>
                    </div>
                    <div class="kt-inbox__details" data-toggle="view">
                        <div class="kt-inbox__message">
                            <span class="kt-inbox__subject">Free Video Marketing Guide - </span>
                            <span class="kt-inbox__summary">Video has rolled into every marketing platform or channel,
                                leaving...</span>
                        </div>
                        <div class="kt-inbox__labels">
                            <span
                                class="kt-inbox__label kt-badge kt-badge--unified-brand kt-badge--bold kt-badge--inline">YTG123</span>
                        </div>
                    </div>
                    <div class="kt-inbox__datetime">
                        Jan 24
                    </div>
                </div>
                <div class="kt-inbox__item" data-id="61" data-type="marked">
                    <div class="kt-inbox__info">
                        <div class="kt-inbox__actions">
                            <label class="kt-checkbox kt-checkbox--single kt-checkbox--tick kt-checkbox--brand">
                                <input type="checkbox">
                                <span></span>
                            </label>

                        </div>
                        <div class="kt-inbox__sender" data-toggle="view">
                            <span class="kt-media kt-media--sm kt-media--brand"
                                style="background-image: url('assets/media/users/100_7.jpg')">
                                <span></span>
                            </span>
                            <a href="#" class="kt-inbox__author">Rita Levi-Montalcini</a>
                        </div>
                    </div>
                    <div class="kt-inbox__details" data-toggle="view">
                        <div class="kt-inbox__message">
                            <span class="kt-inbox__subject">Pay bills & win up to 600$ Cashback! - </span>
                            <span class="kt-inbox__summary">Please make sure that you have one of the following cards
                                with you when we deliver your order...</span>
                        </div>
                        <div class="kt-inbox__labels">
                            <span
                                class="kt-inbox__label kt-badge kt-badge--unified-brand kt-badge--bold kt-badge--inline">YTG123</span>
                        </div>
                    </div>
                    <div class="kt-inbox__datetime" data-toggle="view">
                        July 24
                    </div>
                </div>
                <div class="kt-inbox__item kt-inbox__item--unread" data-id="71" data-type="marked">
                    <div class="kt-inbox__info">
                        <div class="kt-inbox__actions">
                            <label class="kt-checkbox kt-checkbox--single kt-checkbox--tick kt-checkbox--brand">
                                <input type="checkbox">
                                <span></span>
                            </label>

                        </div>
                        <div class="kt-inbox__sender" data-toggle="view">
                            <span class="kt-media kt-media--sm kt-media--brand"
                                style="background-image: url('assets/media/users/100_8.jpg')">
                                <span></span>
                            </span>
                            <a href="#" class="kt-inbox__author">Stephen Hawking</a>
                        </div>
                    </div>
                    <div class="kt-inbox__details" data-toggle="view">
                        <div class="kt-inbox__message">
                            <span class="kt-inbox__subject">Activate your LIPO Account today - </span>
                            <span class="kt-inbox__summary">Thank you for creating a LIPO Account. Please click the link
                                below to activate your account. This link will expire in 24 hours...</span>
                        </div>
                        <div class="kt-inbox__labels">
                            <span
                                class="kt-inbox__label kt-badge kt-badge--unified-brand kt-badge--bold kt-badge--inline">YTG123</span>
                        </div>
                    </div>
                    <div class="kt-inbox__datetime" data-toggle="view">
                        Jun 13
                    </div>
                </div>
                <div class="kt-inbox__item" data-id="8" data-type="marked">
                    <div class="kt-inbox__info">
                        <div class="kt-inbox__actions">
                            <label class="kt-checkbox kt-checkbox--single kt-checkbox--tick kt-checkbox--brand">
                                <input type="checkbox">
                                <span></span>
                            </label>

                        </div>
                        <div class="kt-inbox__sender" data-toggle="view">
                            <span class="kt-media kt-media--sm kt-media--dark">
                                <span>WE</span>
                            </span>
                            <a href="#" class="kt-inbox__author">Wolfgang Ernst Pauli</a>
                        </div>
                    </div>
                    <div class="kt-inbox__details" data-toggle="view">
                        <div class="kt-inbox__message">
                            <span class="kt-inbox__subject">About your request for PalmLake - </span>
                            <span class="kt-inbox__summary">What you requested can't be arranged ahead of time but
                                PalmLake said they'll do their best to accommodate you upon arrival....</span>
                        </div>
                        <div class="kt-inbox__labels">
                            <span
                                class="kt-inbox__label kt-badge kt-badge--unified-brand kt-badge--bold kt-badge--inline">YTG123</span>
                        </div>
                    </div>
                    <div class="kt-inbox__datetime" data-toggle="view">
                        25 May
                    </div>
                </div>
                <div class="kt-inbox__item" data-id="9" data-type="marked">
                    <div class="kt-inbox__info">
                        <div class="kt-inbox__actions">
                            <label class="kt-checkbox kt-checkbox--single kt-checkbox--tick kt-checkbox--brand">
                                <input type="checkbox">
                                <span></span>
                            </label>

                        </div>
                        <div class="kt-inbox__sender" data-toggle="view">
                            <span class="kt-media kt-media--sm"
                                style="background-image: url('assets/media/users/100_12.jpg')">
                                <span></span>
                            </span>
                            <a href="#" class="kt-inbox__author">Sarah Boysen</a>
                        </div>
                    </div>
                    <div class="kt-inbox__details" data-toggle="view">
                        <div class="kt-inbox__message">
                            <span class="kt-inbox__subject">Verification of your card transaction - </span>
                            <span class="kt-inbox__summary">This is to confirm that you have used your credit/debit card
                                for the booking. If you did not make this booking, please contact us
                                immediately....</span>
                        </div>
                        <div class="kt-inbox__labels">
                            <span
                                class="kt-inbox__label kt-badge kt-badge--unified-brand kt-badge--bold kt-badge--inline">YTG123</span>
                        </div>
                    </div>
                    <div class="kt-inbox__datetime" data-toggle="view">
                        May 23
                    </div>
                </div>
                <div class="kt-inbox__item" data-id="10" data-type="marked">
                    <div class="kt-inbox__info">
                        <div class="kt-inbox__actions">
                            <label class="kt-checkbox kt-checkbox--single kt-checkbox--tick kt-checkbox--brand">
                                <input type="checkbox">
                                <span></span>
                            </label>

                        </div>
                        <div class="kt-inbox__sender" data-toggle="view">
                            <span class="kt-media kt-media--sm kt-media--brand"
                                style="background-image: url('assets/media/users/100_14.jpg')">
                                <span></span>
                            </span>
                            <a href="#" class="kt-inbox__author">Max Born</a>
                        </div>
                    </div>
                    <div class="kt-inbox__details" data-toggle="view">
                        <div class="kt-inbox__message">
                            <span class="kt-inbox__subject">Payment Notification (DE223232034) - </span>
                            <span class="kt-inbox__summary">Your payment of 4500USD to AirCar has been authorized and
                                confirmed, thank you....</span>
                        </div>
                        <div class="kt-inbox__labels">
                            <span
                                class="kt-inbox__label kt-badge kt-badge--unified-brand kt-badge--bold kt-badge--inline">YTG123</span>
                        </div>
                    </div>
                    <div class="kt-inbox__datetime" data-toggle="view">
                        Apr 12
                    </div>
                </div>
                <div class="kt-inbox__item" data-id="11" data-type="marked">
                    <div class="kt-inbox__info">
                        <div class="kt-inbox__actions">
                            <label class="kt-checkbox kt-checkbox--single kt-checkbox--tick kt-checkbox--brand">
                                <input type="checkbox">
                                <span></span>
                            </label>

                        </div>
                        <div class="kt-inbox__sender" data-toggle="view">
                            <span class="kt-media kt-media--sm kt-media--brand"
                                style="background-image: url('assets/media/users/100_5.jpg')">
                                <span></span>
                            </span>
                            <a href="#" class="kt-inbox__author">Patty Jo Watson</a>
                        </div>
                    </div>
                    <div class="kt-inbox__details" data-toggle="view">
                        <div class="kt-inbox__message">
                            <span class="kt-inbox__subject">Welcome, Patty - </span>
                            <span class="kt-inbox__summary">Discover interesting ideas and unique perspectives. Read,
                                explore and follow your interests. Get personalized recommendations delivered to
                                you....</span>
                        </div>
                        <div class="kt-inbox__labels">
                            <span
                                class="kt-inbox__label kt-badge kt-badge--unified-brand kt-badge--bold kt-badge--inline">inbox</span>
                            <span
                                class="kt-inbox__label kt-badge kt-badge--unified-brand kt-badge--bold kt-badge--inline">YTG123</span>
                        </div>
                    </div>
                    <div class="kt-inbox__datetime" data-toggle="view">
                        Mar 1
                    </div>
                </div>
                <div class="kt-inbox__item" data-id="16" data-type="draft">
                    <div class="kt-inbox__info">
                        <div class="kt-inbox__actions">
                            <label class="kt-checkbox kt-checkbox--single kt-checkbox--tick kt-checkbox--brand">
                                <input type="checkbox">
                                <span></span>
                            </label>

                        </div>
                        <div class="kt-inbox__sender" data-toggle="view">
                            <span class="kt-media kt-media--sm kt-media--dark">
                                <span>WE</span>
                            </span>
                            <a href="#" class="kt-inbox__author">Wolfgang Ernst Pauli</a>
                        </div>
                    </div>
                    <div class="kt-inbox__details" data-toggle="view">
                        <div class="kt-inbox__message">
                            <span class="kt-inbox__subject">About your request for PalmLake - </span>
                            <span class="kt-inbox__summary">What you requested can't be arranged ahead of time but
                                PalmLake said they'll do their best to accommodate you upon arrival....</span>
                        </div>
                    </div>
                    <div class="kt-inbox__datetime" data-toggle="view">
                        25 May
                    </div>
                </div>
                <div class="kt-inbox__item" data-id="17" data-type="draft">
                    <div class="kt-inbox__info">
                        <div class="kt-inbox__actions">
                            <label class="kt-checkbox kt-checkbox--single kt-checkbox--tick kt-checkbox--brand">
                                <input type="checkbox">
                                <span></span>
                            </label>

                        </div>
                        <div class="kt-inbox__sender" data-toggle="view">
                            <span class="kt-media kt-media--sm"
                                style="background-image: url('assets/media/users/100_12.jpg')">
                                <span></span>
                            </span>
                            <a href="#" class="kt-inbox__author">Sarah Boysen</a>
                        </div>
                    </div>
                    <div class="kt-inbox__details" data-toggle="view">
                        <div class="kt-inbox__message">
                            <span class="kt-inbox__subject">Verification of your card transaction - </span>
                            <span class="kt-inbox__summary">This is to confirm that you have used your credit/debit card
                                for the booking. If you did not make this booking, please contact us
                                immediately....</span>
                        </div>
                    </div>
                    <div class="kt-inbox__datetime" data-toggle="view">
                        May 23
                    </div>
                </div>
                <div class="kt-inbox__item" data-id="18" data-type="draft">
                    <div class="kt-inbox__info">
                        <div class="kt-inbox__actions">
                            <label class="kt-checkbox kt-checkbox--single kt-checkbox--tick kt-checkbox--brand">
                                <input type="checkbox">
                                <span></span>
                            </label>

                        </div>
                        <div class="kt-inbox__sender" data-toggle="view">
                            <span class="kt-media kt-media--sm kt-media--brand"
                                style="background-image: url('assets/media/users/100_14.jpg')">
                                <span></span>
                            </span>
                            <a href="#" class="kt-inbox__author">Max Born</a>
                        </div>
                    </div>
                    <div class="kt-inbox__details" data-toggle="view">
                        <div class="kt-inbox__message">
                            <span class="kt-inbox__subject">Payment Notification (DE223232034) - </span>
                            <span class="kt-inbox__summary">Your payment of 4500USD to AirCar has been authorized and
                                confirmed, thank you....</span>
                        </div>
                    </div>
                    <div class="kt-inbox__datetime" data-toggle="view">
                        Apr 12
                    </div>
                </div>
                <div class="kt-inbox__item" data-id="19" data-type="sent">
                    <div class="kt-inbox__info">
                        <div class="kt-inbox__actions">
                            <label class="kt-checkbox kt-checkbox--single kt-checkbox--tick kt-checkbox--brand">
                                <input type="checkbox">
                                <span></span>
                            </label>

                        </div>
                        <div class="kt-inbox__sender" data-toggle="view">
                            <span class="kt-media kt-media--sm kt-media--brand"
                                style="background-image: url('assets/media/users/100_14.jpg')">
                                <span></span>
                            </span>
                            <a href="#" class="kt-inbox__author">Max Born</a>
                        </div>
                    </div>
                    <div class="kt-inbox__details" data-toggle="view">
                        <div class="kt-inbox__message">
                            <span class="kt-inbox__subject">Payment Notification (DE223232034) - </span>
                            <span class="kt-inbox__summary">Your payment of 4500USD to AirCar has been authorized and
                                confirmed, thank you....</span>
                        </div>
                    </div>
                    <div class="kt-inbox__datetime" data-toggle="view">
                        Apr 12
                    </div>
                </div>
                <div class="kt-inbox__item kt-inbox__item--unread" data-id="20" data-type="sent">
                    <div class="kt-inbox__info">
                        <div class="kt-inbox__actions">
                            <label class="kt-checkbox kt-checkbox--single kt-checkbox--tick kt-checkbox--brand">
                                <input type="checkbox">
                                <span></span>
                            </label>

                        </div>
                        <div class="kt-inbox__sender" data-toggle="view">
                            <span class="kt-media kt-media--sm kt-media--brand"
                                style="background-image: url('assets/media/users/100_5.jpg')">
                                <span></span>
                            </span>
                            <a href="#" class="kt-inbox__author">Patty Jo Watson</a>
                        </div>
                    </div>
                    <div class="kt-inbox__details" data-toggle="view">
                        <div class="kt-inbox__message">
                            <span class="kt-inbox__subject">Welcome, Patty - </span>
                            <span class="kt-inbox__summary">Discover interesting ideas and unique perspectives. Read,
                                explore and follow your interests. Get personalized recommendations delivered to
                                you....</span>
                        </div>
                    </div>
                    <div class="kt-inbox__datetime" data-toggle="view">
                        Mar 1
                    </div>
                </div>
                <div class="kt-inbox__item" data-id="21" data-type="sent">
                    <div class="kt-inbox__info">
                        <div class="kt-inbox__actions">
                            <label class="kt-checkbox kt-checkbox--single kt-checkbox--tick kt-checkbox--brand">
                                <input type="checkbox">
                                <span></span>
                            </label>

                        </div>
                        <div class="kt-inbox__sender" data-toggle="view">
                            <span class="kt-media kt-media--sm kt-media--warning">
                                <span>RW</span>
                            </span>
                            <a href="#" class="kt-inbox__author">Roberts O'Neill Wilson</a>
                        </div>
                    </div>
                    <div class="kt-inbox__details" data-toggle="view">
                        <div class="kt-inbox__message">
                            <span class="kt-inbox__subject">Optimize with Recommendations, now used by most advertisers
                                - </span>
                            <span class="kt-inbox__summary">Your weekly report is a good way to track your performance.
                                See what’s working so far and explore new opportunities for improvement....</span>
                        </div>
                    </div>
                    <div class="kt-inbox__datetime" data-toggle="view">
                        Feb 11
                    </div>
                </div>
                <div class="kt-inbox__item kt-inbox__item--unread" data-id="22" data-type="trash">
                    <div class="kt-inbox__info">
                        <div class="kt-inbox__actions">
                            <label class="kt-checkbox kt-checkbox--single kt-checkbox--tick kt-checkbox--brand">
                                <input type="checkbox">
                                <span></span>
                            </label>

                        </div>
                        <div class="kt-inbox__sender" data-toggle="view">
                            <span class="kt-media kt-media--sm kt-media--brand">
                                <span>EF</span>
                            </span>
                            <a href="#" class="kt-inbox__author">Enrico Fermi</a>
                        </div>
                    </div>
                    <div class="kt-inbox__details" data-toggle="view">
                        <div class="kt-inbox__message">
                            <span class="kt-inbox__subject">Your Order #224820998666029 has been Confirmed - </span>
                            <span class="kt-inbox__summary">Your Order #224820998666029 has been placed on Saturday, 29
                                June, 2019 10:02:41 via Online Banking...</span>
                        </div>
                    </div>
                    <div class="kt-inbox__datetime" data-toggle="view">
                        11:20PM
                    </div>
                </div>
                <div class="kt-inbox__item" data-id="23" data-type="trash">
                    <div class="kt-inbox__info">
                        <div class="kt-inbox__actions">
                            <label class="kt-checkbox kt-checkbox--single kt-checkbox--tick kt-checkbox--brand">
                                <input type="checkbox">
                                <span></span>
                            </label>

                        </div>
                        <div class="kt-inbox__sender" data-toggle="view">
                            <span class="kt-media kt-media--sm kt-media--brand"
                                style="background-image: url('assets/media/users/100_11.jpg')">
                                <span></span>
                            </span>
                            <a href="#" class="kt-inbox__author">Jane Goodall</a>
                        </div>
                    </div>
                    <div class="kt-inbox__details" data-toggle="view">
                        <div class="kt-inbox__message">
                            <span class="kt-inbox__subject">Payment Notification DLOP2329KD - </span>
                            <span class="kt-inbox__summary">Your payment of 4500USD to AirCar has been authorized and
                                confirmed, thank you...</span>
                        </div>
                    </div>
                    <div class="kt-inbox__datetime" data-toggle="view">
                        2 days ago
                    </div>
                </div>
                <div class="kt-inbox__item kt-inbox__item--unread" data-id="24" data-type="trash">
                    <div class="kt-inbox__info">
                        <div class="kt-inbox__actions">
                            <label class="kt-checkbox kt-checkbox--single kt-checkbox--tick kt-checkbox--brand">
                                <input type="checkbox">
                                <span></span>
                            </label>

                        </div>
                        <div class="kt-inbox__sender" data-toggle="view">
                            <span class="kt-media kt-media--sm kt-media--success">
                                <span>MP</span>
                            </span>
                            <a href="#" class="kt-inbox__author">Max O'Brien Planck</a>
                        </div>
                    </div>
                    <div class="kt-inbox__details" data-toggle="view">
                        <div class="kt-inbox__message">
                            <span class="kt-inbox__subject">Congratulations on your iRun Coach subscription - </span>
                            <span class="kt-inbox__summary">Congratulations on your iRun Coach subscription. You made no
                                space for excuses and you decided on a healthier and happier life...</span>
                        </div>
                    </div>
                    <div class="kt-inbox__datetime" data-toggle="view">
                        Jul 25
                    </div>
                </div>
                <div class="kt-inbox__item" data-id="25" data-type="trash">
                    <div class="kt-inbox__info">
                        <div class="kt-inbox__actions">
                            <label class="kt-checkbox kt-checkbox--single kt-checkbox--tick kt-checkbox--brand">
                                <input type="checkbox">
                                <span></span>
                            </label>

                        </div>
                        <div class="kt-inbox__sender" data-toggle="view">
                            <span class="kt-media kt-media--sm kt-media--brand"
                                style="background-image: url('assets/media/users/100_7.jpg')">
                                <span></span>
                            </span>
                            <a href="#" class="kt-inbox__author">Rita Levi-Montalcini</a>
                        </div>
                    </div>
                    <div class="kt-inbox__details" data-toggle="view">
                        <div class="kt-inbox__message">
                            <span class="kt-inbox__subject">Pay bills & win up to 600$ Cashback! - </span>
                            <span class="kt-inbox__summary">Please make sure that you have one of the following cards
                                with you when we deliver your order...</span>
                        </div>
                    </div>
                    <div class="kt-inbox__datetime" data-toggle="view">
                        July 24
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--End:: Inbox List-->

    <!--Begin:: Inbox View-->
    <div class="kt-grid__item kt-grid__item--fluid    kt-portlet    kt-inbox__view kt-inbox__view--shown-"
        id="kt_inbox_view">
        <div class="kt-portlet__head">
            <div class="kt-inbox__toolbar">
                <div class="kt-inbox__actions">
                    <a href="javascript:void(0)" class="kt-inbox__icon kt-inbox__icon--back">
                        <i class="flaticon2-left-arrow-1"></i>
                    </a>


                    <a href="#" class="kt-inbox__icon" data-toggle="kt-tooltip" title="Delete">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                            height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <rect x="0" y="0" width="24" height="24" />
                                <path
                                    d="M6,8 L6,20.5 C6,21.3284271 6.67157288,22 7.5,22 L16.5,22 C17.3284271,22 18,21.3284271 18,20.5 L18,8 L6,8 Z"
                                    fill="#000000" fill-rule="nonzero" />
                                <path
                                    d="M14,4.5 L14,4 C14,3.44771525 13.5522847,3 13,3 L11,3 C10.4477153,3 10,3.44771525 10,4 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z"
                                    fill="#000000" opacity="0.3" />
                            </g>
                        </svg> </a>

                </div>
                <div class="kt-inbox__controls">
                    <span class="kt-inbox__pages" data-toggle="kt-tooltip" title="Records per page">
                        <span class="kt-inbox__perpage" data-toggle="dropdown">3 of 230 pages</span>
                    </span>
                    <button class="kt-inbox__icon" data-toggle="kt-tooltip" title="Previose message">
                        <i class="flaticon2-left-arrow"></i>
                    </button>
                    <button class="kt-inbox__icon" data-toggle="kt-tooltip" title="Next message">
                        <i class="flaticon2-right-arrow"></i>
                    </button>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body kt-portlet__body--fit-x">
            <div class="kt-inbox__subject">
                <div class="kt-inbox__title">
                    <h3 class="kt-inbox__text">Trip Reminder. Thank you for flying with us!</h3>
                    <span class="kt-inbox__label kt-badge kt-badge--unified-brand kt-badge--bold kt-badge--inline">
                        inbox
                    </span>
                    <span class="kt-inbox__label kt-badge kt-badge--unified-danger kt-badge--bold kt-badge--inline">
                        important
                    </span>
                    <span class="kt-inbox__label kt-badge kt-badge--unified-danger kt-badge--bold kt-badge--inline">
                        12Dykj
                    </span>
                </div>

            </div>
            <div class="kt-inbox__messages">
                <div class="kt-inbox__message kt-inbox__message--expanded">
                    <div class="kt-inbox__head">
                        <span class="kt-media" data-toggle="expand"
                            style="background-image: url('assets/media/users/100_13.jpg')">
                            <span></span>
                        </span>
                        <div class="kt-inbox__info">
                            <div class="kt-inbox__author" data-toggle="expand">
                                <a href="#" class="kt-inbox__name">Chris Muller</a>
                                <div class="kt-inbox__status">
                                    <span class="kt-badge kt-badge--success kt-badge--dot"></span> 1 Day ago
                                </div>
                            </div>
                            <div class="kt-inbox__details">
                                <div class="kt-inbox__tome">
                                    <span class="kt-inbox__label" data-toggle="dropdown">
                                        to me <i class="flaticon2-down"></i>
                                    </span>
                                    <div class="dropdown-menu dropdown-menu-fit dropdown-menu-md dropdown-menu-left">
                                        <table class="kt-inbox__details">
                                            <tr>
                                                <td>from</td>
                                                <td>Mark Andre</td>
                                            </tr>
                                            <tr>
                                                <td>date:</td>
                                                <td>Jul 30, 2019, 11:27 PM</td>
                                            </tr>
                                            <tr>
                                                <td>from:</td>
                                                <td>Mark Andre</td>
                                            </tr>
                                            <tr>
                                                <td>subject:</td>
                                                <td>Trip Reminder. Thank you for flying with us!</td>
                                            </tr>
                                            <tr>
                                                <td>reply to:</td>
                                                <td>mark.andre@gmail.com</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="kt-inbox__desc" data-toggle="expand">
                                    With resrpect, i must disagree with Mr.Zinsser. We all know the most part of
                                    important part....
                                </div>
                            </div>
                        </div>
                        <div class="kt-inbox__actions">
                            <div class="kt-inbox__datetime" data-toggle="expand">
                                Jul 15, 2019, 11:19AM
                            </div>

                        </div>
                    </div>
                    <div class="kt-inbox__body">
                        <div class="kt-inbox__text">
                            <p>Hi Bob,</p>
                            <p class="kt-margin-t-25">
                                With resrpect, i must disagree with Mr.Zinsser. We all know the most part of important
                                part of any article is the title.Without a compelleing title, your reader won't even get
                                to the first sentence.After the title, however, the first few sentences of your article
                                are certainly the most important part.
                            </p>
                            <p class="kt-margin-t-25">
                                Jornalists call this critical, introductory section the "Lede," and when bridge properly
                                executed, it's the that carries your reader from an headine try at attention-grabbing to
                                the body of your blog post, if you want to get it right on of these 10 clever ways to
                                omen your next blog posr with a bang
                            </p>
                            <p class="kt-margin-t-25">
                                Best regards,
                            </p>
                            <p>
                                Jason Muller
                            </p>
                        </div>
                    </div>
                </div>
                <div class="kt-inbox__message">
                    <div class="kt-inbox__head">
                        <span class="kt-media" data-toggle="expand"
                            style="background-image: url('assets/media/users/100_10.jpg')">
                            <span></span>
                        </span>
                        <div class="kt-inbox__info">
                            <div class="kt-inbox__author" data-toggle="expand">
                                <a href="#" class="kt-inbox__name">Lina Nilson</a>
                                <div class="kt-inbox__status">
                                    <span class="kt-badge kt-badge--success kt-badge--dot"></span> 2 Day ago
                                </div>
                            </div>
                            <div class="kt-inbox__details">
                                <div class="kt-inbox__tome">
                                    <span class="kt-inbox__label" data-toggle="dropdown">
                                        to me <i class="flaticon2-down"></i>
                                    </span>
                                    <div class="dropdown-menu dropdown-menu-fit dropdown-menu-md dropdown-menu-left">
                                        <table class="kt-inbox__details">
                                            <tr>
                                                <td>from</td>
                                                <td>Mark Andre</td>
                                            </tr>
                                            <tr>
                                                <td>date:</td>
                                                <td>Jul 30, 2019, 11:27 PM</td>
                                            </tr>
                                            <tr>
                                                <td>from:</td>
                                                <td>Mark Andre</td>
                                            </tr>
                                            <tr>
                                                <td>subject:</td>
                                                <td>Trip Reminder. Thank you for flying with us!</td>
                                            </tr>
                                            <tr>
                                                <td>reply to:</td>
                                                <td>mark.andre@gmail.com</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="kt-inbox__desc" data-toggle="expand">
                                    Jornalists call this critical, introductory section the "Lede," and when bridge
                                    properly executed....
                                </div>
                            </div>
                        </div>
                        <div class="kt-inbox__actions">
                            <div class="kt-inbox__datetime" data-toggle="expand">
                                Jul 20, 2019, 03:20PM
                            </div>

                        </div>
                    </div>
                    <div class="kt-inbox__body">
                        <p>Hi,</p>
                        <p class="kt-margin-t-25">
                            The guide price is based on today's prices instore. However as we shop for your order on the
                            day you want it delivered, some of the prices may have changed. Weighed products: the guide
                            price on the website uses an estimate weight for weighed products such as grapes or cheese.
                            But what you pay will be based on the exact weight of your product, so the price may vary
                            slightly.
                        </p>
                        <p class="kt-margin-t-25">
                            Best regards,
                            <br> Jason Muller
                        </p>
                    </div>
                </div>
                <div class="kt-inbox__message">
                    <div class="kt-inbox__head">
                        <span class="kt-media" data-toggle="expand"
                            style="background-image: url('assets/media/users/100_3.jpg')">
                            <span></span>
                        </span>
                        <div class="kt-inbox__info">
                            <div class="kt-inbox__author" data-toggle="expand">
                                <a href="#" class="kt-inbox__name">Sean Stone</a>
                                <div class="kt-inbox__status">
                                    <span class="kt-badge kt-badge--success kt-badge--dot"></span> 1 Day ago
                                </div>
                            </div>
                            <div class="kt-inbox__details">
                                <div class="kt-inbox__tome">
                                    <span class="kt-inbox__label" data-toggle="dropdown">
                                        to me <i class="flaticon2-down"></i>
                                    </span>
                                    <div class="dropdown-menu dropdown-menu-fit dropdown-menu-md dropdown-menu-left">
                                        <table class="kt-inbox__details">
                                            <tr>
                                                <td>from</td>
                                                <td>Mark Andre</td>
                                            </tr>
                                            <tr>
                                                <td>date:</td>
                                                <td>Jul 30, 2019, 11:27 PM</td>
                                            </tr>
                                            <tr>
                                                <td>from:</td>
                                                <td>Mark Andre</td>
                                            </tr>
                                            <tr>
                                                <td>subject:</td>
                                                <td>Trip Reminder. Thank you for flying with us!</td>
                                            </tr>
                                            <tr>
                                                <td>reply to:</td>
                                                <td>mark.andre@gmail.com</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="kt-inbox__desc" data-toggle="expand">
                                    Headine try at attention-grabbing to the body of your blog post....
                                </div>
                            </div>
                        </div>
                        <div class="kt-inbox__actions">
                            <div class="kt-inbox__datetime">
                                Jul 15, 2019, 11:19AM
                            </div>

                        </div>
                    </div>
                    <div class="kt-inbox__body">
                        <p>Hi Bob,</p>
                        <p class="kt-margin-t-25">
                            With resrpect, i must disagree with Mr.Zinsser. We all know the most part of important part
                            of any article is the title.Without a compelleing title, your reader won't even get to the
                            first sentence.After the title, however, the first few sentences of your article are
                            certainly the most important part.
                        </p>
                        <p class="kt-margin-t-25">
                            Jornalists call this critical, introductory section the "Lede," and when bridge properly
                            executed, it's the that carries your reader from an headine try at attention-grabbing to the
                            body of your blog post, if you want to get it right on of these 10 clever ways to omen your
                            next blog posr with a bang
                        </p>
                        <p class="kt-margin-t-25">
                            Best regards,
                        </p>
                        <p>
                            Jason Muller
                        </p>
                    </div>
                </div>
            </div>
            <div class="row trans-reply-row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <button class="btn btn-primary " id="reply-btn">Reply</button>
                </div>

            </div>
            <div class="kt-inbox__reply kt-inbox__reply--on" id="reply-section">
                <div class="kt-inbox__actions">
                    <button class="btn btn-secondary btn-bold">
                        <i class="flaticon2-reply-1 kt-font-brand"></i> Reply
                    </button>
                    <button class="btn btn-secondary btn-bold">
                        <i class="flaticon2-left-arrow-1 kt-font-brand"></i> Forward
                    </button>
                </div>
                <div class="kt-inbox__form" id="kt_inbox_reply_form">
                    <div class="kt-inbox__body">
                        <div class="kt-inbox__to">
                            <div class="kt-inbox__wrapper">
                                <div class="kt-inbox__field kt-inbox__field--to">
                                    <div class="kt-inbox__label">
                                        To:
                                    </div>
                                    <div class="kt-inbox__input">
                                        <input type="text" name="compose_to" value="Chris Muller, Lina Nilson">
                                    </div>
                                    <div class="kt-inbox__tools">
                                        <span class="kt-inbox__tool kt-inbox__tool--cc">Cc</span>
                                        <span class="kt-inbox__tool kt-inbox__tool--bcc">Bcc</span>
                                    </div>
                                </div>
                                <div class="kt-inbox__field kt-inbox__field--cc">
                                    <div class="kt-inbox__label">
                                        Cc:
                                    </div>
                                    <div class="kt-inbox__input">
                                        <input type="text" name="compose_cc">
                                    </div>
                                    <div class="kt-inbox__tools">
                                        <button type="button"
                                            class="kt-inbox__icon kt-inbox__icon--delete kt-inbox__icon--sm kt-inbox__icon--light">
                                            <i class="flaticon2-cross"></i>
                                        </button>
                                    </div>
                                </div>
                                <div class="kt-inbox__field kt-inbox__field--bcc">
                                    <div class="kt-inbox__label">
                                        Bcc:
                                    </div>
                                    <div class="kt-inbox__input">
                                        <input type="text" name="compose_bcc">
                                    </div>
                                    <div class="kt-inbox__tools">
                                        <button type="button"
                                            class="kt-inbox__icon kt-inbox__icon--delete kt-inbox__icon--sm kt-inbox__icon--light">
                                            <i class="flaticon2-cross"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <hr style="margin-bottom: 0px;">
                        <div class="kt-inbox__to">
                            <div class="kt-inbox__wrapper">
                                <div class="kt-inbox__field kt-inbox__field--to">
                                    <div class="kt-inbox__label">
                                        Subject:
                                    </div>
                                    <div class="kt-inbox__input" style="padding-left: 20px;">
                                        <input type="text" name="compose_to" value="">
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="kt-inbox__editor" id="kt_inbox_reply_editor" style="height: 200px;">
                        </div>
                        <div class="kt-inbox__attachments">
                            <div class="dropzone dropzone-multi" id="kt_inbox_reply_attachments">
                                <div class="dropzone-items">
                                    <div class="dropzone-item" style="display:none">
                                        <div class="dropzone-file">
                                            <div class="dropzone-filename" title="some_image_file_name.jpg">
                                                <span data-dz-name>some_image_file_name.jpg</span> <strong>(<span
                                                        data-dz-size>340kb</span>)</strong>
                                            </div>
                                            <div class="dropzone-error" data-dz-errormessage></div>
                                        </div>
                                        <div class="dropzone-progress">
                                            <div class="progress">
                                                <div class="progress-bar kt-bg-brand" role="progressbar"
                                                    aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"
                                                    data-dz-uploadprogress></div>
                                            </div>
                                        </div>
                                        <div class="dropzone-toolbar">
                                            <span class="dropzone-delete" data-dz-remove><i
                                                    class="flaticon2-cross"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="kt-inbox__foot">
                        <div class="kt-inbox__primary">
                            <div class="btn-group">
                                <button type="button" class="btn btn-brand btn-bold">
                                    Send
                                </button>
                                <button type="button"
                                    class="btn btn-brand btn-bold dropdown-toggle dropdown-toggle-split"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                </button>
                                <div class="dropdown-menu dropup dropdown-menu-fit dropdown-menu-right">
                                    <ul class="kt-nav">
                                        <li class="kt-nav__item">
                                            <a href="#" class="kt-nav__link">
                                                <i class="kt-nav__link-icon flaticon2-writing"></i>
                                                <span class="kt-nav__link-text">Schedule Send</span>
                                            </a>
                                        </li>
                                        <li class="kt-nav__item">
                                            <a href="#" class="kt-nav__link">
                                                <i class="kt-nav__link-icon flaticon2-hourglass-1"></i>
                                                <span class="kt-nav__link-text">Cancel</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--End:: Inbox View-->
</div>

<!--End::Inbox-->

<!--Begin:: Inbox Compose-->
<div class="modal modal-sticky-bottom-right modal-sticky-lg" id="kt_inbox_compose" role="dialog" data-backdrop="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content kt-inbox">
            <div class="kt-inbox__form" id="kt_inbox_compose_form">
                <div class="kt-inbox__head">
                    <div class="kt-inbox__title">Compose</div>
                    <div class="kt-inbox__actions">
                        <button type="button" class="kt-inbox__icon kt-inbox__icon--md kt-inbox__icon--light">
                            <i class="flaticon2-arrow-1"></i>
                        </button>
                        <button type="button" class="kt-inbox__icon kt-inbox__icon--md kt-inbox__icon--light"
                            data-dismiss="modal">
                            <i class="flaticon2-cross"></i>
                        </button>
                    </div>
                </div>
                <div class="kt-inbox__body">
                    <div class="kt-inbox__to">
                        <div class="kt-inbox__wrapper">
                            <div class="kt-inbox__field kt-inbox__field--to">
                                <div class="kt-inbox__label">
                                    To:
                                </div>
                                <div class="kt-inbox__input">
                                    <input type="text" name="compose_to" value="Chris Muller, Lina Nilson">
                                </div>
                                <div class="kt-inbox__tools">
                                    <span class="kt-inbox__tool kt-inbox__tool--cc">Cc</span>
                                    <span class="kt-inbox__tool kt-inbox__tool--bcc">Bcc</span>
                                </div>
                            </div>
                            <div class="kt-inbox__field kt-inbox__field--cc">
                                <div class="kt-inbox__label">
                                    Cc:
                                </div>
                                <div class="kt-inbox__input">
                                    <input type="text" name="compose_cc">
                                </div>
                                <div class="kt-inbox__tools">
                                    <button type="button"
                                        class="kt-inbox__icon kt-inbox__icon--delete kt-inbox__icon--sm kt-inbox__icon--light">
                                        <i class="flaticon2-cross"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="kt-inbox__field kt-inbox__field--bcc">
                                <div class="kt-inbox__label">
                                    Bcc:
                                </div>
                                <div class="kt-inbox__input">
                                    <input type="text" name="compose_bcc">
                                </div>
                                <div class="kt-inbox__tools">
                                    <button type="button"
                                        class="kt-inbox__icon kt-inbox__icon--delete kt-inbox__icon--sm kt-inbox__icon--light">
                                        <i class="flaticon2-cross"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="kt-inbox__subject">
                        <input class="form-control" name="compose_subject" placeholder="Subject">
                    </div>
                    <div class="kt-inbox__editor" id="kt_inbox_compose_editor" style="height: 300px">
                    </div>
                    <div class="kt-inbox__attachments">
                        <div class="dropzone dropzone-multi" id="kt_inbox_compose_attachments">
                            <div class="dropzone-items">
                                <div class="dropzone-item" style="display:none">
                                    <div class="dropzone-file">
                                        <div class="dropzone-filename" title="some_image_file_name.jpg">
                                            <span data-dz-name>some_image_file_name.jpg</span> <strong>(<span
                                                    data-dz-size>340kb</span>)</strong>
                                        </div>
                                        <div class="dropzone-error" data-dz-errormessage></div>
                                    </div>
                                    <div class="dropzone-progress">
                                        <div class="progress">
                                            <div class="progress-bar kt-bg-brand" role="progressbar" aria-valuemin="0"
                                                aria-valuemax="100" aria-valuenow="0" data-dz-uploadprogress></div>
                                        </div>
                                    </div>
                                    <div class="dropzone-toolbar">
                                        <span class="dropzone-delete" data-dz-remove><i
                                                class="flaticon2-cross"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="kt-inbox__foot">
                    <div class="kt-inbox__primary">
                        <div class="btn-group">
                            <button type="button" class="btn btn-brand btn-bold">
                                Send
                            </button>
                            <button type="button" class="btn btn-brand btn-bold dropdown-toggle dropdown-toggle-split"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            </button>
                            <div class="dropdown-menu dropup dropdown-menu-fit dropdown-menu-right">
                                <ul class="kt-nav">
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon flaticon2-writing"></i>
                                            <span class="kt-nav__link-text">Schedule Send</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon flaticon2-medical-records"></i>
                                            <span class="kt-nav__link-text">Save & archive</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon flaticon2-hourglass-1"></i>
                                            <span class="kt-nav__link-text">Cancel</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="kt-inbox__panel">
                            <label class="kt-inbox__icon kt-inbox__icon--light"
                                id="kt_inbox_compose_attachments_select">
                                <i class="flaticon2-clip-symbol"></i>
                            </label>
                            <label class="kt-inbox__icon kt-inbox__icon--light">
                                <i class="flaticon2-pin"></i>
                            </label>
                        </div>
                    </div>
                    <div class="kt-inbox__secondary">
                        <button class="kt-inbox__icon kt-inbox__icon--light" data-toggle="kt-tooltip"
                            title="More actions">
                            <i class="flaticon2-settings"></i>
                        </button>
                        <button class="kt-inbox__icon kt-inbox__icon--remove kt-inbox__icon--light"
                            data-toggle="kt-tooltip" title="Dismiss reply">
                            <i class="flaticon2-rubbish-bin-delete-button"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--End:: Inbox Compose-->

@endsection

@push('scripts')
<script src="{{asset('assets/js/pages/custom/inbox/inbox.js')}}" type="text/javascript"></script>
<script>
$(function() {
    $("#reply-section").hide()

})
$("#reply-btn").on('click', function() {
    $("#reply-section").show()
})
</script>

<script>
var KTDatatablesDataSourceHtml = function() {

    var initTable1 = function() {
        var table = $('#kt_table_1');

        // begin first table
        table.DataTable({
            responsive: true,

        });

    };

    return {

        //main function to initiate the module
        init: function() {
            initTable1();
        },

    };

}();

jQuery(document).ready(function() {
    KTDatatablesDataSourceHtml.init();


});
</script>


@endpush
