@extends('operations.layouts.master')
@section('title','Admin Area')
@push('styles')
<link href="{{asset('assets/css/pages/wizard/wizard-1.css')}}" rel="stylesheet" type="text/css" />

@endpush

@section('content')

    <div class="kt-portlet kt-portlet--tabs">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Project Update | {{$project->projectID}}
                    <span class="trans-status trans-green-bg">{{$project->status}} project</span>
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <ul class="nav nav-tabs nav-tabs-bold nav-tabs-line nav-tabs-line-right nav-tabs-line-brand"
                    role="tablist">

                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#kt_portlet_tab_1_2" role="tab">
                            Step 1
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#kt_portlet_tab_1_3" role="tab">
                            Step 2
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#kt_portlet_tab_1_4" role="tab">
                            Step 3
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div class="tab-content">
                <form method="POST" action="{{route('projects.update',$project->id)}}">
                    {{ method_field('PUT') }}
                    @csrf
                    <div class="tab-pane active" id="kt_portlet_tab_1_2">
                        <div class="kt-wizard-v1__content" data-ktwizard-type="step-content">
                            <div class="kt-section kt-section--first">
                                <div class="kt-wizard-v1__form">
                                    <div class="row">
                                        <div class="col-xl-12">
                                            <div class="kt-section__body">
                                                <div class="form-group row">
                                                    <div class="col-lg-9 col-xl-6">
                                                        <h3 class="kt-section__title kt-section__title-md">Project Details</h3>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-lg-9 col-xl-9">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend"></div>
                                                            <input name="project_id" type="text" class="form-control"
                                                                value="{{$project->projectID}}" placeholder="Project ID"
                                                                aria-describedby="basic-addon1">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="trans-project-lang-container">
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">Source Language</label>
                                                        <div class="col-lg-9 col-xl-9">
                                                            <select class="form-control" name="from_lang_id">
                                                                <option>Select Language...</option>
                                                                @foreach($languages as $language)
                                                                <option value="{{$language->id}}" @if($project->from_lang_id == $language->id) selected @endif>{{$language->name}}
                                                                </option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">Target Language</label>
                                                        <div class="col-lg-9 col-xl-9">
                                                            <select class="form-control" name="to_lang_id">
                                                                <option>Select Language...</option>
                                                                @foreach($languages as $language)
                                                                <option value="{{$language->id}}" @if($project->to_lang_id == $language->id) selected @endif>{{$language->name}}
                                                                </option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <!-- <div>
                                                        <p class="trans-underline trans-font-bold">Add More+</p>
                                                    </div> -->
                                                </div>
                                                <div class="form-group form-group-last row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">Certification required?
                                                    </label>
                                                    <div class="col-lg-9 col-xl-6">
                                                        <div class="">
                                                            <label class="kt-checkbox">
                                                                <input name="is_certification_required" type="checkbox"
                                                                    @if($project->is_certification_required) checked="" @endif>
                                                                <span></span>
                                                            </label>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group form-group-last row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">CAT tool required?
                                                    </label>
                                                    <div class="col-lg-9 col-xl-6">
                                                        <div class="">
                                                            <label class="kt-checkbox">
                                                                <input value="1" name="is_cat_tool_required" type="checkbox"
                                                                    @if($project->is_cat_tool_required) checked="" @endif>
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">Software name</label>
                                                    <div class="col-lg-9 col-xl-9">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend"></div>
                                                            <input name="software_name" value="{{$project->software_name}}" type="text" class="form-control"
                                                                placeholder="Software name" aria-describedby="basic-addon1">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group form-group-last row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">Retyping required?
                                                    </label>
                                                    <div class="col-lg-9 col-xl-6">
                                                        <div class="">
                                                            <label class="kt-checkbox">
                                                            <input name="is_retyping_required" type="checkbox"
                                                            @if($project->is_retyping_required) checked="" @endif>
                                                                <span></span>
                                                            </label>

                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group form-group-last row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">DTP required?
                                                    </label>
                                                    <div class="col-lg-9 col-xl-6">
                                                        <div class="">
                                                            <label class="kt-checkbox">
                                                            <input name="is_dtp_required" type="checkbox"
                                                            @if($project->is_dtp_required) checked="" @endif>
                                                                <span></span>
                                                            </label>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">number of pages</label>
                                                    <div class="col-lg-9 col-xl-9">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend"></div>
                                                            <input name="number_of_pages" value="{{$project->number_of_pages}}" type="number" class="form-control"
                                                                placeholder="Number of pages" aria-describedby="basic-addon1">
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="form-group form-group-last row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">Formatting required?
                                                    </label>
                                                    <div class="col-lg-9 col-xl-6">
                                                        <div class="">
                                                            <label class="kt-checkbox">
                                                            <input name="is_formating_required" type="checkbox"
                                                            @if($project->is_formating_required) checked="" @endif>
                                                                <span></span>
                                                            </label>

                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">DTP rate/page</label>
                                                    <div class="col-lg-9 col-xl-9">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend"></div>
                                                            <input name="dtp_range" value="{{$project->dtp_range}}" type="number" class="form-control"
                                                                placeholder="DTP rate/page" aria-describedby="basic-addon1">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">Count</label>
                                                    <div class="col-lg-9 col-xl-9">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend"></div>
                                                            <input name="count" value="{{$project->count}}" type="number" class="form-control" placeholder="Count"
                                                                aria-describedby="basic-addon1">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">Count By</label>
                                                    <div class="col-lg-9 col-xl-9">
                                                        <select name="count_by" class="form-control">
                                                            <option value="page" @if($project->count_by == "page") selected @endif>Page</option>
                                                            <option value="word" @if($project->count_by == "word") selected @endif>Word</option>
                                                            <option value="minute" @if($project->count_by == "minute") selected @endif>minute</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">Count @</label>
                                                    <div class="col-lg-9 col-xl-9">
                                                        <select  name="count_at" class="form-control">
                                                            <option value="source" @if($project->count_at == "source") selected @endif>Source</option>
                                                            <option value="target" @if($project->count_at == "target") selected @endif>Target</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">Client Rate</label>
                                                    <div class="col-lg-9 col-xl-9">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend"></div>
                                                            <input name="client_rate" value="{{$project->client_rate}}" type="number" class="form-control" placeholder="Client Rate"
                                                                aria-describedby="basic-addon1">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">Change Project
                                                        Status</label>
                                                    <div class="col-lg-9 col-xl-9">
                                                        <select class="form-control">
                                                            <option value="option1">Pending</option>
                                                            <option value="option2">Delivered </option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="kt_portlet_tab_1_3">

                        <div class="kt-wizard-v1__content" data-ktwizard-type="step-content">
                            <div class="kt-form__section kt-form__section--first">
                                <div class="kt-wizard-v1__form">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="kt-section__body">

                                                <div class="form-group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">Client Name</label>
                                                    <div class="col-lg-9 col-xl-9">
                                                        <select name="client_id" class="form-control">
                                                            @foreach($clients as $client)
                                                            <option value="{{$client->id}}" @if($project->client_id == $client->id) selected @endif>{{$client->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">Agent Name</label>
                                                    <div class="col-lg-9 col-xl-9">
                                                        <select name="agent_id" class="form-control">
                                                            @foreach($agents as $agent)
                                                            <option value="{{$agent->id}}" @if($project->agent_id == $agent->id) selected @endif>{{$agent->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">Multiple File Upload</label>
                                                    <div class="col-lg-9 col-xl-9">
                                                        <div class="dropzone dropzone-default dropzone-brand"
                                                            id="kt_dropzone_2">
                                                            <div class="dropzone-msg dz-message needsclick">
                                                                <h3 class="dropzone-msg-title">Drop files here or click to
                                                                    upload.</h3>
                                                                <span class="dropzone-msg-desc">Upload up to 10 files</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-form-label col-lg-3 col-sm-12">Client deadline</label>
                                                    <div class="col-lg-9 col-xl-9">
                                                        <div class="input-group date">
                                                            <input name="client_deadline" type="text" class="form-control" readonly=""
                                                                placeholder="Select date &amp; time" id="kt_datetimepicker_2">
                                                            <div class="input-group-append">
                                                                <span class="input-group-text"><i
                                                                        class="la la-calendar-check-o glyphicon-th"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-form-label col-lg-3 col-sm-12">Time Zone</label>
                                                    <div class="col-lg-9 col-xl-9">
                                                    <select name="timezone" class="form-control kt-select2" id="kt_select2"
                                                                    >
                                                        @foreach($timezones as $timezone)
                                                        <option value="{{$timezone}}">{{$timezone}}</option>
                                                        @endforeach
                                                    </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="tab-pane" id="kt_portlet_tab_1_4">
                        <div class="kt-wizard-v1__content" data-ktwizard-type="step-content">
                            <div class="kt-form__section kt-form__section--first">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="kt-section__body">
                                            <div class="trans-project-lang-container" style="    max-height: 397px;
                                                                                overflow-y: scroll;">

                                                @if(count($project->notes))
                                                    @foreach($project->notes as $note)
                                                        <div class="form-group row">
                                                            <label class="col-xl-3 col-lg-3 col-form-label">Assign to
                                                            </label>
                                                            <div class="col-lg-9 col-xl-9">
                                                                <select name="assign[{{$loop->index}}][assigned_to]" class="form-control">
                                                                    @foreach($managers as $manager)
                                                                        <option value="{{$manager->id}}" @if($note->assigned_to == $manager->id) selected @endif>{{$manager->name}} - {{$manager->role}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-xl-3 col-lg-3 col-form-label">Notes
                                                            </label>
                                                            <div class="col-lg-9 col-xl-9">
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend"></div>
                                                                    <textarea name="assign[{{$loop->index}}][notes]" class="form-control" placeholder="Notes
                                                                                                " aria-describedby="basic-addon1">{{$note->notes}}</textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                <div id="assign_section"></div>

                                                <div>
                                                    <p class="trans-underline trans-font-bold trans-cursor-pointer"
                                                        onclick="addMoreAssign('{{count($project->notes)}}')">Add More+</p>
                                                </div>
                                            @endif
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Request total in USD
                                                </label>
                                                <div class="col-lg-9 col-xl-9">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend"></div>
                                                        <input name="request_total" value="{{$project->request_total}}" type="number" class="form-control" placeholder="Request total in USD
                                                                                    " aria-describedby="basic-addon1">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Clients guidelines
                                                </label>
                                                <div class="col-lg-9 col-xl-9">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend"></div>
                                                        <textarea name="client_guidelines" class="form-control" placeholder="Clients guidelines
                                                                                    "
                                                            aria-describedby="basic-addon1"></textarea>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <button type="submit" class="btn btn-success">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <div class="alert alert-light alert-elevate fade show" role="alert">
                <div class="alert-text">
                    <div class="row">
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="trans-project-service-type-container trans-cursor-pointer">
                                <h3 style="font-size:16px">Download Source Files</h3>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="trans-project-service-type-container trans-cursor-pointer">
                                <h3 style="font-size:16px">Download Target Files</h3>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <a href="{{route('operations.delivery_mail',$project->id)}}">
                                <div class="trans-project-service-type-container trans-cursor-pointer">
                                    <h3 style="font-size:16px; color:black">Create delivery mail</h3>
                                </div>

                            </a>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <a href="./users.html">
                                <div class="trans-project-service-type-container trans-cursor-pointer">
                                    <h3 style="font-size:16px; color:black">Add / view comments for LC/ PM
                                    </h3>
                                </div>
                            </a>

                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <a href="{{route('operations.project_activity',$project->id)}}">
                                <div class="trans-project-service-type-container trans-cursor-pointer">
                                    <h3 style="font-size:16px; color:black">Project activity
                                    </h3>
                                </div>
                            </a>
                        </div>
                        @if($project->status =='delivered')
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="trans-project-service-type-container trans-cursor-pointer" id="updateStatus">
                                <h3 style="font-size:16px">Update project status
                                </h3>
                            </div>
                        </div>
                        @endif
                    </div>

                </div>
            </div>
        </div>

    </div>


@endsection

@push('scripts')

<!--end::Global Theme Bundle -->


<!--end::Page Vendors -->

<!--begin::Page Scripts(used by this page) -->
<script src="{{asset('assets/js/pages/crud/forms/widgets/bootstrap-datepicker.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/js/pages/custom/projects/add-project.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/js/pages/crud/file-upload/dropzonejs.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/js/pages/crud/forms/widgets/bootstrap-datetimepicker.js')}}" type="text/javascript">
</script>
<script src="{{asset('assets/js/pages/components/extended/sweetalert2.js')}}" type="text/javascript"></script>
<script>
$(function() {
    $('#kt_sweetalert_trans').click(function(e) {
        swal.fire("Thank you!", "The request has been successfully submitted!", "success")
            .then(result => {
                if (result.value) {
                    window.location.replace('{{route('projects.index')}}')
                }
            })
    });

})
</script>>

<script>
function addMoreAssign(count) {
    var i = parseInt(count) + 1;
    $("#assign_section").append(`
				<hr>
				<div class="form-group row">
					<label class="col-xl-3 col-lg-3 col-form-label">Assign to
					</label>
					<div class="col-lg-9 col-xl-9">
						<select class="form-control" name=assign[${i}][assigned_to]>
                            @foreach($managers as $manager)
                                <option value="{{$manager->id}}">{{$manager->name}} - {{$manager->role}}</option>
                            @endforeach
                        </select>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-xl-3 col-lg-3 col-form-label">Notes
					</label>
					<div class="col-lg-9 col-xl-9">
						<div class="input-group">
							<div class="input-group-prepend"></div>
							<textarea name="assign[${i}][notes]" class="form-control" placeholder="Notes
							" aria-describedby="basic-addon1"></textarea>
						</div>
					</div>
				</div>
                `)
    i++;
}
</script>

<script>
            $(function(){
                $('#updateStatus').click(function(e) {
                    Swal.fire({
                        title: 'Are you sure?',
                        text: "Change status to completed",
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes'
                        }).then((result) => {
                        if (result.value) {
                            window.location.replace('{{route('operations.project_compelete',$project->id)}}')
                        }
                        })        });

            })
        </script>
@endpush
