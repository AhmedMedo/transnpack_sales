@extends('operations.layouts.master')
@section('title','Admin Area')
@push('styles')
<link href="{{asset('assets/css/pages/wizard/wizard-1.css')}}" rel="stylesheet" type="text/css" />
<style>
/* #services  input{
    visibility: hidden;
    position: absolute;
}

#services  input + .trans-project-service-type-container{
    cursor:pointer;
    border:2px solid transparent;

}
#services  input:checked + .trans-project-service-type-container{

    background-color: #ffd6bb;
    border: 1px solid #ff6600;
} */

.labl {
    display: block;
    width: 400px;
}

.labl>input {
    /* HIDE RADIO */
    visibility: hidden;
    /* Makes input not-clickable */
    position: absolute;
    /* Remove input from document flow */
}

.labl>input+div {
    /* DIV STYLES */
    cursor: pointer;
    border: 2px solid transparent;
}

.labl>input:checked+div {
    /* (RADIO CHECKED) DIV STYLES */
    background-color: #ffd6bb;
    border: 1px solid #ff6600;
}
</style>
@endpush

@section('content')
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet">
        <div class="kt-portlet__body kt-portlet__body--fit">
            <div class="kt-grid  kt-wizard-v1 kt-wizard-v1--white" id="kt_projects_add"
                data-ktwizard-state="step-first">
                <div class="kt-grid__item">

                    <!--begin: Form Wizard Nav -->
                    <div class="kt-wizard-v1__nav">
                        <div class="kt-wizard-v1__nav-items">

                            <!--doc: Replace A tag with SPAN tag to disable the step link click -->
                            <div class="kt-wizard-v1__nav-item" data-ktwizard-type="step" data-ktwizard-state="current">
                                <div class="kt-wizard-v1__nav-body">

                                    <div class="kt-wizard-v1__nav-label">
                                        Step 1
                                    </div>
                                </div>
                            </div>
                            <div class="kt-wizard-v1__nav-item" data-ktwizard-type="step">
                                <div class="kt-wizard-v1__nav-body">

                                    <div class="kt-wizard-v1__nav-label">
                                        Step 2
                                    </div>
                                </div>
                            </div>
                            <div class="kt-wizard-v1__nav-item" data-ktwizard-type="step">
                                <div class="kt-wizard-v1__nav-body">
                                    <div class="kt-wizard-v1__nav-label">
                                        Step 3
                                    </div>
                                </div>
                            </div>
                            <div class="kt-wizard-v1__nav-item" data-ktwizard-type="step">
                                <div class="kt-wizard-v1__nav-body">

                                    <div class="kt-wizard-v1__nav-label">
                                        Step 4
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--end: Form Wizard Nav -->
                </div>
                <div class="kt-grid__item kt-grid__item--fluid kt-wizard-v1__wrapper">

                    <!--begin: Form Wizard Form-->
                    <form class="kt-form" id="kt_projects_add_form" action="{{route('projects.store')}}">

                        <!--begin: Form Wizard Step 1-->
                        <div class="kt-wizard-v1__content" data-ktwizard-type="step-content"
                            data-ktwizard-state="current">
                            <div class="kt-heading kt-heading--md">Choose Service :</div>
                            <div class="kt-section kt-section--first">
                                <div class="kt-wizard-v1__form">
                                    <div class="row" id="services">
                                        @foreach($services as $service)
                                        <div class="col-md-4 col-sm-4 col-xs-12">
                                            <input type="radio" name="service" value="{{$service->id}}"
                                                id="r{{$loop->iteration}}" checked />
                                            <div class="trans-project-service-type-container">
                                                <h3 style="font-size:16px">{{$service->name}}</h3>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!--end: Form Wizard Step 1-->

                        <!--begin: Form Wizard Step 2-->
                        <div class="kt-wizard-v1__content" data-ktwizard-type="step-content">
                            <div class="kt-section kt-section--first">
                                <div class="kt-wizard-v1__form">
                                    <div class="row">
                                        <div class="col-xl-12">
                                            <div class="kt-section__body">
                                                <div class="form-group row">
                                                    <div class="col-lg-9 col-xl-6">
                                                        <h3 class="kt-section__title kt-section__title-md">Project
                                                            Details</h3>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">Project ID</label>
                                                    <div class="col-lg-9 col-xl-9">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend"></div>
                                                            <input type="text" name="project_id" class="form-control"
                                                                value="1112kjjll" placeholder="Email"
                                                                aria-describedby="basic-addon1">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="trans-project-lang-container">
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">Source
                                                            Language</label>
                                                        <div class="col-lg-9 col-xl-9">
                                                            <select class="form-control" name="from_lang_id">
                                                                <option>Select Language...</option>
                                                                @foreach($languages as $language)
                                                                <option value="{{$language->id}}">{{$language->name}}
                                                                </option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">Target
                                                            Language</label>
                                                        <div class="col-lg-9 col-xl-9">
                                                            <select class="form-control" name="to_lang_id">
                                                                <option>Select Language...</option>
                                                                @foreach($languages as $language)
                                                                <option value="{{$language->id}}">{{$language->name}}
                                                                </option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <!-- <div>
                                                        <p class="trans-underline trans-font-bold">Add More+</p>
                                                    </div> -->
                                                </div>
                                                <div class="form-group form-group-last row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">Certification
                                                        required?
                                                    </label>
                                                    <div class="col-lg-9 col-xl-6">
                                                        <div class="">
                                                            <label class="kt-checkbox">
                                                                <input value="1" name="is_certification_required"
                                                                    type="checkbox" checked="">
                                                                <span></span>
                                                            </label>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group form-group-last row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">CAT tool required?
                                                    </label>
                                                    <div class="col-lg-9 col-xl-6">
                                                        <div class="">
                                                            <label class="kt-checkbox">
                                                                <input value="1" name="is_cat_tool_required"
                                                                    type="checkbox" checked="">
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">Software
                                                        name</label>
                                                    <div class="col-lg-9 col-xl-9">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend"></div>
                                                            <input name="software_name" type="text" class="form-control"
                                                                placeholder="Software name"
                                                                aria-describedby="basic-addon1">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group form-group-last row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">Retyping required?
                                                    </label>
                                                    <div class="col-lg-9 col-xl-6">
                                                        <div class="">
                                                            <label class="kt-checkbox">
                                                                <input name="is_retyping_required" type="checkbox"
                                                                    checked="">
                                                                <span></span>
                                                            </label>

                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group form-group-last row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">DTP required?
                                                    </label>
                                                    <div class="col-lg-9 col-xl-6">
                                                        <div class="">
                                                            <label class="kt-checkbox">
                                                                <input name="is_dtp_required" type="checkbox"
                                                                    checked="">
                                                                <span></span>
                                                            </label>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">number of
                                                        pages</label>
                                                    <div class="col-lg-9 col-xl-9">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend"></div>
                                                            <input name="number_of_pages" type="number"
                                                                class="form-control" placeholder="Number of pages"
                                                                aria-describedby="basic-addon1">
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="form-group form-group-last row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">Formatting required?
                                                    </label>
                                                    <div class="col-lg-9 col-xl-6">
                                                        <div class="">
                                                            <label class="kt-checkbox">
                                                                <input name="is_formating_required" type="checkbox"
                                                                    checked="">
                                                                <span></span>
                                                            </label>

                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">DTP
                                                        rate/page</label>
                                                    <div class="col-lg-9 col-xl-9">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend"></div>
                                                            <input name="dtp_range" type="number" class="form-control"
                                                                placeholder="DTP rate/page"
                                                                aria-describedby="basic-addon1">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">Count</label>
                                                    <div class="col-lg-9 col-xl-9">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend"></div>
                                                            <input name="count" type="number" class="form-control"
                                                                placeholder="Count" aria-describedby="basic-addon1">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">Count By</label>
                                                    <div class="col-lg-9 col-xl-9">
                                                        <select name="count_by" class="form-control">
                                                            <option value="page">Page</option>
                                                            <option value="word">Word</option>
                                                            <option value="minute">minute</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">Count @</label>
                                                    <div class="col-lg-9 col-xl-9">
                                                        <select name="count_at" class="form-control">
                                                            <option value="source">Source</option>
                                                            <option value="target">Target</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">Client Rate</label>
                                                    <div class="col-lg-9 col-xl-9">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend"></div>
                                                            <input name="client_rate" type="number" class="form-control"
                                                                placeholder="Client Rate"
                                                                aria-describedby="basic-addon1">
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!--end: Form Wizard Step 2-->

                        <!--begin: Form Wizard Step 3-->
                        <div class="kt-wizard-v1__content" data-ktwizard-type="step-content">
                            <div class="kt-heading kt-heading--md">Setup Your Address</div>
                            <div class="kt-form__section kt-form__section--first">
                                <div class="kt-wizard-v1__form">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="kt-section__body">

                                                <div class="form-group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">Client Name</label>
                                                    <div class="col-lg-9 col-xl-9">
                                                        <select name="client_id" class="form-control">
                                                            @foreach($clients as $client)
                                                            <option value="{{$client->id}}">{{$client->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">Agent Name</label>
                                                    <div class="col-lg-9 col-xl-9">
                                                        <select name="agent_id" class="form-control">
                                                            @foreach($agents as $agent)
                                                            <option value="{{$agent->id}}">{{$agent->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">Multiple File
                                                        Upload</label>
                                                    <div class="col-lg-9 col-xl-9">
                                                        <div class="dropzone dropzone-default dropzone-brand"
                                                            id="file_upload">
                                                            <div class="dropzone-msg dz-message needsclick">
                                                                <h3 class="dropzone-msg-title">Drop files here or click
                                                                    to upload.</h3>
                                                                <span class="dropzone-msg-desc">Upload up to 10
                                                                    files</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="file_pathes">
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-form-label col-lg-3 col-sm-12">Client
                                                        deadline</label>
                                                    <div class="col-lg-9 col-xl-9">
                                                        <div class="input-group date">
                                                            <input name="client_deadline" type="text"
                                                                class="form-control" readonly=""
                                                                placeholder="Select date &amp; time"
                                                                id="kt_datetimepicker_2">
                                                            <div class="input-group-append">
                                                                <span class="input-group-text"><i
                                                                        class="la la-calendar-check-o glyphicon-th"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-form-label col-lg-3 col-sm-12">Time Zone</label>
                                                    <div class="col-lg-9 col-xl-9">
                                                        <select name="timezone" class="form-control kt-select2"
                                                            id="kt_select2">
                                                            @foreach($timezones as $timezone)
                                                            <option value="{{$timezone}}">{{$timezone}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>



                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <!--end: Form Wizard Step 3-->

                        <!--begin: Form Wizard Step 4-->
                        <div class="kt-wizard-v1__content" data-ktwizard-type="step-content">
                            <div class="kt-heading kt-heading--md">Step 4</div>
                            <div class="kt-form__section kt-form__section--first">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="kt-section__body">
                                            <div class="trans-project-lang-container" style="    max-height: 397px;
																		overflow-y: scroll;">
                                                <div class="form-group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">Assign to
                                                    </label>
                                                    <div class="col-lg-9 col-xl-9">
                                                        <select name="assign[0][assigned_to]" class="form-control">
                                                            @foreach($managers as $manager)
                                                            <option value="{{$manager->id}}">{{$manager->name}} -
                                                                {{$manager->role}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">Notes
                                                    </label>
                                                    <div class="col-lg-9 col-xl-9">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend"></div>
                                                            <textarea name="assign[0][notes]" class="form-control"
                                                                placeholder="Notes
																						" aria-describedby="basic-addon1"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="assign_section"></div>

                                                <div>
                                                    <p class="trans-underline trans-font-bold trans-cursor-pointer"
                                                        onclick="addMoreAssign()">Add More+</p>
                                                </div>
                                            </div>


                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Request total in USD
                                                </label>
                                                <div class="col-lg-9 col-xl-9">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend"></div>
                                                        <input type="number" name="request_total" class="form-control"
                                                            placeholder="Request total in USD
                                                                                    " aria-describedby="basic-addon1"
                                                            value="12334" disabled>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <!--end: Form Wizard Step 4-->

                        <!--begin: Form Actions -->
                        <div class="kt-form__actions">
                            <div class="btn btn-secondary btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u"
                                data-ktwizard-type="action-prev">
                                Previous
                            </div>
                            <div class="btn btn-success btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u"
                                id="kt_sweetalert_trans" data-ktwizard-type="action-submit">
                                Submit
                            </div>
                            <div class="btn btn-brand btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u"
                                data-ktwizard-type="action-next">
                                Next Step
                            </div>
                        </div>

                        <!--end: Form Actions -->
                    </form>

                    <!--end: Form Wizard Form-->
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@push('scripts')

<!--end::Global Theme Bundle -->


<!--end::Page Vendors -->

<!--begin::Page Scripts(used by this page) -->
<script src="{{asset('assets/js/pages/crud/forms/widgets/bootstrap-datepicker.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/js/pages/custom/projects/add-project.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/js/pages/crud/file-upload/dropzonejs.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/js/pages/crud/forms/widgets/bootstrap-datetimepicker.js')}}" type="text/javascript">
</script>
<script src="{{asset('assets/js/pages/components/extended/sweetalert2.js')}}" type="text/javascript"></script>
<script>
$(function() {
    $('#kt_sweetalert_trans').click(function(e) {
        swal.fire("Thank you!", "The request has been successfully submitted!", "success")
            .then(result => {
                if (result.value) {
                    window.location.replace('{{route('projects.index')}}')
                }
            })
    });

})
</script>>

<script>
function addMoreAssign() {
    var i = 1;
    $("#assign_section").append(`
				<hr>
				<div class="form-group row">
					<label class="col-xl-3 col-lg-3 col-form-label">Assign to
					</label>
					<div class="col-lg-9 col-xl-9">
						<select class="form-control" name=assign[${i}][assigned_to]>
                        @foreach($managers as $manager)
                             <option value="{{$manager->id}}">{{$manager->name}} - {{$manager->role}}</option>
                          @endforeach
						</select>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-xl-3 col-lg-3 col-form-label">Notes
					</label>
					<div class="col-lg-9 col-xl-9">
						<div class="input-group">
							<div class="input-group-prepend"></div>
							<textarea name="assign[${i}][notes]" class="form-control" placeholder="Notes
							" aria-describedby="basic-addon1"></textarea>
						</div>
					</div>
				</div>
                `)
    i++;
}
</script>

<script>

// $.ajaxSetup({
//     headers: {
//         'X-CSRF-TOKEN': '{{csrf_token()}}'
//     }
// });
$('#file_upload').dropzone({
    url: "{{route('operations.upload_files')}}", // Set the url for your upload script location
    paramName: "file", // The name that will be used to transfer the file
    maxFiles: 10,
    maxFilesize: 10, // MB
    addRemoveLinks: true,
    headers: {
    'X-CSRF-Token': '{{csrf_token()}}'
  },
  init:function () {
        this.on("success", function (file, responseText) {
            $("#file_pathes").append(`
                <input type="hidden" value="`+responseText.path+`" name="paths[]">
            `);
            console.log(responseText);
        });
    }


});
</script>
@endpush
