@extends('operations.layouts.master')
@section('title','Admin Area')
@push('styles')

@endpush

@section('content')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12 text-right" style="margin-bottom:12px">
        <a type="button" class="btn btn-success btn-sm" style="color: white;" href="{{route('projects.edit',$project->id)}}">Update
            Project</a>
    </div>
</div>

<!--Begin::Section-->
<div class="row">
    <div class="col-xl-12">
        <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
            <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                <div class="kt-portlet__head-label" style="width: 100%!important;">
                    <h3 class="kt-portlet__head-title" style="width: 100%!important;">

                        <span>ProjectID: {{$project->projectID}}</span>
                        <span class="trans-status trans-purple-bg">Assigned project</span>
                        <!-- <span class="trans-status trans-orange-bg">Pending Project</span>
										  <span class="trans-status trans-green-bg">Delivered project</span>
										  <span class=" trans-status trans-cursor-pointer trans-lightgreen-bg pull-right" style="margin-left: 3px;">Accept</span>
										  <span class=" trans-status trans-cursor-pointer trans-red-bg pull-right">Decline</span> -->
                    </h3>
                </div>

            </div>
            <div class="kt-portlet__body kt-portlet__body--fit">

                <!--begin: body -->
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <table class="table table-striped">
                            <tbody>
                                <tr>
                                    <td class="font--bold">Service Type</td>
                                    <td>{{@$project->service->name}}</td>
                                </tr>

                                <tr>
                                    <td class="font--bold">Source Language</td>
                                    <td>{{@$project->Fromlanguage->name}}</td>
                                </tr>

                                <tr>
                                    <td class="font--bold">Target Language</td>
                                    <td>{{@$project->Tolanguage->name}}</td>
                                </tr>
                                <tr>
                                    <td class="font--bold">Certification</td>
                                    <td>{{$project->is_certification_required ? 'Yes' : 'No'}}</td>
                                </tr>
                                <tr>
                                    <td class="font--bold">CAT tool</td>
                                    <td>{{$project->is_cat_tool_required ? 'Yes' : 'No'}}</td>
                                </tr>
                                <tr>
                                    <td class="font--bold">CAT tool Software</td>
                                    <td>{{$project->software_name}}</td>
                                </tr>
                                <tr>
                                    <td class="font--bold">DTP</td>
                                    <td>{{$project->is_dtp_required ? 'Yes' : 'No'}}</td>
                                </tr>
                                <tr>
                                    <td class="font--bold">Formatting Required?</td>
                                    <td>{{$project->is_formating_required ? 'Yes' : 'No'}}</td>
                                </tr>
                                <tr>
                                    <td class="font--bold">Total Word Count</td>
                                    <td>10</td>
                                </tr>
                                <tr>
                                    <td class="font--bold">Total DTP pages</td>
                                    <td>{{$project->number_of_pages}}</td>
                                </tr>
                                <tr>
                                    <td class="font--bold">Service Rate</td>
                                    <td>$909</td>
                                </tr>
                                <tr>
                                    <td class="font--bold">DTP Rate</td>
                                    <td>@8989</td>
                                </tr>
                                <tr>
                                    <td class="font--bold">Total Request Rate</td>
                                    <td>{{$project->request_total}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <table class="table table-striped">
                            <tbody>
                                <tr>
                                    <td class="font--bold">Client Name</td>
                                    <td>{{@$project->client->name}}</td>
                                </tr>

                                <tr>
                                    <td class="font--bold">Agent Name</td>
                                    <td>{{@$project->agent->name}}</td>
                                </tr>

                                <tr>
                                    <td class="font--bold">Project Number of Files</td>
                                    <td>12</td>
                                </tr>
                                <tr>
                                    <td class="font--bold">Deadline</td>
                                    <td>{{$project->client_deadline}}</td>
                                </tr>
                                @if($project->new_deadline)
                                <tr class="trans-lightgreen-bg">
                                    <td class="font--bold ">New Request Deadline</td>
                                    <td>{{$project->new_deadline}}</td>
                                </tr>
                                @endif
                                <tr class="trans-red-bg">
                                    <td class="font--bold">Deadline</td>
                                    <td>{{$project->client_deadline}}</td>
                                </tr>
                                <tr>
                                    <td class="font--bold">Time Zone</td>
                                    <td>{{$project->timezone}}</td>
                                </tr>
                                <tr>
                                    <td class="font--bold">Client Guidelines</td>
                                    <td>{{$project->client_guidelines}}</td>
                                </tr>
                                <tr>
                                    <td class="font--bold">Assigned To</td>
                                    <td>
                                        @foreach($project->notes as $note)
                                            <span class="trans-border-view"> {{@$note->AssginedTo->name}}</span>
                                        @endforeach
                                </tr>
                                <tr>
                                    <td class="font--bold">Project Status</td>
                                    <td>{{$project->status}}</td>
                                </tr>
                                <tr>
                                    <td class="font--bold">Assigned By</td>
                                    <td>Admin</td>
                                </tr>

                            </tbody>

                        </table>

                    </div>
                </div>

                <!--end: body -->
            </div>
        </div>
    </div>

</div>
<div class="row">
    <div class="col">
        <div class="alert alert-light alert-elevate fade show" role="alert">
            <div class="alert-text">
                <div class="row">
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="trans-project-service-type-container trans-cursor-pointer">
                            <h3 style="font-size:16px">Download Source Files</h3>
                        </div>
                    </div>


                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <a href="./users.html">
                            <div class="trans-project-service-type-container trans-cursor-pointer">
                                <h3 style="font-size:16px; color:black">Add / view comments for LC/ PM
                                </h3>
                            </div>
                        </a>

                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                    <a href="{{route('operations.project_activity',$project->id)}}">
                            <div class="trans-project-service-type-container trans-cursor-pointer">
                                <h3 style="font-size:16px; color:black">Project activity
                                </h3>
                            </div>
                        </a>

                    </div>

                </div>

            </div>
        </div>
    </div>

</div>

@endsection

@push('scripts')



@endpush
