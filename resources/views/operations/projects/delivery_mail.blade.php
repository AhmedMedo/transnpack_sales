@extends('operations.layouts.master')
@section('title','Admin Area')
@push('styles')
<link href="{{asset('assets/css/pages/inbox/inbox.css')}}" rel="stylesheet" type="text/css" />

@endpush

@section('content')
<div class="row">
    <div class="col-md-12">

        <!--begin::Portlet-->
        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Project Deilevery Mail
                    </h3>
                </div>
            </div>

            <!--begin::Form-->
            <form class="kt-form" method="POST" action="{{route('operations.send_mail',$id)}}">
                @csrf
                <div class="kt-portlet__body">
                    <div class="form-group">
                        <label>To</label>
                        <input type="email" name="to_email" class="form-control" aria-describedby="emailHelp" placeholder="Enter email">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Subject</label>
                        <input type="text" name="subject" class="form-control" id="exampleInputPassword1" placeholder="Subject">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">CC</label>
                        <input type="text" name="cc_email" class="form-control" id="exampleInputPassword1" placeholder="CC">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Body</label>
                        <textarea id="kt-tinymce-2" name="body" class="tox-target"></textarea>
                    </div>
                </div>
                <div class="kt-portlet__foot">
                    <div class="kt-form__actions">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <button type="reset" class="btn btn-secondary" onclick="location.href='{{route('projects.index')}}'">Cancel</button>
                    </div>
                </div>
            </form>

            <!--end::Form-->
        </div>

        <!--end::Portlet-->
    </div>
</div>
@endsection

@push('scripts')
<script src="{{asset('assets/plugins/custom/tinymce/tinymce.bundle.js')}}" type="text/javascript"></script>

<!--end::Page Vendors -->

<!--begin::Page Scripts(used by this page) -->
<script src="{{asset('assets/js/pages/crud/forms/editors/tinymce.js')}}" type="text/javascript"></script>

<script>

</script>


@endpush
