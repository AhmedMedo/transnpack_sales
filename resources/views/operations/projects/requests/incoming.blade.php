@extends('operations.layouts.master')
@section('title','Admin Area')
@push('styles')

@endpush

@section('content')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12 text-right" style="margin-bottom:12px">
        <a type="button" class="btn btn-success btn-sm" style="color: white;" href="./client-add.html">Add New
            Client</a>

        <a type="button" class="btn btn-success btn-sm" style="color: white;" href="./project-add.html">Add New
            Project</a>
    </div>
</div>

<!--Begin::Section-->
<div class="row">
    <div class="col-xl-12">
        <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
            <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Incoming Requests
                    </h3>
                </div>

            </div>
            <div class="kt-portlet__body">

                <!--begin: Datatable -->
                <div class="kt-datatable" id="incoming_requests"></div>
                <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
                    <thead>
                        <tr>
                            <th>Request Date</th>
                            <th>Request Type</th>
                            <th>Request From</th>
                            <th>Project ID</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($projects as $project)
                        <tr>
                            <td>{{$project->created_at}}</td>
                            <td>{{$project->service->name}}</td>
                            <td>{{$project->AssginedFrom->name}}</td>
                            <td>{{$project->projectID}}</td>
                            <td>
                                <span style="overflow: visible; position: relative; width: 80px;">
                                    <div class="dropdown">
                                        <a data-toggle="dropdown" class="btn btn-sm btn-clean btn-icon btn-icon-md">                                <i class="flaticon-more-1"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <ul class="kt-nav">
                                                <li class="kt-nav__item">
                                                    <a class="kt-nav__link" href="{{route('operations.show_incoming_requests',$project->id)}}">
                                                        <i class="kt-nav__link-icon flaticon2-expand"></i>
                                                        <span class="kt-nav__link-text">View Request</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </span>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <!--end: Datatable -->
            </div>
        </div>
    </div>

</div>

@endsection

@push('scripts')
<script>
var KTDatatablesDataSourceHtml = function() {

    var initTable1 = function() {
        var table = $('#kt_table_1');

        // begin first table
        table.DataTable({
            responsive: true,

        });

    };

    return {

        //main function to initiate the module
        init: function() {
            initTable1();
        },

    };

}();

jQuery(document).ready(function() {
    KTDatatablesDataSourceHtml.init();


});
</script>


@endpush
