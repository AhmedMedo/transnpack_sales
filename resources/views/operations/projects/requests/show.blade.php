@extends('operations.layouts.master')
@section('title','Admin Area')
@push('styles')

@endpush

@section('content')
<div class="row">

@if($project->assigned_by == Auth::guard('manager')->user()->id && $project->status =='completed')
<div class="col-md-12 col-sm-12 col-xs-12 text-right" style="margin-bottom:12px">
        <a type="button" class="btn btn-success btn-sm" style="color: white;"
            href="{{route('projects.reopen_project',$project->id)}}">Re-open Project</a>
    </div>
@else
    <div class="col-md-12 col-sm-12 col-xs-12 text-right" style="margin-bottom:12px">
        <a type="button" class="btn btn-success btn-sm" style="color: white;"
            href="{{route('projects.edit',$project->id)}}">Update
            Project</a>
    </div>
@endif
</div>

<!--Begin::Section-->
<div class="row">
    <div class="col-xl-12">
        <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
            <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                <div class="kt-portlet__head-label" style="width: 100%!important;">
                    <h3 class="kt-portlet__head-title" style="width: 80%!important;">
                        <span>ProjectID: {{$project->projectID}}</span>
                        <span class="trans-status trans-purple-bg">{{$project->status}} project</span>
                        <!-- <span class="trans-status trans-orange-bg">Pending Project</span>
                        <span class="trans-status trans-green-bg">Delivered project</span>
                        <span class=" trans-status trans-cursor-pointer trans-lightgreen-bg pull-right" style="margin-left: 3px;">Accept</span>
                        <span class=" trans-status trans-cursor-pointer trans-red-bg pull-right">Decline</span> -->
                    </h3>
                    @if($project->status =='assigned' && $project->assigned_by != Auth::guard('manager')->user()->id )
                    <form method="POST" id="formStatus" action="{{route('operations.project_status',$project->id)}}" class="form-inline">
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <button class="btn btn-info btn-sm" type="submit" name="accept" id="accept" value="accept">Accept</button>

                            </div>
                                <div class="col-md-6">
                                    <button class="btn btn-danger btn-sm" type="button" data-toggle="modal" data-target="#kt_modal_5" value="decline">Reject</button>

                            </div>
                        </div>
                    </form>
                        <div class="modal fade" id="kt_modal_5" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog modal-sm" role="document">
                                <div class="modal-content">
                                <form method="POST" id="formStatus" action="{{route('operations.project_status',$project->id)}}" class="form-inline">
                                    @csrf
                                        <input type="hidden" name="reject" value="reject">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Rejection Reason</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                                <div class="form-group">
                                                    <textarea name="rejection_reason" class="form-control"style="width:100%" id="message-text"></textarea>
                                                </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Send</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    @endif
                    @if($project->status =='rejected')
                    <div class="row">
                        <strong>Rejection Reason :</strong> <span class="danger">{{$project->rejection_reason}}<span>

                    </div>
                    @endif
                </div>
            </div>
            <div class="kt-portlet__body kt-portlet__body--fit">

                <!--begin: body -->
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <table class="table table-striped">
                            <tbody>
                                <tr>
                                    <td class="font--bold">Service Type</td>
                                    <td>{{@$project->service->name}}</td>
                                </tr>

                                <tr>
                                    <td class="font--bold">Source Language</td>
                                    <td>{{@$project->Fromlanguage->name}}</td>
                                </tr>

                                <tr>
                                    <td class="font--bold">Target Language</td>
                                    <td>{{@$project->Tolanguage->name}}</td>
                                </tr>
                                <tr>
                                    <td class="font--bold">Certification</td>
                                    <td>{{$project->is_certification_required ? 'Yes' : 'No'}}</td>
                                </tr>
                                <tr>
                                    <td class="font--bold">CAT tool</td>
                                    <td>{{$project->is_cat_tool_required ? 'Yes' : 'No'}}</td>
                                </tr>
                                <tr>
                                    <td class="font--bold">CAT tool Software</td>
                                    <td>{{$project->software_name}}</td>
                                </tr>
                                <tr>
                                    <td class="font--bold">DTP</td>
                                    <td>{{$project->is_dtp_required ? 'Yes' : 'No'}}</td>
                                </tr>
                                <tr>
                                    <td class="font--bold">Formatting Required?</td>
                                    <td>{{$project->is_formating_required ? 'Yes' : 'No'}}</td>
                                </tr>
                                <tr>
                                    <td class="font--bold">Total Word Count</td>
                                    <td>10</td>
                                </tr>
                                <tr>
                                    <td class="font--bold">Total DTP pages</td>
                                    <td>{{$project->number_of_pages}}</td>
                                </tr>
                                <tr>
                                    <td class="font--bold">Service Rate</td>
                                    <td>$909</td>
                                </tr>
                                <tr>
                                    <td class="font--bold">DTP Rate</td>
                                    <td>@8989</td>
                                </tr>
                                <tr>
                                    <td class="font--bold">Total Request Rate</td>
                                    <td>{{$project->request_total}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <table class="table table-striped">
                            <tbody>
                                <tr>
                                    <td class="font--bold">Client Name</td>
                                    <td>{{@$project->client->name}}</td>
                                </tr>

                                <tr>
                                    <td class="font--bold">Agent Name</td>
                                    <td>{{@$project->agent->name}}</td>
                                </tr>

                                <tr>
                                    <td class="font--bold">Project Number of Files</td>
                                    <td>12</td>
                                </tr>
                                @if($project->assigned_by == Auth::guard('manager')->user()->id && $project->new_deadline)
                                <tr class="trans-lightgreen-bg">
                                    <td class="font--bold ">new deadline requested by Language Coordinator</td>
                                    <td>{{$project->new_deadline}}</td>
                                    <td>
                                        <span class="trans-status btn-success trans-cursor-pointer">Approve</span>
                                        <span class="trans-status trans-red-bg trans-cursor-pointer">Cancel</span>
                                    </td>

                                </tr>
                                @endif
                                <tr class="trans-red-bg">
                                    <td class="font--bold">Deadline</td>
                                    <td>{{$project->client_deadline}}</td>
                                </tr>
                                <tr>
                                    <td class="font--bold">Time Zone</td>
                                    <td>{{$project->timezone}}</td>
                                </tr>
                                <tr>
                                    <td class="font--bold">Client Guidelines</td>
                                    <td>{{$project->client_guidelines}}</td>
                                </tr>
                                <tr>
                                    <td class="font--bold">Assigned To</td>
                                    <td>
                                        @foreach($project->notes as $note)
                                        <span class="trans-border-view"> {{$note->AssginedTo->name}}</span>
                                        @endforeach
                                    </td>
                                </tr>
                                <tr>
                                    <td class="font--bold">Project Status</td>
                                    <td>{{$project->status}}</td>
                                </tr>
                                <tr>
                                    <td class="font--bold">Assigned By</td>
                                    <td>Admin</td>
                                </tr>

                            </tbody>
                            @if($project->status =='assigned' && $project->assigned_by != Auth::guard('manager')->user()->id)
                            <tfoot>
                                <tr>
                                    <td>
                                        <form method="POST" action="{{route('operations.project_new_deadline',$project->id)}}">
                                            @csrf
                                            <label>New Deadline</label>
                                            <input type="text" name="new_deadline" class="form-control" id="kt_datepicker_1" readonly=""
                                                placeholder="Select date">
                                                <br>
                                                <div class="input-group" style="margin-bottom: 12px;">
                                                    <div class="input-group-prepend"></div>
                                                    <textarea name="new_deadline_notes" class="form-control" placeholder="Reason" aria-describedby="basic-addon1"></textarea>
                                                </div>
                                                <button class="btn btn-info btn-sm" type="submit" name="accept" id="accept" value="accept">Send</button>
                                        </form>

                                    </td>
                                    <!-- <td style="padding-top: 41px;">
                                        <span class="trans-status btn-success trans-cursor-pointer">Approve</span>
                                        <span class="trans-status trans-red-bg trans-cursor-pointer">Cancel</span>
                                    </td> -->
                                </tr>
                            </tfoot>
                            @endif
                        </table>


                    </div>
                </div>

                <!--end: body -->
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col">
        <div class="alert alert-light alert-elevate fade show" role="alert">
            <div class="alert-text">
                <div class="row">
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="trans-project-service-type-container trans-cursor-pointer">
                            <h3 style="font-size:16px">Download Source Files</h3>
                        </div>
                    </div>


                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <a href="./users.html">
                            <div class="trans-project-service-type-container trans-cursor-pointer">
                                <h3 style="font-size:16px; color:black">Add / view comments for LC/ PM
                                </h3>
                            </div>
                        </a>

                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <a href="./project-activity.html">
                            <div class="trans-project-service-type-container trans-cursor-pointer">
                                <h3 style="font-size:16px; color:black">Project activity
                                </h3>
                            </div>
                        </a>

                    </div>

                </div>

            </div>
        </div>
    </div>

</div>

@endsection

@push('scripts')
<script src="{{asset('assets/js/pages/crud/forms/widgets/bootstrap-datepicker.js')}}" type="text/javascript"></script>



@endpush
