<!DOCTYPE html>
<html>
<head>
<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>
</head>
<body>
	<p>
A complementary package has been assigned free of charge..<br>
<br>
 <br>

Order info:
<br>
<br>
<br>
Package ID: {{$package->package_ID}}<br>
<br>
<br>
Client name: {{@$package->client->fullname}}
<br>
<br>
<!-- Source Language: [Language name / Package name]
<br>
<br>
Target Language(s): [Language name / Package name]
<br>
<br> -->
@foreach($package->Fromlanguages as $lang)
		<table>
		  <tr>
		    <th>Source language</th>
		    <th>Target Language</th>
		  </tr>
		  	@foreach($lang->ToLanguages()->wherePivot('package_id',$package->id)->get() as $toLang)
		  	  <tr>
		  		<td>
		  			{{$lang->name}}
		  		</td>
		  		<td>
		  			{{$toLang->name}}
		  		</td>
		  	 </tr>
		  	@endforeach
		</table>
		<br>
@endforeach
<br>

Number of words: {{$package->total_num_of_words}}
<br>
<br>
Package total amount: 0 USD
<br>
<br>
To activate your complementary package kindly follow this  <a href="{{$package->package_payment_link}}">link</a>.
<br>
<br>
You can refer back to sales@transnpack.com for any further inquiries.
<br>
<br>
TransnPack team

</p>


</body>
</html>
