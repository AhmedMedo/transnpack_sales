  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- Sidebar user panel (optional) -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{ asset('img/logo.png') }}"" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{Auth::guard('sales')->user()->name}}</p>
          <!-- Status -->
          <a href="#"><i class="fa  fa-plus-square-o text-success"></i>  Sales</a>
        </div>
      </div>
      <!-- Sidebar Menu -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <!-- Optionally, you can add icons to the links -->
        <li class="active"><a href="#"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a> </li>
          <li class="treeview">
              <a href="#">
                  <i class="fa fa-laptop"></i>
                  <span>Packages</span>
                  <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
              </a>
              <ul class="treeview-menu">
                  <li><a href="{{route('packages.index')}}"><i class="fa fa-circle-o"></i> List Packages</a></li>
                  <li><a href="{{route('packages.create')}}"><i class="fa fa-circle-o"></i> Add Package</a></li>
              </ul>
          </li>
          @if(Auth::guard('sales')->user()->can_view_reports)
          <li ><a href="{{route('reports')}}"><i class="fa fa-file"></i> <span>Reports</span></a> </li>
          @endif

      </ul>

      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>
