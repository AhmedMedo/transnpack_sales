<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang="en">
<head>
<title>Sales Forget Password</title>
  <link rel="stylesheet" href="{{ asset('bower_components/font-awesome/css/font-awesome.min.css')}}">
  <link rel="stylesheet" href="{{ asset('css/style.css')}}">

<link href="//fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Basic Login Form Responsive Widget,Login form widgets, Sign up Web forms , Login signup Responsive web form,Flat Pricing table,Flat Drop downs,Registration Forms,News letter Forms,Elements" />
  <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <script src="{{ asset('bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script>$(document).ready(function(c) {
    $('.alert-close').on('click', function(c){
      $('.main-agile').fadeOut('slow', function(c){
        $('.main-agile').remove();
      });
    });
  });
  </script>
</head>
<body>
  <h1>Transnpack Sales Area</h1>
  <div class="main-agile">
    <div class="content-wthree">
    <div class="circle-w3layouts"></div>
      <h2>Sales Forget Password</h2>
   <form method="POST" action="{{ route('forget_password.submit') }}" aria-label="{{ __('Login') }}">
         @csrf
                       @if (Session::has('errors'))

                           <span class="help-block">
                               <strong>{{ session('errors')->first('email') }}</strong>
                           </span>
                       @endif
                <div class="inputs-w3ls">
                  <i class="fa fa-user" aria-hidden="true"></i>
                  <input type="email" name="email" placeholder="Email" required=""/>
                </div>

                  <input type="submit" value="Forget password">

      </form>
    </div>
  </div>
  <div class="footer-w3l">
    <p class="agileinfo"> &copy; 2019 Transnpack. All Rights Reserved | Design by <a href="https://www.linkedin.com/in/ahmedalaa100/">Medo</a></p>
  </div>
</body>
</html>
