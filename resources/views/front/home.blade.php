@extends('front.layouts.master')
@section('title','Home Page')

@section('content')
    <section id="slider" class="mt-4">
        <div class='left-slider'></div>
        <div class="slider">
            <div></div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col font-weight-bold">
                    <h1><span>Professionally</span> Translate Your<br>Documents With Certified<br>Human Translators.
                    </h1>
                    <p class="mt-3">Save more with our translation packages 60+ languages, 24/7 support,<br>Ready to
                        start translating? Create your order in seconds.</p>
                    <div class="col-12 p-0">
                        <a href="./packages.html" class="btn btn-primary slider-btns packagepop-btn ml-0">Order Now</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="how-it-works" class="font-weight-bold pb-5">
        <h2>How it works</h2>
        <div class="container">
            <div class="row justify-content-between">
                <p class="col-12 col-lg-8 mb-2 mx-auto">
                    Three simple steps to start your translation experience with TransnPack
                    Select your language pair, select expected volume you would like to use, and
                    content type and you can start your translation right away from your account
                    dashboard
                </p>
            </div>

            <div class="row pt-5 pl-3 text-left how-contents text-center">

                <div class="col-12 col-md-4">
                    <div class="circle">
                        <p>1</p>
                    </div>
                    <h6 class="text-uppercase font-weight-bold pt-2">Register</h6>
                    <p class="pt-2">Sign up choosing a Pack with the language pairs you need and the word count you may
                        consume.</p>
                </div>
                <div class="col-12 col-md-4">
                    <div class="circle">
                        <p>2</p>
                    </div>
                    <h6 class="text-uppercase font-weight-bold pt-2">Upload your files</h6>
                    <p class="pt-2">Get your papers in whatever extension and submit them on your dashboard.</p>
                </div>
                <div class="col-12 col-md-4">
                    <div class="circle">
                        <p>3</p>
                    </div>
                    <h6 class="text-uppercase font-weight-bold pt-2">Download translations</h6>
                    <p class="pt-2">Wait till your papers are translated and it will be in your *Ready Files for
                        Download* section.</p>
                </div>
                <div class="col-8 offset-2 mt-5">
                    <img src="{{asset('front/img/how-it-works.png')}}">
                </div>
            </div>
        </div>
    </section>


    <img class="lang-bg" src="{{asset('front/img/languages-bg.png')}}">
    <section id="pricing">
        <div class="col-12 col-md-6 tool-img">
            <img class="packages-img" src="{{asset('front/img/tool.png')}}">
        </div>
        <div class="container text-left">
            <h2 class="text-left">TAILORED PACKAGES</h2>
            <div class="row">
                <div class="col-12 col-md-7 col-lg-6">
                    <p>Our pack is a combination of your needed language pairs and the word count you may consume, we
                        have created for you some packages with the most popular language pairs and a reasonable word
                        count.

                        <a class="btn btn-primary mt-5 buy-btn py-2" href="../?p=pack">ORDER NOW</a>
                </div>
            </div>
        </div>
    </section>
    <section id="platform">
        <div class="container">
            <h2 class=>Your Personal Localization Platform</h2>
            <div class="row justify-content-between">
                <p class="col-12 col-lg-8 mb-2 mx-auto text-center">
                    Through our platform you can start using your translation or proofreading package to
                    have your linguistic needs met according to the set budget you are purchasing.
                    our platform is easy to use and you can add as much packages as you want in various
                    languages according to your needs
                </p>
            </div>


            <div class="row">
                <div class="col-12 col-md-10 offset-md-1 mt-5">
                    <img src="{{asset('front/img/chart.png')}}">
                </div>
            </div>

        </div>
    </section>
    <section id="languages">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12 col-lg-7 text-left order-2 order-lg-1 mt-5">
                    <h2 class="text-left">TRANSLATE INTO 65+ LANGUAGES</h2>
                    <p class="mt-4">
                        We provide more than language services: Analyse & plan your linguistic needs, easily control,
                        manage and optimize your localization workflows on your account - all covered seamlessly thanks
                        to our user-friendly dashboard.
                    </p>

                </div>

                <div class="col-12 col-md-12 col-lg-5 order-1 order-lg-2 mb-5">
                    <table class="w-100">
                        <tr>
                            <td><img src="{{asset('front/img/flags/croatian.png')}}"> Croatian</td>
                            <td><img src="{{asset('front/img/flags/lithuanian.png')}}"> Lithuanian</td>
                            <td><img src="{{asset('front/img/flags/italian.png')}}"> Italian</td>
                        </tr>
                        <tr>
                            <td><img src="{{asset('front/img/flags/czech.png')}}"> Czech</td>
                            <td><img src="{{asset('front/img/flags/polish.png')}}"> Polish</td>
                            <td><img src="{{asset('front/img/flags/french.png')}}"> French</td>
                        </tr>
                        <tr>
                            <td><img src="{{asset('front/img/flags/dutch.png')}}"> Dutch</td>
                            <td><img src="{{asset('front/img/flags/romanian.png')}}"> Romanian</td>
                            <td><img src="{{asset('front/img/flags/portuguese.png')}}"> Portuguese</td>
                        </tr>
                        <tr>
                            <td><img src="{{asset('front/img/flags/danish.png')}}"> Danish</td>
                            <td><img src="{{asset('front/img/flags/polish.png')}}"> Polish</td>
                            <td><img src="{{asset('front/img/flags/spanish.png')}}"> Spanish</td>
                        </tr>
                        <tr>
                            <td><img src="{{asset('front/img/flags/greek.png')}}"> Greek</td>
                            <td><img src="{{asset('front/img/flags/russian.png')}}"> Russian</td>
                            <td><img src="{{asset('front/img/flags/albanian.png')}}"> Albanian</td>
                        </tr>
                        <tr>
                            <td><img src="{{asset('front/img/flags/icelandic.png')}}"> Icelandic</td>
                            <td><img src="{{asset('front/img/flags/ukrainian.png')}}"> Ukrainian</td>
                            <td><img src="{{asset('front/img/flags/bosnian.png')}}"> Bosnian</td>
                        </tr>
                        <tr>
                            <td><img src="{{asset('front/img/flags/estonian.png')}}"> Estonian</td>
                            <td><img src="{{asset('front/img/flags/slovene.png')}}"> Slovene</td>
                            <td><img src="{{asset('front/img/flags/bulgarian.png')}}"> Bulgarian</td>
                        </tr>
                        <tr>
                            <td><img src="{{asset('front/img/flags/latvian.png')}}"> Latvian</td>
                            <td><img src="{{asset('front/img/flags/german.png')}}"> German</td>
                            <td><img src="{{asset('front/img/flags/catalan.png')}}"> Catalan</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <section id="map">
        <div class="container">
            <h2>Powered by 5000+ Expert Linguists</h2>
            <div class="row justify-content-between">
                <p class="col-12 col-lg-8 mb-2 mx-auto text-center">
                    We work with a global and growing network of translators, editors, and language
                    team leaders in over 60 countries translating only into their native languages.
                </p>
            </div>
            <div class="row mt-5">
                <div class="col-12">
                    <img src="{{asset('front/img/map.png')}}">
                </div>
            </div>
        </div>
    </section>
    <section id="contact" class="pt-5">
        <div class="container pt-5">
            <h2>Get in touch</h2>
            <div class="row justify-content-between">
                <p class="col-12 col-lg-8 mb-2 mx-auto text-center">Let us answer your questions</p>
            </div>
            <div class="row mt-5">
                <div class="col-12 col-md-6 mx-auto">
                    <form action="../#contact" method="POST" id="contactus-form">
                        <div class="form-group">
                            <input type="text" class="form-control" name="contact-name" placeholder="Full Name*"
                                   value=''>
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control" name="contact-email" placeholder="E-mail*"
                                   value=''>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="contact-company" placeholder="Subject*"
                                   value=''>
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" name="contact-desc" placeholder="Description*"
                                      rows="3"></textarea>
                        </div>

                        <div class="g-recaptcha" data-sitekey="6LdJjJEUAAAAAPbeDbnFffK7KLYP56Dk2j97tMcj"></div>

                        <button type="submit" class="btn btn-primary w-100 mt-3 py-2" name="contact-us">Send</button>
                    </form>
                </div>
            </div>
        </div>
    </section>


    <div class="packages-pricing">
        <div class="container">
            <div class="packages-close">
                <a href="../">X</a>
            </div>

            <h4 class="text-center text-uppercase font-weight-bold">Available Packages</h4>

            <ul class="row text-uppercase">
                <li class="col-12 col-lg-4 packs pack-1 active-lang" id="ty1"><span>Translation Packages</span></li>
                <li class="col-12 col-lg-4 packs pack-2 " id="ty2"><span>Proofreading Packages</span></li>
                <li class="col-12 col-lg-4 packs pack-3 " id="ty3"><span>Translation and Proofreading Packages</span>
                </li>
            </ul>

            <div class='container'>
                <div class="row bg-white mt-3 mb-5 package-card s-pack" id="sp-1">
                    <div class="col-12 m-0 p-0 package-rounded">

                        <div class="select-package sp-1 d-block-pack">
                            <i class="fas fa-check-circle"></i>
                        </div>

                        <img class="package-bg" src="{{asset("front/img/package-map.png")}}">
                        <div class="row p-details">
                            <div class="col-12 col-md-10 package-details">
                                <img class="popular-bg" src="{{asset("front/img/popular.png")}}">
                                <div class="container first-package py-3">
                                    <h6 class="text-uppercase font-weight-bold pt-3">Group 1 Package From (English)</h6>
                                    <span class="text-muted">to the Included Languages:</span>

                                    <div class="mt-3 text-muted">
                                        <img src="{{asset("front/img/flags/german.png")}}" class='my-2'> German
                                        <img src="{{asset('front/img/flags/italian.png')}}" class='my-2'> Italian
                                        <img src="{{asset('front/img/flags/french.png')}}" class='my-2'> French
                                        <img src="{{asset('front/img/flags/spanish.png')}}" class='my-2'> Spanish
                                        <img src="{{asset('front/img/flags/portuguese.png')}}" class='my-2'> Portuguese
                                    </div>

                                    <hr>


                                    <ul class="text-muted text-left package-options">
                                        <li><i class="fas fa-check mr-1"></i>Native translation specialists</li>
                                        <li><i class="fas fa-check mr-1"></i>3-5 Steps of quality assurance</li>
                                        <li><i class="fas fa-check mr-1"></i>24/7 available customer service</li>
                                    </ul>


                                </div>


                            </div>

                            <div class="col-12 col-md-2 package-card-price">
                                <div class="row align-content-center ty ty1 ">
                                    <div class="col-6 col-md-12 pb-2">
                                        <div>You get</div>
                                        <span>1000</span>word
                                    </div>
                                    <div class="col-6 col-md-12">
                                        <div>For only</div>
                                        <span>$150</span>
                                    </div>
                                </div>

                                <div class="row align-content-center text-left ty ty2 ">
                                    <div class="col-6 col-md-12 pb-2">
                                        <div>You get</div>
                                        <span>1000</span>word
                                    </div>
                                    <div class="col-6 col-md-12">
                                        <div>For only</div>
                                        <span>$75</span>
                                    </div>
                                </div>

                                <div class="row align-content-center ty ty3 ">
                                    <div class="col-6 col-md-12 pb-2">
                                        <div>You get</div>
                                        <span>1000</span>word
                                    </div>
                                    <div class="col-6 col-md-12">
                                        <div>For only</div>
                                        <span>$225</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row bg-white my-5 package-card " id="sp-2">
                    <div class="col-12 m-0 p-0 package-rounded">

                        <div class="select-package sp-2 d-none-pack">
                            <i class="fas fa-check-circle"></i>
                        </div>

                        <img class="package-bg" src="{{asset("front/img/package-map.png")}}">
                        <div class="row p-details">
                            <div class="col-12 col-md-10 package-details">
                                <div class="container py-3">
                                    <h6 class="text-uppercase font-weight-bold pt-3">Group 2 Package From (English)</h6>
                                    <span class="text-muted">to the Included Languages:</span>

                                    <div class="mt-3 text-muted">
                                        <img src="{{asset("front/img/flags/croatian.png")}}" class='my-2'> Croatian
                                        <img src="{{asset("front/img/flags/czech.png")}}" class='my-2'> Czech
                                        <img src="{{asset("front/img/flags/dutch.png")}}" class='my-2'> Dutch
                                        <img src="{{asset("front/img/flags/danish.png")}}" class='my-2'> Danish
                                        <img src="{{asset("front/img/flags/greek.png")}}" class='my-2'> Greek
                                        <img src="{{asset("front/img/flags/hungarian.png")}}" class='my-2'> Hungarian<br>
                                        <img src="{{asset("front/img/flags/norwegian.png")}}" class='my-2'> Norwegian
                                        <img src="{{asset("front/img/flags/romanian.png")}}" class='my-2'> Romanian
                                        <img src="{{asset("front/img/flags/polish.png")}}" class='my-2'> Polish
                                        <img src="{{asset("front/img/flags/russian.png")}}" class='my-2'> Russian
                                        <img src="{{asset("front/img/flags/ukrainian.png")}}" class='my-2'> Ukrainian
                                        <img src="{{asset("front/img/flags/slovene.png")}}" class='my-2'> Slovene<br>
                                        <img src="{{asset("front/img/flags/finnish.png")}}" class='my-2'> Finnish
                                        <img src="{{asset("front/img/flags/swedish.png")}}" class='my-2'> Swedish
                                        <img src="{{asset("front/img/flags/estonian.png")}}" class='my-2'> Estonian
                                        <img src="{{asset("front/img/flags/latvian.png")}}" class='my-2'> Latvian
                                        <img src="{{asset("front/img/flags/lithuanian.png")}}" class='my-2'> Lithuanian
                                        <img src="{{asset("front/img/flags/polish.png")}}" class='my-2'> Polish<br>
                                        <img src="{{asset("front/img/flags/slovak.png")}}" class='my-2'> Slovak
                                        <img src="{{asset("front/img/flags/albanian.png")}}" class='my-2'> Albanian
                                        <img src="{{asset("front/img/flags/bosnian.png")}}" class='my-2'> Bosnian
                                        <img src="{{asset("front/img/flags/bulgarian.png")}}" class='my-2'> Bulgarian
                                        <img src="{{asset("front/img/flags/catalan.png")}}" class='my-2'> Catalan
                                        <img src="{{asset("front/img/flags/serbian.png")}}" class='my-2'> Serbian
                                        <img src="{{asset("front/img/flags/armenian.png")}}" class='my-2'> Armenian
                                    </div>

                                    <hr>


                                    <ul class="text-muted text-left package-options">
                                        <li><i class="fas fa-check mr-1"></i>Native translation specialists</li>
                                        <li><i class="fas fa-check mr-1"></i>3-5 Steps of quality assurance</li>
                                        <li><i class="fas fa-check mr-1"></i>24/7 available customer service</li>
                                    </ul>


                                </div>


                            </div>

                            <div class="col-12 col-md-2 package-card-price">
                                <div class="row align-content-center ty ty1 ">
                                    <div class="col-6 col-md-12 pb-2">
                                        <div>You get</div>
                                        <span>1000</span>word
                                    </div>
                                    <div class="col-6 col-md-12">
                                        <div>For only</div>
                                        <span>$180</span>
                                    </div>
                                </div>

                                <div class="row align-content-center ty ty2 ">
                                    <div class="col-6 col-md-12 pb-2">
                                        <div>You get</div>
                                        <span>1000</span>word
                                    </div>
                                    <div class="col-6 col-md-12">
                                        <div>For only</div>
                                        <span>$90</span>
                                    </div>
                                </div>

                                <div class="row align-content-center ty ty3 ">
                                    <div class="col-6 col-md-12 pb-2">
                                        <div>You get</div>
                                        <span>1000</span>word
                                    </div>
                                    <div class="col-6 col-md-12">
                                        <div>For only</div>
                                        <span>$270</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row bg-white my-5 package-card " id="sp-3">
                    <div class="col-12 m-0 p-0 package-rounded">

                        <div class="select-package sp-3 d-none-pack">
                            <i class="fas fa-check-circle"></i>
                        </div>

                        <img class="package-bg" src="{{asset("front/img/package-map.png")}}">
                        <div class="row p-details">
                            <div class="col-12 col-md-10 package-details">
                                <div class="container py-3">
                                    <h6 class="text-uppercase font-weight-bold pt-3">Group 3 Package From (English)</h6>
                                    <span class="text-muted">to the Included Languages:</span>

                                    <div class="mt-3 text-muted">
                                        <img src="{{asset("front/img/flags/arabic.png")}}" class='my-2'> Arabic
                                        <img src="{{asset("front/img/flags/finnish.png")}}" class='my-2'> Farsi
                                        <img src="{{asset("front/img/flags/hebrew.png")}}" class='my-2'> Hebrew
                                        <img src="{{asset("front/img/flags/hindi.png")}}" class='my-2'> Hindi
                                        <img src="{{asset("front/img/flags/indonesian.png")}}" class='my-2'> Indonesian
                                        <img src="{{asset("front/img/flags/japanese.png")}}" class='my-2'> Japanese
                                        <img src="{{asset("front/img/flags/tagalog.png")}}" class='my-2'> Tagalog<br>
                                        <img src="{{asset("front/img/flags/korean.png")}}" class='my-2'> Korean
                                        <img src="{{asset("front/img/flags/simplified_chinese.png")}}" class='my-2'> Simplified Chinese
                                        <img src="{{asset("front/img/flags/traditional_chinese.png")}}" class='my-2'> Traditional
                                        Chinese
                                        <img src="{{asset("front/img/flags/thai.png")}}" class='my-2'> Thai
                                        <img src="{{asset("front/img/flags/turkish.png")}}" class='my-2'> Turkish
                                        <img src="{{asset("front/img/flags/vietnamese.png")}}" class='my-2'> Vietnamese
                                    </div>

                                    <hr>


                                    <ul class="text-muted text-left package-options">
                                        <li><i class="fas fa-check mr-1"></i>Native translation specialists</li>
                                        <li><i class="fas fa-check mr-1"></i>3-5 Steps of quality assurance</li>
                                        <li><i class="fas fa-check mr-1"></i>24/7 available customer service</li>
                                    </ul>


                                </div>


                            </div>

                            <div class="col-12 col-md-2 package-card-price">
                                <div class="row align-content-center ty ty1 ">
                                    <div class="col-6 col-md-12 pb-2">
                                        <div>You get</div>
                                        <span>1000</span>word
                                    </div>
                                    <div class="col-6 col-md-12">
                                        <div>For only</div>
                                        <span>$200</span>
                                    </div>
                                </div>

                                <div class="row align-content-center ty ty2 ">
                                    <div class="col-6 col-md-12 pb-2">
                                        <div>You get</div>
                                        <span>1000</span>word
                                    </div>
                                    <div class="col-6 col-md-12">
                                        <div>For only</div>
                                        <span>$100</span>
                                    </div>
                                </div>

                                <div class="row align-content-center ty ty3 ">
                                    <div class="col-6 col-md-12 pb-2">
                                        <div>You get</div>
                                        <span>1000</span>word
                                    </div>
                                    <div class="col-6 col-md-12">
                                        <div>For only</div>
                                        <span>$300</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row bg-white my-5 package-card " id="sp-4">
                    <div class="col-12 m-0 p-0 package-rounded">

                        <div class="select-package sp-4 d-none-pack">
                            <i class="fas fa-check-circle"></i>
                        </div>

                        <img class="package-bg" src="{{asset("front/img/package-map.png")}}">
                        <div class="row p-details">
                            <div class="col-12 col-md-10 package-details">
                                <div class="container py-3">
                                    <h6 class="text-uppercase font-weight-bold pt-3">Group 4 Package From (English)</h6>
                                    <span class="text-muted">to the Included Languages:</span>

                                    <div class="mt-3 text-muted">
                                        <img src="{{asset('front/img/flags/afrikaans.png')}}" class='my-2'> Afrikaans
                                        <img src="{{asset('front/img/flags/amharic.png')}}"class='my-2'> Amharic
                                        <img src="{{asset('front/img/flags/bengali.png')}}" class='my-2'> Bengali
                                        <img src="{{asset('front/img/flags/bulgarian.png')}}'" class='my-2'> Bulgarian
                                        <img src="{{asset("front/img/flags/burmese.png")}}" class='my-2'> Burmese
                                        <img src="{{asset("front/img/flags/cambodian.png")}}" class='my-2'> Cambodian (Khmer)<br>
                                        <img src="{{asset("front/img/flags/cantonese.png")}}" class='my-2'> Cantonese
                                        <img src="{{asset("front/img/flags/dari.png")}}" class='my-2'> Dari
                                        <img src="{{asset("front/img/flags/filipino.png")}}" class='my-2'> Filipino
                                        <img src="{{asset("front/img/flags/gujarati.png")}}" class='my-2'> Gujarati
                                        <img src="{{asset("front/img/flags/haitian_creole.png")}}" class='my-2'> Haitian Creole
                                        <img src="{{asset("front/img/flags/hausa.png")}}" class='my-2'> Hausa<br>
                                        <img src="{{asset("front/img/flags/hmong.png")}}" class='my-2'> Hmong
                                        <img src="{{asset("front/img/flags/kannada.png")}}" class='my-2'> Kannada
                                        <img src="{{asset('front/img/flags/karen.png')}}" class='my-2'> Karen
                                        <img src="{{asset('front/img/flags/kurdish.png')}}" class='my-2'> Kurdish
                                        <img src="{{asset('front/img/flags/lao.png')}}" class='my-2'> Lao
                                        <img src="{{asset('front/img/flags/icelandic.png')}}" class='my-2'> Icelandic
                                        <img src="{{asset('front/img/flags/lingala.png')}}" class='my-2'> Lingala<br>
                                        <img src="{{asset('front/img/flags/luo.png')}}" class='my-2'> Luo
                                        <img src="{{asset('front/img/flags/malay.png')}}" class='my-2'> Malay
                                        <img src="{{asset('front/img/flags/marathi.png')}}" class='my-2'> Marathi
                                        <img src="{{asset('front/img/flags/marshallese.png')}}" class='my-2'> Marshallese
                                        <img src="{{asset('front/img/flags/mongolian.png')}}" class='my-2'> Mongolian
                                        <img src="{{asset('front/img/flags/pashto.png')}}" class='my-2'> Pashto<br>
                                        <img src="{{asset('front/img/flags/punjabi.png')}}" class='my-2'> Punjabi
                                        <img src="{{asset('front/img/flags/sinhalese.png')}}" class='my-2'> Sinhalese
                                        <img src="{{asset('front/img/flags/swahili.png')}}" class='my-2'> Swahili
                                        <img src="{{asset('front/img/flags/tamil.png')}}" class='my-2'> Tamil
                                        <img src="{{asset('front/img/flags/urdu.png')}}" class='my-2'> Urdu
                                    </div>

                                    <hr>


                                    <ul class="text-muted text-left package-options">
                                        <li><i class="fas fa-check mr-1"></i>Native translation specialists</li>
                                        <li><i class="fas fa-check mr-1"></i>3-5 Steps of quality assurance</li>
                                        <li><i class="fas fa-check mr-1"></i>24/7 available customer service</li>
                                    </ul>


                                </div>


                            </div>

                            <div class="col-12 col-md-2 package-card-price">
                                <div class="row align-content-center ty ty1 ">
                                    <div class="col-6 col-md-12 pb-2">
                                        <div>You get</div>
                                        <span>1000</span>word
                                    </div>
                                    <div class="col-6 col-md-12">
                                        <div>For only</div>
                                        <span>$250</span>
                                    </div>
                                </div>

                                <div class="row align-content-center ty ty2 ">
                                    <div class="col-6 col-md-12 pb-2">
                                        <div>You get</div>
                                        <span>1000</span>word
                                    </div>
                                    <div class="col-6 col-md-12">
                                        <div>For only</div>
                                        <span>$125</span>
                                    </div>
                                </div>

                                <div class="row align-content-center ty ty3 ">
                                    <div class="col-6 col-md-12 pb-2">
                                        <div>You get</div>
                                        <span>1000</span>word
                                    </div>
                                    <div class="col-6 col-md-12">
                                        <div>For only</div>
                                        <span>$375</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <form action="" method=POST id='proceed-payment'>
                    <input type='hidden' value='sp-1' name='p' id='package-vl'>
                    <input type='hidden' value='ty1' name='p-type' id='package-type-vl'>

                    <div class="row my-5 justify-content-center">
                        <div class="col-12 col-md-3">
                            <button type="button" class="py-3 px-5 btn btn-secondary cancel-package">Cancel</button>
                        </div>

                        <div class="col-12 col-md-3">
                            <button type="submit" class="py-3 px-5 btn btn-primary" name='sub-form'>Proceed to
                                payment
                            </button>
                        </div>
                    </div>

                    <div class='extra-buttons'>
                        <div class='container'>
                            <div class='row py-4 justify-content-center'>
                                <div class='col-12 col-md-3'>
                                    <button type='button'
                                            class='py-3 px-5 btn btn-secondary cancel-package'>Cancel
                                    </button>
                                </div>

                                <div class='col-12 col-md-3'>
                                    <button type='submit' class='py-3 px-5 btn btn-primary' name='sub-form'>Proceed to
                                        payment
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>


@endsection

@section('scripts')


    <script src="{{asset('front/js/scripts.js')}}"></script>



@endsection
