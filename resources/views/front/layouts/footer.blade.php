<footer>
    <hr>
    <div class="container">
        <div class="footer-logo">
            <a class="navbar-brand" href="./home.html">
                <img src="./img/logo.png" width="200" alt="">
            </a>
        </div>

        <ul>
            <li>
                <a class="nav-secondary" href="home.html">Home</a>
            </li>
            <li>
                <a class="nav-secondary" href="#how-it-works">How it works</a>
            </li>
            <li>
                <a class="nav-secondary" href="./packages.html">Packages/Pricing</a>
            </li>

            <li>
                <a class="nav-secondary" href="#contact-us">Contact us</a>
            </li>
            <li>
                <a class="nav-secondary" href="./terms.html">Terms And Conditions</a>
            </li>
        </ul>

    </div>
</footer>

<div class="scrollToTop">
    <i href="#" class="fa fa-arrow-up fa-4x" id="tapToTop"></i>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
        crossorigin="anonymous"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>
<script src="{{asset('front/js/transnpack.js')}}"></script>
<script>
    var base_url = '{{url('/')}}';
    var login_url = '{{url('/login')}}';
    var sign_up = '{{url('/register')}}';
    console.log($('meta[name="csrf-token"]').attr('content'));

    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
            }
        });
    });
</script>
@yield('scripts')
