<!DOCTYPE html>
<html>

@include('front.layouts.header')
<body>

<nav class="navbar navbar-expand-xl navbar-light shadow-sm border-bottom">
    <div class="container">
        <a class="navbar-brand" href="{{route('home')}}">
            <img src="{{asset('front/img/logo.png')}}" width="200" alt="">
        </a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
                aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse justify-content-between pt-1" id="navbarCollapse">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="{{route('home')}}">Home</a>
                </li>

                <li class="nav-item mr-2">
                    <a class="nav-link" href="{{route('home')}}#how-it-works">How it works</a>
                </li>

                <li class="nav-item mr-2">
                    <a class="nav-link packages-link" href="./packages.html">Packages/Pricing</a>
                </li>



                <li class="nav-item mr-2">
                    <a class="nav-link" href="{{route('home')}}#contact">Contact us</a>
                </li>

                @if(Auth::guard('translator')->check())
                    <li class='nav-item mr-2 pt-1'>
                        <a class='btn btn-primary px-3 py-1 text-uppercase account-btns' href='{{route('logout')}}'>Logout
                        </a>

                    </li>
                @else
                <li class='nav-item mr-2'>
                    <a class='nav-link text-uppercase font-weight-bold login-btn account-btns'
                       href='{{route('home.login')}}'>Log In</a>
                </li>
                <li class='nav-item mr-2 pt-1'>
                    <a class='btn btn-primary px-3 py-1 text-uppercase account-btns' data-toggle="modal" id = "sign_up_btn"
                       data-target="#submitModal" href='./signup.html'>Sign
                        Up</a>
                    <div id="submitModal" class="multi-step">
                    </div>
                </li>
                @endif

            </ul>
        </div>
    </div>
</nav>
<!-- Start Body -->
@yield('content')
<!-- End Body -->
@include('front.layouts.footer')

</body>

</html>
