@extends('front.layouts.master')
@section('title','Wizard Page')
@section('styles')
    {{--    <link rel="stylesheet" href="{{asset("front/css/new-styles/international_code.css")}}">--}}

    <link rel="stylesheet" href="{{asset('front/css/new-styles/new_styles.css')}}">

@endsection
@section('content')

    <section class="signup-account">
        <div class="row h-100" style="margin-top: 20px;">
            <div class="container">
                <form method="POST" id="save_profile" action="{{route('save_data')}}">
                    @csrf
                    <div id="example-basic">
                        <h3 class="wizard--step">Step 1</h3>
                        @include('front.wizard.step_one')
                        <h3>Step 2</h3>
                        @include('front.wizard.step_two')
                        <h3>Step 3</h3>
                        @include('front.wizard.step_three')
                        <h3>Step 4</h3>
                        @include('front.wizard.step_four')

                    </div>
                </form>

             </div>
        </div>
    </section>
@endsection

@section('scripts')
    <script src="{{asset('front/js/transnpack.js')}}"></script>
    <script src="{{asset('front/js/scripts.js')}}"></script>
    <script src="{{asset('front/js/jquery.progressBarTimer.js')}}"></script>
    <script src="{{asset('front/js/js/uploader.js')}}"></script>

    <script type="text/javascript">

    </script>
    <script>

        $(function(){
            $("#cv_file_progress_section,#cover_letter_progress_section").hide()
        })

    </script>

    <script>
        function showProgress(id) {
            console.log("id=> ",id)
            $(`#${id}_progress_section`).show();
            $(`#${id}_progress`).progressBarTimer().start();
            $(`#${id}_input`).hide();
            $(`#${id}_input`).val('');
        }
        function stopProgress(id) {
            $(`#${id}_progress_section`).hide();
            $(`#${id}_progress`).progressBarTimer().reset();
            $(`#${id}_input`).show();


            // $(`#${id}`).progressBarTimer().stop();

        }
        $(function(){
            $("#cat_tool_section").hide();
        });

        $("#tools_option").on('change', function () {
            console.log($(this).val());
        })
    </script>

    <script type="text/javascript">

        $(document).ready(function () {
            $('#example-basic').steps({
                headerTag: "h3",
                bodyTag: "section",
                transitionEffect: "slideLeft",
                autoFocus: true,
                stepsOrientation: "vertical",
                onStepChanged: function (event, currentIndex, priorIndex) {
                    if (currentIndex == 2) {
                        $(".content").css("overflow", "scroll")
                    }
                    if (currentIndex == 0) {
                        $("#continue_later").remove()
                    }

                    else {
                        if ($("#continue_later").length == 0) {
                            var $input = $(`
                    <li id="continue_later" onclick="continue_later()" aria-hidden="false" aria-disabled="false"><a  style=" background: #4CAF50;"  class="cursor--pointer" role="menuitem">Save and close</a></li>
                    `);
                            $input.appendTo($('ul[aria-label=Pagination]'));
                        }
                        else {
                            return;
                        }



                    }
                },
                onFinished: function (event, currentIndex) {

                    //Submit Form
                    // Ajax Call TO Submit All Form Data

                    Swal.fire({
                        icon: 'success',
                        title: '',
                        text: 'Your profile was successfully updated',

                    })
                }
            });
            var options = {};
            $('.js-uploader__box').uploader(options);
            $('#cover_letter_progress,#cv_file_progress').progressBarTimer({ autoStart: false, smooth: true, label: { show: true, type: 'percent' }});

            $(".number").css("display","none")
            var input = document.querySelector("#phone_number");
            window.intlTelInput(input, {
                utilsScript: "js/utils.js",
                allowExtensions: true,
                autoFormat: false,
                autoHideDialCode: false,
                autoPlaceholder: false,
                defaultCountry: "auto",
                ipinfoToken: "yolo",
                nationalMode: false,
                numberType: "MOBILE",
                preventInvalidNumbers: true,

            });

            var input = document.querySelector("#whats_app");
            window.intlTelInput(input, {
                utilsScript: "js/utils.js",
                allowExtensions: true,
                autoFormat: false,
                autoHideDialCode: false,
                autoPlaceholder: false,
                defaultCountry: "auto",
                ipinfoToken: "yolo",
                nationalMode: false,
                numberType: "MOBILE",
                preventInvalidNumbers: true,

            });


        });

        function continue_later() {
                SaveForm();
            //Ajax Call To Save the stored data
            Swal.fire({
                icon: 'success',
                title: '',
                text: 'The data was stored successfully',

            })
        }

        function SaveForm() {
            var form_data = new FormData($("#save_profile")[0]);
            $.ajax({
                type: "post",
                url: $("#save_profile").attr('action'),
                data: form_data,
                cache: false,
                contentType: false,
                processData: false,
                success: function (result) {
                    if (result.fail) {
                        $.each(result.errors, function (index, value) {
                            var errorDiv = "#" + index + "_error";

                            $(errorDiv).addClass("required");

                            $(errorDiv)
                                .empty()
                                .append(value);

                            $(errorDiv).fadeIn();
                        });
                    } else {
                        $(".btn-next").addClass("display--none");

                        Swal.fire({
                            icon: 'success',
                            title: '',
                            text: 'Account Created Successfully',

                        });
                        // setTimeout(function(){
                        //     location.href = result.url;
                        // },2000);

                        $this.element.modal('hide');


                    }
                }
            });

        }

    </script>
    <script>
        //Disable All Services Inputs by default
        $(`#translation_section :input`).prop("disabled",true);
        $(`#proofreading_section :input`).prop("disabled",true);
        $(`#media_transcription_section :input`).prop("disabled",true);
        $(`#media_subtitling_section :input`).prop("disabled",true);
        $(`#MTPE_section :input`).prop("disabled",true);
        $(`#DTP_section :input`).prop("disabled",true);
        $(`#editing_section :input`).prop("disabled",true);
        $(`#interpreting_section :input`).prop("disabled",true);
        $(`#typing_section :input`).prop("disabled",true);

        var cat_section_count = 0;
        var cat_tool_section_height = $("#cat_tool_section").height();
        function addSecLang() {
            let check_ref = $("#sec_native_check").attr("check");
            console.log("check=> ", check_ref)
            if (check_ref == 'false') {
                $("#sec_native_check").attr("check", "true")
                $("#sec_lang_select").css("display", "block")
            }
            else {
                $("#sec_native_check").attr("check", "false")
                $("#sec_lang_select").css("display", "none")
            }
        }
        function showServiceSection(serviceID) {

            console.log(serviceID);

            var parent_height = $("#example-basic-p-2").parent('.content').height()
            console.log("parent_height => ",parent_height);
            console.log("cat_tool_section_height => ",cat_tool_section_height)
            let service_check = $(`#${serviceID}`).attr("check");
            if (service_check == 'false') {
                if(serviceID == 'translation'|| serviceID == 'proofreading' || serviceID == 'MTPE'||serviceID == 'editing') {
                    cat_section_count+=1;
                    if($("#cat_tool_section").attr("show") == 'false') {
                        parent_height+=cat_tool_section_height;
                        console.log("parent_height + section of cattools=> ",parent_height)
                    }
                    else{
                        parent_height = parent_height+40;
                    }
                    if(cat_section_count<=4 && cat_section_count!=0){
                        $("#cat_tool_section").show();
                        console.log("show", $("#cat_tool_section").attr("show"))
                        $("#cat_tool_section").attr("show","true");

                    }
                    else {
                        $("#cat_tool_section").hide();
                        $("#cat_tool_section").attr("show","false");

                    }

                }
                /*end service check*/


                $(`#${serviceID}`).attr("check", "true")
                $(`#${serviceID}_section`).removeClass("display--none");
                $(`#${serviceID}_section`).addClass("animated bounceInDown");
                $(`#${serviceID}_section :input`).prop("disabled",false);

                var section_height = $(`#${serviceID}_section`).height();
                console.log("section_height => ",section_height)
                var newHeight = section_height + parent_height;
                console.log("newHeight => ",newHeight)

                $("#example-basic-p-2").parent('.content').css("height", newHeight);

            }
            else {

                var section_height = $(`#${serviceID}_section`).height();
                $(`#${serviceID}_section`).removeClass("animated bounceInDown");

                $(`#${serviceID}_section`).addClass("animated bounceOutUp");

                $(`#${serviceID}`).attr("check", "false");
                setTimeout(function () {
                    $(`#${serviceID}_section`).addClass("display--none");
                    $(`#${serviceID}_section`).removeClass("animated bounceOutUp");
                    $(`#${serviceID}_section :input`).prop("disabled",true);

                }, 900)


                var parent_height = $("#example-basic-p-2").parent('.content').height()
                if(serviceID == 'translation'|| serviceID == 'proofreading' || serviceID == 'MTPE'||serviceID == 'editing') {
                    cat_section_count-=1;
                    console.log("mas7 check",cat_section_count)
                    if(cat_section_count == 0){
                        $("#cat_tool_section").hide();
                        $("#cat_tool_section").attr("show","false");
                        parent_height = parent_height - cat_tool_section_height;
                    }
                    else {
                        parent_height = parent_height - 40;
                    }

                }
                newHeight = parent_height - section_height;
                console.log("parent_height => ", newHeight);
                console.log("Cat Height => ",cat_tool_section_height)
                $("#example-basic-p-2").parent('.content').css("height", newHeight);

            }

        }
    </script>
    <script>
        $(document).ready(function () {
            $('.basic-multiple').select2();
            $(".select2 select2-container select2-container--default").css("width", "100%")
        });
    </script>

    <script>
        var fileTypes = ['pdf', 'docx', 'rtf', 'jpg', 'jpeg', 'png', 'txt'];  //acceptable file types
        function readURL(input) {
            if (input.files && input.files[0]) {
                var extension = input.files[0].name.split('.').pop().toLowerCase(),  //file extension from input file
                    isSuccess = fileTypes.indexOf(extension) > -1;  //is extension in acceptable types

                if (isSuccess) { //yes
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        if (extension == 'pdf') {
                            $(input).closest('.fileUpload').find(".icon").attr('src', 'https://image.flaticon.com/icons/svg/179/179483.svg');
                        }
                        else if (extension == 'docx') {
                            $(input).closest('.fileUpload').find(".icon").attr('src', 'https://image.flaticon.com/icons/svg/281/281760.svg');
                        }
                        else if (extension == 'rtf') {
                            $(input).closest('.fileUpload').find(".icon").attr('src', 'https://image.flaticon.com/icons/svg/136/136539.svg');
                        }
                        else if (extension == 'png') {
                            $(input).closest('.fileUpload').find(".icon").attr('src', 'https://image.flaticon.com/icons/svg/136/136523.svg');
                        }
                        else if (extension == 'jpg' || extension == 'jpeg') {
                            $(input).closest('.fileUpload').find(".icon").attr('src', 'https://image.flaticon.com/icons/svg/136/136524.svg');
                        }
                        else if (extension == 'txt') {
                            $(input).closest('.fileUpload').find(".icon").attr('src', 'https://image.flaticon.com/icons/svg/136/136538.svg');
                        }
                        else {
                            //console.log('here=>'+$(input).closest('.uploadDoc').length);
                            $(input).closest('.uploadDoc').find(".docErr").slideUp('slow');
                        }
                    }

                    reader.readAsDataURL(input.files[0]);
                }
                else {
                    //console.log('here=>'+$(input).closest('.uploadDoc').find(".docErr").length);
                    $(input).closest('.uploadDoc').find(".docErr").fadeIn();
                    setTimeout(function () {
                        $('.docErr').fadeOut('slow');
                    }, 9000);
                }
            }
        }
        $(document).ready(function () {

            $('#first_name,#last_name,#email').addClass('has-val')
            $('.input2').each(function () {
                $(this).on('blur', function () {
                    if ($(this).val().trim() != "") {
                        $(this).addClass('has-val');
                    }
                    else {
                        $(this).removeClass('has-val');
                    }
                })
            })

            $(document).on('change', '.up', function () {
                var id = $(this).attr('id'); /* gets the filepath and filename from the input */
                var profilePicValue = $(this).val();
                var fileNameStart = profilePicValue.lastIndexOf('\\'); /* finds the end of the filepath */
                profilePicValue = profilePicValue.substr(fileNameStart + 1).substring(0, 20); /* isolates the filename */
                //var profilePicLabelText = $(".upl"); /* finds the label text */
                if (profilePicValue != '') {
                    //console.log($(this).closest('.fileUpload').find('.upl').length);
                    $(this).closest('.fileUpload').find('.upl').html(profilePicValue); /* changes the label text */
                }
            });

            $(".btn-new").on('click', function () {
                $("#uploader").append('<div class="row uploadDoc"><div class="col-sm-3"><div class="docErr">Please upload valid file</div><!--error--><div class="fileUpload btn btn-orange"> <img src="https://image.flaticon.com/icons/svg/136/136549.svg" class="icon"><span class="upl" id="upload">Upload document</span><input type="file" class="upload up" id="up" onchange="readURL(this);" /></div></div><div class="col-sm-8"><input type="text" class="form-control" name="" placeholder="Note"></div><div class="col-sm-1"><a class="btn-check"><i class="fa fa-times"></i></a></div></div>');
            });

            $(document).on("click", "a.btn-check", function () {
                if ($(".uploadDoc").length > 1) {
                    $(this).closest(".uploadDoc").remove();
                } else {
                    alert("You have to upload at least one document.");
                }
            });
        });
    </script>
    <script>
        function uploadFile(event) {
            console.log(event.target.value)
        }

    </script>

    <script>
        var listimg = [];
        var check = false;
        function closebtn(index, value) {
            listimg.splice(index, 1);
            $.each(listimg, function (id, value) {
                value.index = id;
            });
            check = true;
            $("#file-1").prop('disabled', false);
            updateList();
        }

        updateList = function () {
            let input = document.getElementById('file-1');
            let output = document.getElementById('fileList');
            let files1 = input.files;
            if (check == true) {
                output.innerHTML = '<ul class="js-uploader__file-list uploader__file-list">';
                for (var i = 0; i < listimg.length; i++) {
                    output.innerHTML += `<li class="js-uploader__file-list uploader__file-list">
                              <span class="uploader__file-list__thumbnail">
                              <img class="thumbnail" id="img_" src="${listimg[i].image}">
                              </span><span class="uploader__file-list__text">${listimg[i].name}</span>
                              <span class="uploader__file-list__size">${(listimg[i].size) / 1000} KB</span>
                              <span class="uploader__file-list__button"></span>
                              <span class="uploader__file-list__button" id="delete" ><button id="close" onclick="closebtn(${listimg[i].index})" class="uploader__icon-button fa fa-times" >
                              </button></span></li>`;
                }
                output.innerHTML += '</ul>';
                check = false;
            }

            else {


                if (window.File && window.FileList && window.FileReader) {

                    for (var i = 0; i < files1.length; i++) {
                        var file = files1[i];
                        var imgReader = new FileReader();
                        imgReader.addEventListener("load", function (event) {
                            var imgFile = event.target;
                            listimg.push({
                                'name': file.name,
                                'size': file.size,
                                'index': listimg.length,
                                'image': imgFile.result
                            });
                            output.innerHTML = '<ul class="js-uploader__file-list uploader__file-list">';
                            for (var i = 0; i < listimg.length; i++) {
                                output.innerHTML += `<li class="js-uploader__file-list uploader__file-list">
                              <span class="uploader__file-list__thumbnail">
                              <img class="thumbnail" id="img_" src="${listimg[i].image}">
                              </span><span class="uploader__file-list__text">${listimg[i].name}</span>
                              <span class="uploader__file-list__size">${(listimg[i].size) / 1000} KB</span>
                              <span class="uploader__file-list__button"></span>
                              <span class="uploader__file-list__button" id="delete" ><button id="close" onclick="closebtn(${listimg[i].index})" class="uploader__icon-button fa fa-times" >
                              </button></span></li>`;
                            }
                            output.innerHTML += '</ul>';

                        });

                        //Read the image
                        imgReader.readAsDataURL(file);
                    }
                }


            }


        }
    </script>

    <script type="text/javascript">
        $(function() {
            $(".lang_option").on('dblclick',function(e){
                e.stopImmediatePropagation()
            })
        })
        jQuery(document).ready(function ($) {
            $('#search').multiselect({

                search: {
                    left: '<input style="    margin-bottom: 10px;    border: 1px solid #c1c1c1!important;" type="text" name="q" class="form-control" placeholder="Search..." />',
                    right: '<input style="    margin-bottom: 10px;    border: 1px solid #c1c1c1!important;" type="text" name="q" class="form-control" placeholder="Search..." />',
                },
                fireSearch: function (value) {
                    return value.length > 0;
                }
            });
        });
        function selectLang() {
            let search_to_length = $("#search_to option").length;
            var selected_items = $("#search :selected").length;

            var total = parseInt(search_to_length) + parseInt(selected_items)
            console.log("total=> ", total)


            if(selected_items <= 2 && search_to_length<2 ) {
                console.log("as3'ar men 2")
                $("#search_rightSelected").removeAttr("disabled");

            }
            if(selected_items > 2 || search_to_length == 2 || (selected_items == 2 &&search_to_length==1 )) {
                console.log("akbar men2")


                $("#search_rightSelected").attr("disabled","true");

            }
            console.log("selected items => ",selected_items)
        }
        // function checkLength() {
        //     let x = $("#search_to option").length + 1
        //     // alert($("#search_to option").length)
        //     if (x == 2) {
        //         $("#search_rightSelected").attr("disabled", true)
        //     }
        //     else {
        //         $("#search_rightSelected").attr("disabled", false)
        //     }
        // }
    </script>
    <script type="text/javascript">
        var newRecord = {};
        var translator_list = [];
        var list_all = { "translation": [], "proofreading": [], "media_subtitling": [], "MTPE": [], "editing": [], "media_transcription": [], "interpreting": [] };
        function addMore(ref,id=0) {
            console.log(ref)
            console.log(list_all[`${ref}`]);
            if (ref == 'media_transcription') {
                list_all[`${ref}`].push({
                    "language": document.querySelector(`#${ref}_lang`).options[document.querySelector(`#${ref}_lang`).selectedIndex].text,
                })
            }
            else if (ref == 'interpreting') {
                console.log("iiii")
                list_all[`${ref}`].push({
                    "source_lang": document.querySelector(`#${ref}_source_lang`).options[document.querySelector(`#${ref}_source_lang`).selectedIndex].text,
                    "target_lang": document.querySelector(`#${ref}_target_lang`).options[document.querySelector(`#${ref}_target_lang`).selectedIndex].text,

                })
            }
            else {
                list_all[`${ref}`].push({
                    "source_lang": document.querySelector(`#${ref}_source_lang`).options[document.querySelector(`#${ref}_source_lang`).selectedIndex].text,
                    "target_lang": document.querySelector(`#${ref}_target_lang`).options[document.querySelector(`#${ref}_target_lang`).selectedIndex].text,
                })
            }

            console.log(list_all)


            $(`#${ref}_table`).empty()

            for (let i = 0; i < list_all[`${ref}`].length; i++) {
                console.log("9090909")

                if (ref == 'media_transcription') {
                    $(`#${ref}_table`).append(`
                        <tr>
                            <td>
                                ${list_all[`${ref}`][i].language}
                            <input type="hidden" value="${list_all[`${ref}`][i].language}" name="services[`+id+`][languages][`+i+`][from_lang_id]"

                            </td>


                            <td ><i onclick="deleteItem('media_transcription',${i},${id})" class="far fa-trash-alt"></i></td>
                        </tr>
                            `)
                }

                else if (ref == 'interpreting') {

                    $(`#${ref}_table`).append(`
                        <tr>
                            <td>
                            ${list_all[`${ref}`][i].source_lang}
                            <input type="hidden" value="${list_all[`${ref}`][i].source_lang}" name="services[`+id+`][languages][`+i+`][from_lang_id]"
                            </td>
                            <td>${list_all[`${ref}`][i].target_lang}
                            <input type="hidden" value="${list_all[`${ref}`][i].target_lang}" name="services[`+id+`][languages][`+i+`][to_lang_id]"
                            </td>


                            <td ><i onclick="deleteItem('${ref}',${i},${id})" class="far fa-trash-alt"></i></td>
                        </tr>
            `)
                }

                else {
                    $(`#${ref}_table`).append(`
                        <tr>
                            <td>
                            ${list_all[`${ref}`][i].source_lang}
                            <input type="hidden" value="${list_all[`${ref}`][i].source_lang}" name="services[`+id+`][languages][`+i+`][from_lang_id]"
                            </td>
                            <td>${list_all[`${ref}`][i].target_lang}
                            <input type="hidden" value="${list_all[`${ref}`][i].target_lang}" name="services[`+id+`][languages][`+i+`][to_lang_id]"
                            </td>
                            <td ><i onclick="deleteItem('${ref}',${i},${id})" class="far fa-trash-alt"></i></td>
                        </tr>
            `)
                }

            }

            console.log("object => ", newRecord)
        }

        function deleteItem(ref, index,id=0) {
            console.log("ref=>", ref)
            console.log("index=>", index);
            console.log(list_all[ref])

            list_all[ref].splice(index, 1)
            console.log(list_all[ref])

            $(`#${ref}_table`).empty()

            for (let i = 0; i < list_all[`${ref}`].length; i++) {
                console.log("9090909")

                if (ref == 'media_transcription') {
                    $(`#${ref}_table`).append(`
                       <tr>
                           <td>${list_all[`${ref}`][i].language}</td>

                           <td ><i onclick="deleteItem('media_transcription',${i},${id})" class="far fa-trash-alt"></i></td>
                       </tr>
                           `)
                }
                else if (ref == 'interpreting') {
                    $(`#${ref}_table`).append(`
                       <tr>
                           <td>${list_all[`${ref}`][i].source_lang}</td>
                           <td>${list_all[`${ref}`][i].target_lang}</td>


                           <td ><i onclick="deleteItem('${ref}',${i},${id})" class="far fa-trash-alt"></i></td>
                       </tr>
                    `)
                }

                else {
                    $(`#${ref}_table`).append(`
                       <tr>
      <td>
                            ${list_all[`${ref}`][i].source_lang}
                            <input type="hidden" value="${list_all[`${ref}`][i].source_lang}" name="services[`+id+`][languages][`+i+`][from_lang_id]"
                            </td>
                            <td>${list_all[`${ref}`][i].target_lang}
                            <input type="hidden" value="${list_all[`${ref}`][i].target_lang}" name="services[`+id+`][languages][`+i+`][to_lang_id]"

                            </td>
                           <td ><i onclick="deleteItem('${ref}',${i},${id})" class="far fa-trash-alt"></i></td>
                       </tr>
                    `)
                }

            }


        }

    </script>

    <script>
        function loadSelectOptions(element, options) {
            if (element.prop("tagName") != "SELECT") return;


                element.html('<option value=""></option>');


            $.each(options, function (key, value) {
                // console.log(value.id);
                // console.log(value.name);
                element.append('<option value=' + value.id + '>' + value.name + '</option>');
            });
            // element.prepend('<option value="" selected></option>');
        }
        function load_cities(source, destination) {
            console.log($(source).val());
            var country = $(source).val();
            if(country != null)
            {
                $.ajax({
                    "type": "POST",
                    "url": "{{route('get_cities')}}",
                    "data": {country: country, _token: "{{ csrf_token() }}"},
                    "success": function (response) {
                        $('.loader').hide();
                        if (response.status == 1) {
                            loadSelectOptions($(destination), response.data);
                        }
                    }
                });

            }
        }
    </script>
@endsection

