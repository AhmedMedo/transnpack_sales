<div class="col-md-12 col-sm-12 col-xs-12 wizard--check-box">


    <input id="DTP" name="services[5][id]" type="checkbox" check="false" value="6"
           onclick="showServiceSection('DTP')">
    <label for="DTP" class="wizard--label-input-service">DTP (Desktop
        Publishing)</label>

    <div id="DTP_section" class="row wizard--service-details display--none">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <fieldset class="scheduler-border">
                <legend class="scheduler-border wizard--title">
                    DTP (Desktop Publishing)
                </legend>
                <div class="control-group">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="row wizard--row">
                            <div class="col-md-12 col-sm-12 col-xs-12 col">
                                <div class="form-group wizard--form-group">
                                    <label class="wizard--form-group-label"><span>
                                                                            </span>Languages Supported</label>
                                    <select class="basic-multiple" name="services[5][languages]"
                                            multiple="multiple">
                                        @foreach($languages as $language)
                                            <option value="{{$language->id}}">{{$language->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </fieldset>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <fieldset class="scheduler-border">
                <legend class="scheduler-border wizard--title">
                    Your preferred rate
                </legend>
                <div class="control-group">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="row wizard--row">
                            <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                <div>
                                    <div class="wrap-input2 validate-input">
                                        <select name="services[5][currency_id]" class="input2 select_new has-val"
                                                id="typing_currency">
                                            @foreach($currencies as $currency)
                                                <option value="{{$currency->id}}">{{$currency->name}}</option>
                                            @endforeach
                                        </select>
                                        <span class="focus-input2"
                                              data-placeholder="Currency"></span>
                                        <div
                                            class="login--forgetpass cursor--pointer">
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                <div>
                                    <div class="wrap-input2 validate-input">
                                        <select class="  input2 select_new has-val"
                                                id="typing_per_unit">
                                            @foreach($per_unit as $unit)
                                                <option value="{{$unit->id}}">{{$unit->name}}</option>
                                            @endforeach
                                        </select>
                                        <span class="focus-input2"
                                              data-placeholder="Per unit"></span>
                                        <div
                                            class="login--forgetpass cursor--pointer">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                <div>
                                    <div
                                        class="wrap-input2 validate-input login--password"
                                        data-validate="Name is required">
                                        <input class="input2" type="number"
                                               name="services[5][rate]"
                                               id="typing_rate">
                                        <span class="focus-input2"
                                              data-placeholder="Rate"></span>
                                        <div
                                            class="login--forgetpass cursor--pointer">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <fieldset class="scheduler-border">
                <legend class="scheduler-border wizard--title">
                    Tools
                </legend>
                <div class="control-group">
                    <div class="row wizard--row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <input name="services[5][support_dtp_tools]" id="tooldtp" type="checkbox" value="yes">
                            <label for="tooldtp"
                                   class="wizard--label-input-service">Do you
                                support DTP Tools?*</label>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group wizard--form-group">
                                <label
                                    class="wizard--form-group-label"><span> </span>If
                                    yes
                                    select the software you support</label>
                                <select id="tools_option2" class="basic-multiple"
                                        name="services[5][softwares][]" multiple="multiple">
                                    @foreach($softwares as $software)
                                        <option value="{{$software->name}}">{{$software->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                        </div>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
</div>
