<div class="col-md-12 col-sm-12 col-xs-12 wizard--check-box">
    <input id="media_transcription" name="services[2][id]" type="checkbox" value="3"
           check="false" onclick="showServiceSection('media_transcription')">
    <label for="media_transcription" class="wizard--label-input-service">Media
        transcription</label>

    <div class="row wizard--service-details display--none"
         id="media_transcription_section">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <fieldset class="scheduler-border">
                <legend class="scheduler-border wizard--title">
                    Media transcription
                </legend>
                <div class="control-group">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div>
                            <div class="wrap-input2 validate-input">
                                <select class="  input2 select_new has-val"
                                        id="media_transcription_lang" required>
                                    @foreach($languages as $language)
                                        <option value="{{$language->id}}">{{$language->name}}</option>
                                    @endforeach
                                </select>
                                <span class="focus-input2"
                                      data-placeholder="Select Language"></span>
                                <div
                                    class="login--forgetpass cursor--pointer"></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="row wizard--row">

                            <div
                                class="col-lg-12 col-md-12 col-sm-12 col-xs-12 wizard--add-more-col">
                                <button type="button" class="btn wizard--add-more"
                                        onclick="addMore('media_transcription',2)">Add
                                    more
                                </button>
                            </div>
                        </div>
                    </div>


                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <hr>
                        <div class="">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th scope="col">Language</th>
                                    <th scope="col">Action</th>
                                </tr>
                                </thead>
                                <tbody id="media_transcription_table">

                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </fieldset>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <fieldset class="scheduler-border">
                <legend class="scheduler-border wizard--title">
                    Your preferred rate
                </legend>
                <div class="control-group">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="row wizard--row">
                            <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                <div>
                                    <div class="wrap-input2 validate-input">
                                        <select name="services[2][currency_id]" class="input2 select_new has-val"
                                                id="typing_currency">
                                            @foreach($currencies as $currency)
                                                <option value="{{$currency->id}}">{{$currency->name}}</option>
                                            @endforeach
                                        </select>
                                        <span class="focus-input2"
                                              data-placeholder="Currency"></span>
                                        <div
                                            class="login--forgetpass cursor--pointer">
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                <div>
                                    <div class="wrap-input2 validate-input">
                                        <select name="services[2][per_unit_id]" class="input2 select_new has-val"
                                                id="typing_per_unit">
                                            @foreach($per_unit as $unit)
                                                <option value="{{$unit->id}}">{{$unit->name}}</option>
                                            @endforeach
                                        </select>
                                        <span class="focus-input2"
                                              data-placeholder="Per unit"></span>
                                        <div
                                            class="login--forgetpass cursor--pointer">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                <div>
                                    <div
                                        class="wrap-input2 validate-input login--password"
                                        data-validate="Name is required">
                                        <input class="input2" type="number"
                                               name="services[2][rate]"
                                               id="typing_rate">
                                        <span class="focus-input2"
                                              data-placeholder="Rate"></span>
                                        <div
                                            class="login--forgetpass cursor--pointer">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <fieldset class="scheduler-border">
                <legend class="scheduler-border wizard--title">
                    Tools
                </legend>
                <div class="control-group">
                    <div class="row wizard--row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <input id="transcription_tools" name="services[2][support_transcription_tools]" type="checkbox"
                                   value="1">
                            <label for="transcription_tools"
                                   class="wizard--label-input-service">Do you
                                support
                                Transcription Tools?*</label>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group wizard--form-group">
                                <label
                                    class="wizard--form-group-label"><span> </span>If
                                    yes
                                    select the software you support</label>
                                <select id="tools_option1" class="basic-multiple"
                                        name="services[2][softwares][]" multiple="multiple">
                                    @foreach($softwares as $software)
                                        <option value="{{$software->name}}">{{$software->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                        </div>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
</div>
