<div class="col-md-12 col-sm-12 col-xs-12 wizard--check-box">
    <input id="media_subtitling" name="services[3][id]" type="checkbox" check="false"
           value="4"
           onclick="showServiceSection('media_subtitling')">
    <label for="media_subtitling" class="wizard--label-input-service">Media
        subtitling</label>

    <div id="media_subtitling_section"
         class="row wizard--service-details display--none">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <fieldset class="scheduler-border">
                <legend class="scheduler-border wizard--title">
                    Media subtitling
                </legend>
                <div class="control-group">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="row wizard--row">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-lg-6">
                                <div>
                                    <div class="wrap-input2 validate-input">
                                        <select class="input2 select_new has-val"
                                                id="media_subtitling_source_lang"
                                                required>
                                            @foreach($languages as $language)
                                                <option value="{{$language->id}}">{{$language->name}}</option>
                                            @endforeach

                                        </select>
                                        <span class="focus-input2"
                                              data-placeholder="Choose Source Language*"></span>
                                        <div
                                            class="login--forgetpass cursor--pointer">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6 col-sm-6 col-xs-12 col-lg-6">
                                <div>
                                    <div class="wrap-input2 validate-input">
                                        <select class="  input2 select_new has-val"
                                                id="media_subtitling_target_lang">
                                            @foreach($languages as $language)
                                                <option value="{{$language->id}}">{{$language->name}}</option>
                                            @endforeach

                                        </select>
                                        <span class="focus-input2"
                                              data-placeholder="Choose Target Language*"></span>
                                        <div
                                            class="login--forgetpass cursor--pointer">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="row wizard--row">


                            <div
                                class="col-lg-12 col-md-12 col-sm-12 col-xs-12 wizard--add-more-col">
                                <button type="button" class="btn wizard--add-more"
                                        onclick="addMore('media_subtitling',3)">Add
                                    more
                                </button>
                            </div>
                        </div>
                    </div>


                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <hr>
                        <div class="">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th scope="col">Source Language</th>
                                    <th scope="col">Target Language</th>
                                    <th scope="col">Action</th>
                                </tr>
                                </thead>
                                <tbody id="media_subtitling_table">
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </fieldset>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <fieldset class="scheduler-border">
                <legend class="scheduler-border wizard--title">
                    Your preferred rate
                </legend>
                <div class="control-group">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="row wizard--row">
                            <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                <div>
                                    <div class="wrap-input2 validate-input">
                                        <select name="services[3][currency_id]" class="input2 select_new has-val"
                                                id="typing_currency">
                                            @foreach($currencies as $currency)
                                                <option value="{{$currency->id}}">{{$currency->name}}</option>
                                            @endforeach
                                        </select>
                                        <span class="focus-input2"
                                              data-placeholder="Currency"></span>
                                        <div
                                            class="login--forgetpass cursor--pointer">
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                <div>
                                    <div class="wrap-input2 validate-input">
                                        <select name="services[3][per_unit_id]" class="input2 select_new has-val"
                                                id="typing_per_unit">
                                            @foreach($per_unit as $unit)
                                                <option value="{{$unit->id}}">{{$unit->name}}</option>
                                            @endforeach
                                        </select>
                                        <span class="focus-input2"
                                              data-placeholder="Per unit"></span>
                                        <div
                                            class="login--forgetpass cursor--pointer">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                <div>
                                    <div
                                        class="wrap-input2 validate-input login--password"
                                        data-validate="Name is required">
                                        <input class="input2" type="number"
                                               name="services[3][rate]"
                                               id="typing_rate">
                                        <span class="focus-input2"
                                              data-placeholder="Rate"></span>
                                        <div
                                            class="login--forgetpass cursor--pointer">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <fieldset class="scheduler-border">
                <legend class="scheduler-border wizard--title">
                    Tools
                </legend>
                <div class="control-group">
                    <div class="row wizard--row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <input name="services[3][support_subtitiling_tools]" id="media_subtitling_tools" type="checkbox"
                                   value="1">
                            <label for="media_subtitling_tools"
                                   class="wizard--label-input-service">Do you
                                support
                                Subtitling Tools?*</label>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group wizard--form-group">
                                <label
                                    class="wizard--form-group-label"><span> </span>If
                                    yes
                                    select the software you support</label>
                                <select class="basic-multiple" name="services[3][softwares][]"
                                        multiple="multiple">
                                    @foreach($softwares as $software)
                                        <option value="{{$software->name}}">{{$software->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div>
                                <div
                                    class="wrap-input2 validate-input login--password">
                                    <input class="input2" type="text"
                                           name="services[3][extensions]">
                                    <span class="focus-input2"
                                          data-placeholder="Extensions supported"></span>
                                    <div class="login--forgetpass cursor--pointer">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>

    </div>
</div>
