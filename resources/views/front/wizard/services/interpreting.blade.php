<div class="col-md-12 col-sm-12 col-xs-12 wizard--check-box">
    <input id="interpreting" name="services[7][id]" type="checkbox" check="false" value="8"
           onclick="showServiceSection('interpreting')">
    <label for="interpreting"
           class="wizard--label-input-service">Interpreting</label>

    <div id="interpreting_section"
         class="row wizard--service-details display--none">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <fieldset class="scheduler-border">
                <legend class="scheduler-border wizard--title">
                    Interpreting
                </legend>
                <div class="control-group">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="row wizard--row">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-lg-6">
                                <div>
                                    <div class="wrap-input2 validate-input">
                                        <select class="  input2 select_new has-val"
                                                id="interpreting_source_lang"
                                                required>
                                            @foreach($languages as $language)
                                                <option value="{{$language->id}}">{{$language->name}}</option>
                                            @endforeach
                                        </select>
                                        <span class="focus-input2"
                                              data-placeholder="Choose Source Language*"></span>
                                        <div
                                            class="login--forgetpass cursor--pointer">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6 col-sm-6 col-xs-12 col-lg-6">
                                <div>
                                    <div class="wrap-input2 validate-input">
                                        <select class="  input2 select_new has-val"
                                                id="interpreting_target_lang"
                                                required>
                                            @foreach($languages as $language)
                                                <option value="{{$language->id}}">{{$language->name}}</option>
                                            @endforeach
                                        </select>
                                        <span class="focus-input2"
                                              data-placeholder="Choose Target Language*"></span>
                                        <div
                                            class="login--forgetpass cursor--pointer">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="row wizard--row">
                            <div
                                class="col-lg-12 col-md-12 col-sm-12 col-xs-12 wizard--add-more-col">
                                <button type="button" class="btn wizard--add-more"
                                        onclick="addMore('interpreting',7)">Add more
                                </button>
                            </div>
                        </div>
                    </div>


                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <hr>
                        <div class="">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th scope="col">Source Language</th>
                                    <th scope="col">Target Language</th>
                                    <th scope="col">Action</th>
                                </tr>
                                </thead>
                                <tbody id="interpreting_table">
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </fieldset>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <fieldset class="scheduler-border">
                <legend class="scheduler-border wizard--title">
                    Your preferred rate
                </legend>
                <div class="control-group">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="row wizard--row">
                            <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                <div>
                                    <div class="wrap-input2 validate-input">
                                        <select class="input2 select_new has-val"
                                                id="typing_currency" name="services[7][currency_id]">
                                            @foreach($currencies as $currency)
                                                <option value="{{$currency->id}}">{{$currency->name}}</option>
                                            @endforeach

                                        </select>
                                        <span class="focus-input2"
                                              data-placeholder="Currency"></span>
                                        <div
                                            class="login--forgetpass cursor--pointer">
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                <div>
                                    <div class="wrap-input2 validate-input">
                                        <select class="input2 select_new has-val"
                                                id="typing_per_unit" name="services[7][per_unit_id]">
                                            @foreach($per_unit as $unit)
                                                <option value="{{$unit->id}}">{{$unit->name}}</option>
                                            @endforeach
                                        </select>
                                        <span class="focus-input2"
                                              data-placeholder="Per unit"></span>
                                        <div
                                            class="login--forgetpass cursor--pointer">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                <div>
                                    <div
                                        class="wrap-input2 validate-input login--password"
                                        data-validate="Name is required">
                                        <input class="input2" type="number"
                                               name="services[7][rate]"
                                               id="typing_rate">
                                        <span class="focus-input2"
                                              data-placeholder="Rate"></span>
                                        <div
                                            class="login--forgetpass cursor--pointer">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <fieldset class="scheduler-border">
                <legend class="scheduler-border wizard--title">
                    Other
                </legend>
                <div class="control-group">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="row wizard--row">
                            <div class="col-md-12 col-sm-12 col-xs-12 col">
                                <input style="display: none;" id="q_1"
                                       type="checkbox" check="false"
                                       value="1" name="services[7][available_for_face_to_face_interpretation]">
                                <label for="q_1"
                                       class="wizard--label-input-service"> Are you
                                    available for face to face
                                    interpretation?*</label>
                            </div>


                            <div class="col-md-12 col-sm-12 col-xs-12 col">
                                <input style="display: none;" id="q_2"
                                       type="checkbox" check="false"
                                       value="1" name="services[7][willing_to_travel]">
                                <label for="q_12"
                                       class="wizard--label-input-service"> Are you
                                    willing to travel if needed?*</label>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12 col">
                                <input style="display: none;" id="q_3"
                                       type="checkbox" check="false"
                                       value="1" name="services[7][available_for_video_interpreting]">
                                <label for="q_3"
                                       class="wizard--label-input-service"> Are you
                                    available for video interpreting?*</label>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12 col">
                                <input style="display: none;" id="q_4"
                                       type="checkbox" check="false"
                                       value="1" name="services[7][available_for_phone_interpreting]">
                                <label for="q_4"
                                       class="wizard--label-input-service">Are you
                                    available for Phone interpreting?*</label>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12 col">
                                <input style="display: none;" id="q_5"
                                       type="checkbox" check="false"
                                       value="1" name="services[7][can_support_asl_interpretation]">
                                <label for="q_5"
                                       class="wizard--label-input-service">Do you
                                    support ASL interpreting service?*</label>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12 col">
                                <input style="display: none;" id="q_6"
                                       type="checkbox" check="false"
                                       value="1" name="services[7][support_bsl_interpreting_services]">
                                <label for="q_6"
                                       class="wizard--label-input-service">Do you
                                    support BSL interpreting service?*</label>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12 col">
                                <input style="display: none;" id="q_7"
                                       type="checkbox" check="false"
                                       value="1" name="services[7][provide_text_translation]">
                                <label for="q_7"
                                       class="wizard--label-input-service">Can you
                                    provide text translation during
                                    meetings?*</label>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12 col">
                                <input style="display: none;" id="q_8"
                                       type="checkbox" check="false"
                                       value="1" name="services[7][provide_meeting_minutes]">
                                <label for="q_8"
                                       class="wizard--label-input-service">Can you
                                    provide meeting minutes during
                                    meetings?*</label>
                            </div>


                        </div>
                    </div>
                </div>
            </fieldset>
        </div>

        <!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <fieldset class="scheduler-border">
                <legend class="scheduler-border wizard--title">
                   Tools
                </legend>
                <div class="control-group">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="row wizard--row">
                            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                                <input id="tool" type="checkbox"  value="yes">
                                <label for="tool" class="wizard--label-input-service">Do you Support CAT tool</label>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                                <div class="form-group wizard--form-group">
                                    <label class="wizard--form-group-label"><span><i class="far fa-dot-circle"></i> </span>If yes select the software you support</label>
                                    <select class="form-control" id="per_unit">
                                        <option>Trados (v. 2019 / 2017 / 2015/ older version</option>
                                        <option>SDL TMS</option>
                                        <option>XTM</option>
                                        <option>MemoQ</option>
                                        <option>Catalyst</option>
                                        <option>Passolo</option>
                                        <option>Google translator</option>
                                        <option>Xbench</option>
                                        <option>Memsource</option>
                                        <option>Across</option>
                                        <option>Transfix</option>
                                        <option>Matecat</option>
                                        <option>Other (please specify)</option>
                                      </select>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>
        </div> -->
    </div>
</div>
