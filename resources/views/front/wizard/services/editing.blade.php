<div class="col-md-12 col-sm-12 col-xs-12 wizard--check-box">
    <input id="editing" name="services[6][id]" type="checkbox" check="false" value="7"
           onclick="showServiceSection('editing')">
    <label for="editing" class="wizard--label-input-service">Editing</label>

    <div id="editing_section" class="row wizard--service-details display--none">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <fieldset class="scheduler-border">
                <legend class="scheduler-border wizard--title">
                    Editing
                </legend>
                <div class="control-group">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="row wizard--row">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-lg-6">
                                <div>
                                    <div class="wrap-input2 validate-input">
                                        <select class="  input2 select_new has-val"
                                                id="editing_source_lang" required>
                                            @foreach($languages as $language)
                                                <option value="{{$language->id}}">{{$language->name}}</option>
                                            @endforeach
                                        </select>
                                        <span class="focus-input2"
                                              data-placeholder="Choose Source Language*"></span>
                                        <div
                                            class="login--forgetpass cursor--pointer">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6 col-sm-6 col-xs-12 col-lg-6">
                                <div>
                                    <div class="wrap-input2 validate-input">
                                        <select class="  input2 select_new has-val"
                                                id="editing_target_lang" required>
                                            @foreach($languages as $language)
                                                <option value="{{$language->id}}">{{$language->name}}</option>
                                            @endforeach                                        </select>
                                        <span class="focus-input2"
                                              data-placeholder="Choose Target Language*"></span>
                                        <div
                                            class="login--forgetpass cursor--pointer">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="row wizard--row">


                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <input id="lqa" name="services[6][provide_lqa]" value="1" type="checkbox">
                                <label for="lqa"
                                       class="wizard--label-input-service">Do you
                                    provide LQA?*</label>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 col">

                                <input style="display: none;" id="editing_sworn"
                                       type="checkbox" check="false"
                                       name="services[6][is_sworn]" value="1">
                                <label for="editing_sworn"
                                       class="wizard--label-input-service"> Are you
                                    sworn translator?</label>
                            </div>
                            <div
                                class="col-lg-12 col-md-12 col-sm-12 col-xs-12 wizard--add-more-col">
                                <button type="button" class="btn wizard--add-more"
                                        onclick="addMore('editing',6)">Add more
                                </button>
                            </div>
                        </div>
                    </div>


                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <hr>
                        <div class="">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th scope="col">Source Language</th>
                                    <th scope="col">Target Language</th>
                                    <th scope="col">Action</th>
                                </tr>
                                </thead>
                                <tbody id="editing_table">
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </fieldset>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <fieldset class="scheduler-border">
                <legend class="scheduler-border wizard--title">
                    Your preferred rate
                </legend>
                <div class="control-group">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="row wizard--row">
                            <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                <div>
                                    <div class="wrap-input2 validate-input">
                                        <select class="input2 select_new has-val"
                                                id="typing_currency" name="services[6][currency_id]">
                                            @foreach($currencies as $currency)
                                                <option value="{{$currency->id}}">{{$currency->name}}</option>
                                            @endforeach

                                        </select>
                                        <span class="focus-input2"
                                              data-placeholder="Currency"></span>
                                        <div
                                            class="login--forgetpass cursor--pointer">
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                <div>
                                    <div class="wrap-input2 validate-input">
                                        <select class="input2 select_new has-val"
                                                id="typing_per_unit" name="services[6][per_unit_id]">
                                            @foreach($per_unit as $unit)
                                                <option value="{{$unit->id}}">{{$unit->name}}</option>
                                            @endforeach
                                        </select>
                                        <span class="focus-input2"
                                              data-placeholder="Per unit"></span>
                                        <div
                                            class="login--forgetpass cursor--pointer">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                <div>
                                    <div
                                        class="wrap-input2 validate-input login--password"
                                        data-validate="Name is required">
                                        <input class="input2" type="number"
                                               name="services[6][rate]"
                                               id="typing_rate">
                                        <span class="focus-input2"
                                              data-placeholder="Rate"></span>
                                        <div
                                            class="login--forgetpass cursor--pointer">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
</div>
