<div class="col-md-12 col-sm-12 col-xs-12 wizard--check-box">
    <input id="typing" type="checkbox" name="services[8][id]" check="false" value="9"
           onclick="showServiceSection('typing')">
    <label for="typing" class="wizard--label-input-service">Typing</label>
    <div id="typing_section" class="row wizard--service-details display--none">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <fieldset class="scheduler-border">
                <legend class="scheduler-border wizard--title">
                    Typing
                </legend>
                <div class="control-group">
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <div class="row wizard--row">
                            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                                <div class="form-group wizard--form-group">
                                    <label class="wizard--form-group-label"><span>
                                     </span>Languages Supported</label>
                                    <select class="basic-multiple" name="services[8][languages][]"
                                            multiple="multiple">
                                        @foreach($languages as $language)
                                            <option value="{{$language->id}}">{{$language->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </fieldset>
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <fieldset class="scheduler-border">
                <legend class="scheduler-border wizard--title">
                    Your preferred rate
                </legend>
                <div class="control-group">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="row wizard--row">
                            <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                <div>
                                    <div class="wrap-input2 validate-input">
                                        <select class="input2 select_new has-val"
                                                id="typing_currency" name="services[8][currency_id]">
                                            @foreach($currencies as $currency)
                                                <option value="{{$currency->id}}">{{$currency->name}}</option>
                                            @endforeach
                                        </select>
                                        <span class="focus-input2"
                                              data-placeholder="Currency"></span>
                                        <div
                                            class="login--forgetpass cursor--pointer">
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                <div>
                                    <div class="wrap-input2 validate-input">
                                        <select class="input2 select_new has-val"
                                                id="typing_per_unit" name="services[8][per_unit_id]">
                                            @foreach($per_unit as $unit)
                                                <option value="{{$unit->id}}">{{$unit->name}}</option>
                                            @endforeach
                                        </select>
                                        <span class="focus-input2"
                                              data-placeholder="Per unit"></span>
                                        <div
                                            class="login--forgetpass cursor--pointer">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                <div>
                                    <div
                                        class="wrap-input2 validate-input login--password"
                                        data-validate="Name is required">
                                        <input class="input2" type="number"
                                               name="services[8][rate]"
                                               id="typing_rate">
                                        <span class="focus-input2"
                                              data-placeholder="Rate"></span>
                                        <div
                                            class="login--forgetpass cursor--pointer">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <fieldset class="scheduler-border">
                <legend class="scheduler-border wizard--title">
                    Tools
                </legend>
                <div class="control-group">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="row wizard--row">
                            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                                <input id="typing_tools" name="services[8][support_certain_typing_softwares]" type="checkbox"
                                       value="1">
                                <label for="typing_tools"
                                       class="wizard--label-input-service">Do you
                                    support
                                    certain typing software?*</label>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                                <label class="wizard--form-group-label">If yes
                                    select the
                                    software you support</label>
                                <textarea name="services[8][softwares]" class="form-control"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
</div>
