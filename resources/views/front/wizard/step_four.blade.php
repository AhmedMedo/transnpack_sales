<section>
    <div class="row">
        <fieldset class="scheduler-border">
            <legend class="scheduler-border wizard--title">
                Availability and documents
            </legend>
            <div class="control-group">
                <div class="row">


                    <div class="col-md-12 col-sm-12 col-xs-12 col">

                        <input style="display: none;" id="rush" type="checkbox" check="false"
                               value="rush">
                        <label for="rush" class="wizard--label-input-service"> Are you available for
                            rush requests?*</label>
                    </div>


                    <div class="col-md-12 col-sm-12 col-xs-12 col">

                        <input style="display: none;" id="weekend" type="checkbox" check="false"
                               value="weekend">
                        <label for="weekend" class="wizard--label-input-service"> Are you available
                            to accept work on weekend and holidays?*</label>
                    </div>

                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <fieldset class="scheduler-border">
                                <legend class="scheduler-border wizard--title">
                                    Files
                                </legend>
                                <div class="control-group">
                                    <div class="row  wizard--row">
                                        <div class="row">
                                            <div
                                                class="col-md-12 col-sm-12 col-xs-12 med--margin-bottom">
                                                <label
                                                    class="wizard--form-group-label"><span></span>Upload
                                                    CV*</label>
                                                <input type="file" id='cv_file_input'
                                                       onchange="showProgress('cv_file')">
                                            </div>


                                            <div class="col-md-12 col-sm-12 col-xs-12"
                                                 id="cv_file_progress_section">
                                                <div class="row">
                                                    <div class="col-md-10 col-sm-10 col-xs-10">
                                                        <div id="cv_file_progress"></div>
                                                    </div>
                                                    <div class="col-md-2 col-sm-2 col-xs-2">
                                                        <i class="fas fa-times cursor--pointer"
                                                           onclick="stopProgress('cv_file')"></i>
                                                    </div>
                                                </div>

                                            </div>

                                            <div
                                                class="col-md-12 col-sm-12 col-xs-12 med--margin-bottom">
                                                <label
                                                    class="wizard--form-group-label"><span></span>Upload
                                                    cover letter</label>
                                                <input id="cover_letter_input" type="file"
                                                       onchange="showProgress('cover_letter')">
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12"
                                                 id="cover_letter_progress_section">
                                                <div class="row">
                                                    <div class="col-md-10 col-sm-10 col-xs-10">
                                                        <div id="cover_letter_progress"></div>
                                                    </div>
                                                    <div class="col-md-2 col-sm-2 col-xs-2">
                                                        <i class="fas fa-times cursor--pointer"
                                                           onclick="stopProgress('cover_letter')"></i>
                                                    </div>
                                                </div>

                                            </div>


                                            <!-- <div class="col-md-12 col-sm-12 col-xs-12 med--margin-bottom">
                                                <div class="wizard--upload-container">
                                                    <div id="fileList"></div>
                                                    <div class="form-group inputfile-upload">
                                                        <input class="inputfile inputfile-1" id="file-1" type="file" name="file-1[]"
                                                            data-multiple-caption="{count} files selected" multiple=""
                                                            onchange="updateList()">
                                                        <label for="file-1" style="    color: #f1e5e6;
                                                      border-radius: 5px;
                                                      background-color: #b5b5b5;
                                                      padding-top: 4px;
                                                      padding-bottom: 8px;"><span style="color: #f7f7f7;
                                                        font-size: 15px;
                                                    ">Upload certificates</span></label>
                                                    </div>
                                                </div>
                                            </div> -->
                                            <div class="col-md-12 col-sm-12 col-xs-12"
                                                 style="margin-top: 15px;">
                                                <label
                                                    class="wizard--form-group-label"><span></span>Upload
                                                    certificates</label>

                                                <div
                                                    class="uploader__box js-uploader__box l-center-box">
                                                    <form action="your/nonjs/fallback/"
                                                          method="POST">
                                                        <div class="uploader__contents">
                                                            <label class="button button--secondary"
                                                                   for="fileinput">Select
                                                                Files</label>
                                                            <input class="uploader__file-input"
                                                                   id="fileinput" type="file"
                                                                   multiple value="Select Files">
                                                        </div>
                                                        <input class="button button--big-bottom"
                                                               type="submit"
                                                               value="Upload Selected Files">
                                                    </form>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>

                </div>

            </div>
        </fieldset>
    </div>
</section>
