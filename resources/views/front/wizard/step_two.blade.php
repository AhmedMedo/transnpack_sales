<section>
    <div class="row">
        <fieldset class="scheduler-border">
            <legend class="scheduler-border wizard--title">
                Languages
            </legend>
            <div class="control-group">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label class="font--bold">Choose your native Languages</label>
                    </div>
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                        <select onchange="selectLang()" name="from[]" id="search"
                                class="form-control" size="8" multiple="multiple">
                            @foreach($languages as $language)
                                <option value="{{$language->id}}" class="lang_option">{{$language->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12"
                         style="padding-top: 50px;">
                        <button type="button " id="search_rightSelected"
                                class="btn btn-block multi-select-btn"
                                onclick="checkLength()"><i class="fas fa-caret-right"></i></button>
                        <button type="button " id="search_leftSelected"
                                class="btn btn-block multi-select-btn"
                                onclick="checkLength()"><i class="fas fa-caret-left"></i></button>
                    </div>

                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                        <select name="languages[]" id="search_to" name="native_languages" class="form-control" size="8"
                                multiple="multiple"></select>
                    </div>
                </div>
            </div>
        </fieldset>

    </div>

</section>
