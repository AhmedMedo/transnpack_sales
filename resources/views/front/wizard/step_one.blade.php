<section>
    <fieldset class="scheduler-border">
        <legend class="scheduler-border wizard--title">
            Basic Info
        </legend>
        <div class="control-group">
            <div class="row  wizard--row">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div>
                        <div class="wrap-input2 validate-input login--password"
                             data-validate="Name is required">
                            <input disabled style="background-color: white;" class="input2"
                                   type="text" id="first_name" name="first_name" value="{{$translator->first_name}}">
                            <span class="focus-input2" data-placeholder="First name"></span>
                            <div class="login--forgetpass cursor--pointer"></div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div>
                        <div class="wrap-input2 validate-input ">
                            <input style="background-color: white;" disabled class="input2"
                                   id="last_name" type="text" name="las_name" value="{{$translator->last_name}}">
                            <span class="focus-input2" data-placeholder="Last name"></span>
                            <div class="login--forgetpass cursor--pointer"></div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div>
                        <div class="wrap-input2 validate-input">
                            <input style="background-color: white;" disabled class="input2"
                                   id="email" type="text" name="email" value="{{$translator->email}}">
                            <span class="focus-input2" data-placeholder="Email"></span>
                            <div class="login--forgetpass cursor--pointer"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!--Country ,city and state-->
            <div class="row  wizard--row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div>
                        <div class="wrap-input2 validate-input">
                            <select class="input2 select_new has-val" id="country" name="country_id" onchange="load_cities(this,city)" required>
                                @foreach($countries as $country)
                                  <option value="{{$country->id}}">{{$country->name}}</option>
                                @endforeach
                            </select>
                            <span class="focus-input2" data-placeholder="Country*"></span>
                            <div class="login--forgetpass cursor--pointer"></div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div>
                        <div class="wrap-input2 validate-input">
                            <select class="input2 select_new has-val" name="city_id" id="city">

                            </select>
                            <span class="focus-input2" data-placeholder="City"></span>
                            <div class="login--forgetpass cursor--pointer"></div>
                        </div>
                    </div>
                </div>
{{--                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">--}}
{{--                    <div>--}}
{{--                        <div class="wrap-input2 validate-input">--}}
{{--                            <select class="input2 select_new has-val">--}}
{{--                                <option>United state</option>--}}
{{--                                <option>Egypt</option>--}}
{{--                            </select>--}}
{{--                            <span class="focus-input2" data-placeholder="State"></span>--}}
{{--                            <div class="login--forgetpass cursor--pointer"></div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
            </div>
            <!--Address 1 and 2 -->
            <div class="row wizard--row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div>
                        <div class="wrap-input2 validate-input">
                            <input class="input2" type="text" name="address">
                            <span class="focus-input2" data-placeholder="Address"></span>
                            <div class="login--forgetpass cursor--pointer"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <div>
                        <div class="wrap-input2 validate-input">
                            <input class="input2" type="text" name="address_2">
                            <span class="focus-input2" data-placeholder="Address(2)"></span>
                            <div class="login--forgetpass cursor--pointer"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div>
                        <div class="wrap-input2 validate-input">
                            <input class="input2" type="text" name="postal_code">
                            <span class="focus-input2" data-placeholder="Postal Code"></span>
                            <div class="login--forgetpass cursor--pointer"></div>
                        </div>
                    </div>
                </div>
                <div
                    class="col-lg-12 col-md-12 col-sm-12 col-xs-12 other-tools">
                    <div class="">
                        <label class="wizard--form-group-label">Biograpghy</label>
                        <textarea name="bio" type="text" class="form-control "></textarea>
                    </div>
                </div>

            </div>
        </div>
    </fieldset>

    <fieldset class="scheduler-border">
        <legend class="scheduler-border wizard--title">
            Communication
        </legend>
        <div class="control-group">
            <div class="row  wizard--row">


                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div>
                        @php
                            $time_zone ="";
                        @endphp
                        <div class="wrap-input2 validate-input">
                            <select class="  input2 select_new has-val" name="timezone" required>
                                <option value="Pacific/Kwajalein"
                                        @if($time_zone=="Pacific/Kwajalein" ) selected
                                    @endif>
                                    (GMT +12:00) Eniwetok,
                                    Kwajalein
                                </option>
                                <option value="Pacific/Samoa"
                                        @if($time_zone=="Pacific/Samoa" ) selected
                                    @endif>
                                    (GMT -11:00) Midway
                                    Island,
                                    Samoa
                                </option>
                                <option value="America/Atka"
                                        @if($time_zone=="America/Atka" ) selected
                                    @endif>
                                    (GMT -10:00) Hawaii
                                </option>
                                <option value="America/Anchorage"
                                        @if($time_zone=="America/Anchorage" ) selected
                                    @endif>
                                    (GMT -9:00) Alaska
                                </option>
                                <option value="America/Dawson"
                                        @if($time_zone=="America/Dawson" ) selected
                                    @endif>
                                    (GMT -8:00) Pacific Time
                                    (US
                                    &amp; Canada)
                                </option>
                                <option value="America/Boise"
                                        @if($time_zone=="America/Boise" ) selected
                                    @endif>
                                    (GMT -7:00) Mountain Time
                                    (US
                                    &amp; Canada)
                                </option>
                                <option value="America/Mexico_City"
                                        @if($time_zone=="America/Mexico_City" ) selected
                                    @endif>
                                    (GMT -6:00) Central
                                    Time
                                    (US &amp; Canada), Mexico City
                                </option>
                                <option value="America/Bogota"
                                        @if($time_zone=="America/Bogota" ) selected
                                    @endif>
                                    (GMT -5:00) Eastern Time
                                    (US
                                    &amp; Canada), Bogota, Lima
                                </option>
                                <option value="America/Caracas"
                                        @if($time_zone=="America/Caracas" ) selected
                                    @endif>
                                    (GMT -4:30) Caracas
                                </option>
                                <option value="America/Santiago"
                                        @if($time_zone=="America/Santiago" ) selected
                                    @endif>
                                    (GMT -4:00) Atlantic
                                    Time
                                    (Canada), La Paz, Santiago
                                </option>
                                <option value="Canada/Newfoundland"
                                        @if($time_zone=="Canada/Newfoundland" ) selected
                                    @endif>
                                    (GMT -3:30)
                                    Newfoundland
                                </option>
                                <option value="Brazil/East"
                                        @if($time_zone=="Brazil/East" ) selected @endif>
                                    (GMT
                                    -3:00) Brazil East
                                </option>
                                <option value="Brazil/West"
                                        @if($time_zone=="Brazil/West" ) selected @endif>
                                    (GMT
                                    -4:00) Brazil West
                                </option>
                                <option value="Atlantic/South_Georgia"
                                        @if($time_zone=="Atlantic/South_Georgia" )
                                        selected @endif>(GMT -2:00)
                                    Mid-Atlantic
                                </option>
                                <option value="Atlantic/Azores"
                                        @if($time_zone=="Atlantic/Azores" ) selected
                                    @endif>
                                    (GMT -1:00) Azores, Cape
                                    Verde Islands
                                </option>
                                <option value="Europe/London"
                                        @if($time_zone=="Europe/London" ) selected
                                    @endif>
                                    (GMT) Western Europe Time,
                                    London, Lisbon, Casablanca, Greenwich
                                </option>
                                <option value="Europe/Paris"
                                        @if($time_zone=="Europe/Paris" ) selected
                                    @endif>
                                    (GMT +1:00) Brussels,
                                    Copenhagen, Madrid, Paris
                                </option>
                                <option value="Africa/Cairo"
                                        @if($time_zone=="Africa/Cairo" ) selected
                                    @endif>
                                    (GMT +2:00) Kaliningrad,
                                    South
                                    Africa, Cairo
                                </option>
                                <option value="Asia/Baghdad"
                                        @if($time_zone=="Asia/Baghdad" ) selected
                                    @endif>
                                    (GMT +3:00) Baghdad,
                                    Riyadh,
                                    Moscow, St. Petersburg
                                </option>
                                <option value="Asia/Tehran"
                                        @if($time_zone=="Asia/Tehran" ) selected @endif>
                                    (GMT
                                    +3:30) Tehran
                                </option>
                                <option value="Asia/Muscat"
                                        @if($time_zone=="Asia/Muscat" ) selected @endif>
                                    (GMT
                                    +4:00) Abu Dhabi,
                                    Muscat,
                                    Yerevan, Baku, Tbilisi
                                </option>
                                <option value="Asia/Kabul"
                                        @if($time_zone=="Asia/Kabul" ) selected @endif>
                                    (GMT +4:30) Kabul
                                </option>
                                <option value="Asia/Karachi"
                                        @if($time_zone=="Asia/Karachi" ) selected
                                    @endif>
                                    (GMT +5:00) Ekaterinburg,
                                    Islamabad, Karachi, Tashkent
                                </option>
                                <option value="Asia/Kolkata"
                                        @if($time_zone=="Asia/Kolkata" ) selected
                                    @endif>
                                    (GMT +5:30) Mumbai,
                                    Kolkata,
                                    Chennai, New Delhi
                                </option>
                                <option value="Asia/Katmandu"
                                        @if($time_zone=="Asia/Katmandu" ) selected
                                    @endif>
                                    (GMT +5:45) Katmandu
                                </option>
                                <option value="Asia/Dhaka"
                                        @if($time_zone=="Asia/Dhaka" ) selected @endif>
                                    (GMT +6:00) Almaty, Dhaka,
                                    Colombo
                                </option>
                                <option value="Asia/Rangoon"
                                        @if($time_zone=="Asia/Rangoon" ) selected
                                    @endif>
                                    (GMT +6:30) Yangon, Cocos
                                    Islands
                                </option>
                                <option value="Asia/Bangkok"
                                        @if($time_zone=="Asia/Bangkok" ) selected
                                    @endif>
                                    (GMT +7:00) Bangkok, Hanoi,
                                    Jakarta
                                </option>
                                <option value="Asia/Hong_Kong"
                                        @if($time_zone=="Asia/Hong_Kong" ) selected
                                    @endif>
                                    (GMT +8:00) Beijing,
                                    Perth,
                                    Singapore, Hong Kong
                                </option>
                                <option value="Asia/Tokyo"
                                        @if($time_zone=="Asia/Tokyo" ) selected @endif>
                                    (GMT +9:00) Tokyo, Seoul,
                                    Osaka,
                                    Sapporo, Yakutsk
                                </option>
                                <option value="Australia/Adelaide"
                                        @if($time_zone=="Australia/Adelaide" ) selected
                                    @endif>
                                    (GMT +9:30) Adelaide,
                                    Darwin
                                </option>
                                <option value="Australia/ACT"
                                        @if($time_zone=="Australia/ACT" ) selected
                                    @endif>
                                    (GMT +10:00) Eastern
                                    Australia, Guam, Vladivostok
                                </option>
                                <option value="Asia/Magadan"
                                        @if($time_zone=="Asia/Magadan" ) selected
                                    @endif>
                                    (GMT +11:00) Magadan,
                                    Solomon Islands, New Caledonia
                                </option>
                                <option value="Antarctica/McMurdo"
                                        @if($time_zone=="Antarctica/McMurdo" ) selected
                                    @endif>
                                    (GMT +12:00) Auckland,
                                    Wellington, Fiji, Kamchatka
                                </option>

                            </select>
                            <span class="focus-input2" data-placeholder="Time zone*"></span>
                            <div class="login--forgetpass cursor--pointer"></div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div>
                        <div class="wrap-input2 validate-input">
                            <input class="input2" type="text" name="skype_id">
                            <span class="focus-input2" data-placeholder="Skype ID"></span>
                            <div class="login--forgetpass cursor--pointer"></div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="row  wizard--row">

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">

                    <div>
                        <div class="wrap-input2 validate-input">
                            <input class="with_code input2" style="padding-top: 20px;"
                                   id="phone_number" name="phone_number" type="tel">

                            <span class="focus-input2" data-placeholder="Phone number"></span>
                            <div class="login--forgetpass cursor--pointer"></div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div>
                        <div class="wrap-input2 validate-input">
                            <input class="with_code input2" style="padding-top: 20px;"
                                   id="whats_app" name="whats_app" type="tel">

                            <span class="focus-input2" data-placeholder="WhatsApp number"></span>
                            <div class="login--forgetpass cursor--pointer"></div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </fieldset>

</section>
