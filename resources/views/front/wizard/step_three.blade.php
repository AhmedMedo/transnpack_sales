<section>
    <fieldset class="scheduler-border">
        <legend class="scheduler-border wizard--title">
            Services
        </legend>
        <div class="control-group">
            <div class="row wizard--row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <p class="wizard--service">Select services you provide</p>
                </div>

                @foreach($services as $service)
                    @include('front.wizard.services.'.$service->slug_name,['languages'=>$languages])
               @endforeach

            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 20px;">

                    <div class="form-group wizard--form-group">
                        <p class="wizard--service">Choose your field(s) of speciality</p>

                        <select class="basic-multiple" name="specialities[]" multiple="multiple">
                            @foreach($specialities as $sepeciality)
                              <option value="{{$sepeciality->id}}">{{$sepeciality->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="row wizard--row">
                        <!-- <div class="col-md-6 col-sm-12 col-xs-12">
                            <div>
                                <div class="wrap-input2 validate-input">
                                    <select class="  input2 select_new has-val">
                                        <option>Yes</option>
                                        <option>No</option>
                                    </select>
                                    <span class="focus-input2" data-placeholder="Rates negotiable*"></span>
                                    <div class="login--forgetpass cursor--pointer"> </div>
                                </div>
                            </div>
                        </div> -->

                        <div class="col-md-12 col-sm-12 col-xs-12 col">

                            <input style="display: none;" id="rates_negotiable" type="checkbox"
                                   check="false"
                                   name="rates_negotiable"
                                   value="1">
                            <label for="rates_negotiable" class="wizard--label-input-service"> Rates
                                negotiable*</label>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12 col">

                            <input style="display: none;" id="accpet_paypal" type="checkbox"
                                   check="false"
                                   name="accpet_paypal"
                                   value="1">
                            <label for="accpet_paypal" class="wizard--label-input-service"> Accept Paypal</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 col">

                            <input style="display: none;" id="accept_bank_transfer" type="checkbox"
                                   check="false"
                                   name="accept_bank_transfer"
                                   value="1">
                            <label for="accept_bank_transfer" class="wizard--label-input-service"> Accept Bank Transfer</label>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12 col">

                            <input style="display: none;" id="accept_volunteer_work" type="checkbox"
                                   check="false"
                                   name="accept_volunteer_work"
                                   value="1">
                            <label for="accept_volunteer_work" class="wizard--label-input-service"> Accept Volunteer Work</label>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="row wizard--row" id="cat_tool_section" show="false">

                                <div class="col-md-12 col-sm-12 col-xs-12 col">

                                    <input style="display: none;" id="tools" type="checkbox"
                                           check="false"
                                           value="1"
                                            name="support_cat_tools">
                                    <label for="tools" class="wizard--label-input-service">Do you
                                        Support
                                        CAT tools?*</label>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 col"
                                     style="margin-top: 12px;">
                                    <div class="form-group wizard--form-group">
                                        <p class="wizard--service">If yes select
                                            the software you support</p>
                                        <!-- <label class="wizard--form-group-label"><span> </span></label> -->
                                        <select id="tools_option0" class="basic-multiple"
                                                name="softwares[]"
                                                multiple="multiple">
                                            @foreach($softwares as $software)
                                                <option value="{{$software->id}}">{{$software->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div
                                        class="col-lg-12 col-md-12 col-sm-12 col-xs-12 other-tools">
                                        <div class="">
                                            <label class="wizard--form-group-label">Other</label>
                                            <textarea name="other_softwares" type="text" class="form-control "></textarea>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </fieldset>

</section>
