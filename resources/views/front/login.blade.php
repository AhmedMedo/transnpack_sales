@extends('front.layouts.master')
@section('title','Login Page')
@section('styles')
{{--    <link rel="stylesheet" href="{{asset("front/css/new-styles/international_code.css")}}">--}}

    <link rel="stylesheet" href="{{asset('front/css/new-styles/new_styles.css')}}">

@endsection
@section('content')
    <section class="signup-account">
        <div class="row h-100">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-6 login-content">
                        <div class="container">
                            <div class="row">
                                <div class="col-12 col-md-12 col-lg-12 mx-auto">
                                    <!-- <h4 class="font-weight-bold " style="font-family: 'Montserrat', sans-serif">You are now one step away from getting your translation process <span>easier than ever.</span></h4> -->
                                    <h4 class="font-weight-bold login--typing"
                                        style="font-family: 'Montserrat', sans-serif;font-size: 25px;"></h4>
                                    <img class="animated bounceInLeft" src="{{asset('front/img/log-in.png')}}" width="100%">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 login-content">

                        <div class="alert alert-danger"></div>

                        <div class="container">
                            <div class="row animated bounceInRight">
                                <div class="col-12 col-md-10 col-lg-10 mx-auto">
                                    <div class="card">
                                        <div class="card-body login--welcome">
                                            <h5 class="card-title welcon-txt font--bold"
                                                style="font-family: 'Nanum Gothic', sans-serif;">Welcome To TransnPack
                                            </h5>
                                        </div>
                                        <div class="card-body">
                                            <div class="row">

                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="wrap-input2 validate-input"
                                                         data-validate="Valid email is required: ex@abc.xyz">
                                                        <input class="input2" type="text" name="email">
                                                        <span class="focus-input2"
                                                              data-placeholder="Email*"></span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="wrap-input2 validate-input login--password"
                                                         data-validate="Name is required">
                                                        <input class="input2" type="password" name="password">
                                                        <span class="focus-input2" data-placeholder="Password*"></span>
                                                        <div class="login--forgetpass cursor--pointer"
                                                             id="forget-password">Forgot password?</div>
                                                    </div>

                                                    <div
                                                        class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center login--btn--col">
                                                        <button class="btn btn-primary login--btn">Sign in</button>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <p class="login--or text-center">Or</p>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center "
                                                         style="margin-top: 20px;">
                                                        <p class="font--bold login--social cursor--pointer"><img
                                                                src="{{asset("front/img/login_page/Google.png")}}">Sign in with Google
                                                        </p>
                                                        <p class="font--bold login--social login--fb cursor--pointer">
                                                            <img src="{{asset("front/img/login_page/Facebook_icon_2013.svg.png")}}">Sign
                                                            in with Google</p>
                                                    </div>

                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                                                        <p class="login--register">New to TransnPack? <span
                                                                data-toggle="modal" data-target="#submitModal"
                                                                class="cursor--pointer">Create
                                                                new account</span></p>
                                                    </div>
                                                    <!-- Modal -->
                                                    <div id="submitModal" class="multi-step">
                                                    </div>

                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <script src="{{asset('front/js/scripts.js')}}"></script>


    <script>
        $(function () {

            $('.input2').each(function () {
                $(this).on('blur', function () {
                    if ($(this).val().trim() != "") {
                        $(this).addClass('has-val');
                    }
                    else {
                        $(this).removeClass('has-val');
                    }
                })
            })

        })
    </script>

    <script>
        $('#forget-password').on('click', function () {
            $(".card").empty()
            $(".card").addClass('animated flipInY')
            $('.card').append(`
                <div class="card-body login--welcome">
                    <h5 class="card-title welcon-txt font--bold"
                        style="font-family: 'Nanum Gothic', sans-serif;">Can't login? forgot your Password?
                    </h5>
                    <p class="forget--desc">Enter your email address below and we will send you password reset instructions.</p>

                </div>
                <div class="card-body">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">

                            <div class="wrap-input2 validate-input"
                                data-validate="Valid email is required: ex@abc.xyz">
                                <input class="input2" type="text" name="email">
                                <span class="focus-input2"
                                    data-placeholder="Email address*"></span>
                                    <div class="login--forgetpass color--error"></div>

                            </div>
                    </div>
                    <div
                    class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center login--btn--col">
                    <button class="btn btn-primary login--btn">Send Reset Instructions</button>
                </div>
                <hr>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <small>
                        <span class="color--main font--bold">A note</span> about spam filters  if you do not get an email from us within a few minutes please be sure to chek your spam filter.  <span class="color--dark font--bold">the email will be coming from no-reply@transnpack.com</span>
                    </small>
                </div>
                </div>

                </div>
            `);
            $('.input2').each(function () {
                $(this).on('blur', function () {
                    if ($(this).val().trim() != "") {
                        $(this).addClass('has-val');
                    }
                    else {
                        $(this).removeClass('has-val');
                    }
                })
            })
        })


    </script>


    <script>
        $(function () {
            var typed = new Typed('.login--typing', {
                strings: ["", "You are now one step away from getting your translation process <span>easier than ever.</span>"],
                typeSpeed: 30
            });

        })
    </script>
@endsection

