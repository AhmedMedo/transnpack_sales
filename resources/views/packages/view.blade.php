@extends('layouts.app')
@section('title','Package Report') 
@section("header")
    <h1>Packages
        <small>View Package</small>
    </h1>
@endsection
@push('styles')


@endpush
@section("content")
<section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-globe"></i>Package :  {{$package->package_ID}}.
            <small class="pull-right">Date: {{\Carbon\Carbon::now()->format('m/d/Y')}}</small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">
        <!-- /.col -->
        <div class="col-sm-6 invoice-col">
          <strong>Client</strong>
          <address>
            Name : {{@$package->client->fullname}}<br>
            Email: {{@$package->client->email}}
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-6 invoice-col">
          <b>Package #{{$package->package_ID}}</b><br>
          <b>Package Type : </b> {{@$package->type->name}}<br>
          <b> Field Of Text :</b> {{ucwords(str_replace('_',' ',$package->field_of_text))}}<br>
          <b>Total Package Words : </b> {{$package->total_num_of_words}}<br>
          <b>Complementary:</b> {{ $package->is_complementary ? 'Yes' : 'NO'}} <br>
          <b>Discount percentage:</b> {{$package->discount_percentage}} %<br>
          <b>Price Before Discount:</b> {{$package->price_before_discount}} $ <br>
          <b>Discount Amount : </b>$ {{$package->DiscountAmount()}} <br>
          <b> Word Price : </b>$ {{$package->getWordPrice()}}<br>
          <b>Final Price:</b> $ {{$package->price}} <br>
 
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped">
            <thead>
            <tr>
              <th>Source Languages</th>
              <th>Target Languages</th>
              <th>Number of Words</th>
              <th>Word Price</th>
            </tr>
            </thead>
            <tbody>
            @foreach($package->languages as $pck)
            <tr>
              <td>{{$pck->fromLang->name}}</td>
              <td>{{$pck->ToLang->name}}</td>
              <td>{{$pck->num_of_words}}</td>
              <td>{{$pck->word_price}} $</td>
            </tr>
            @endforeach
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
@endsection

@push('scripts')

@endpush
