@extends('layouts.app')
@section("header")
    <h1>Packages
        <small>create Package</small>
    </h1>
@endsection
@section("content")
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Create Package</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form role="form" method="POST" id="package_form" action="{{route('packages.store')}}">
                    @csrf
                    <div class="box-body">
                        <div class="row">
                            <div class="form-group">
                                <div class="col-sm-6">
                                    <label for="input-type">Package Type</label>
                                    <div id="input-type" class="row">
                                        @foreach($package_types as $type)
                                        <div class="col-sm-4">
                                            <label class="radio-inline">
                                                <input type="radio"  name="package_type"  value="{{$type->id}}" checked>{{$type->name}}
                                            </label>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>

                        </div>
                        <br>
                        <div class="row">
                            <div class="form-group col-md-5">
                                <label>From Languages</label>
                                <select id="source" class="form-control">
                                    @foreach($languages as $language)

                                            <option value="{{$language->id}}">{{$language->name}}</option>
                                    @endforeach

                                </select>
                            </div>
                            <div class="form-group col-md-5">
                                <label>Target Languages</label>
                                <select id="target" class="form-control">
                                    @foreach($languages as $language)
                                        <option value="{{$language->id}}">{{$language->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-2" style="margin-top: 25px">
                                <button  class="btn btn-primary" onclick="addToList();return false;"><i class="fa fa-plus"></i> Add more</button>
                            </div>
                        </div>
                        <div class="row" style="padding-top: 20px;">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th scope="col">Source</th>
                                        <th scope="col">Target</th>
                                        <th scope="col">Number of words</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody id="list-body">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label for="input-type">Field Of Text</label>
                                <div id="input-type" class="row">
                                    <div class="col-sm-4">
                                        <label class="radio-inline">
                                            <input type="radio"  name="domain_type"  value="general_domain" checked>General Domain
                                        </label>
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="radio-inline">
                                            <input type="radio" name="domain_type" value="technical_domain">Technical Domain
                                        </label>
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="radio-inline">
                                            <input type="radio"  name="domain_type" value="medical_domain">Medical Domain
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <br>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                            <input type="checkbox" name="is_complementary" value="1" id="complementary">
                                                Is Complementary
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input name="discount" maxlength="3" size="2"  min="0" max="100" type="number" class="form-control" id="discount"  placeholder="Discount Percentage" >
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Client Name</label>
                                        <input type="text" class="form-control" name="client_name" placeholder="Client Name" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Client Email</label>
                                        <input type="email" class="form-control" name="client_email" placeholder="Client Email" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Duration</label>
                                    <select name="duration" class="form-control">
                                        @for($i=1 ; $i<=36 ; $i++)
                                            <option value="{{$i}}">{{$i}} Months</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <button type="button" class="btn btn-primary" id="package_button"> <i class="fa fa-spinner fa-spin" id="loader" style="display: none"></i>Calculate</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-md-12">

            <!-- Profile Image -->
            <div class="box box-primary">
                <div class="box-body box-profile">

                    <h3 class="profile-username text-center">Package Price</h3>


                    <!-- <ul class="list-group list-group-unbordered">
                        <li class="list-group-item">
                            <b>Total Price</b> <a class="pull-right" id="price">Not calculated</a>
                        </li>
                        <li class="list-group-item">
                            <b>Discount</b> <a class="pull-right" id="discount_value">Not calculated</a>
                        </li>
                        <li class="list-group-item">
                            <b>Price Before Discount</b> <a class="pull-right" id="price_before">Not calculated</a>
                        </li>
                        <li class="list-group-item">
                            <b>Discount Amount</b> <a class="pull-right" id="discount_amount">Not calculated</a>
                        </li>
                        <li class="list-group-item">
                            <b>Word Price</b> <a class="pull-right" id="word_price">Not calculated</a>
                        </li>
                        <div id="langs">
                        </div>
                    </ul> -->
                    <div id="result"></div>
                    <form role="form" method="POST" id="confirm_form"  action="{{route('packages.confirm')}}">
                        @csrf
                        <input type="hidden" name="package_id" id="package_id">
                    <button type="submit" class="btn btn-primary btn-block" id="package_confirm" disabled><b>confirm</b></button>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

            <!-- About Me Box -->
            <!-- /.box -->
        </div>

    </div>
@endsection
@push('scripts')
    <script>
        var count=0;
        function addToList() {
            count++;
            let source_lang =  $("#source").find('option:selected').text();
            let source_val=    $("#source").find('option:selected').val();

            let target_lang =  $("#target").find('option:selected').text();
            let target_val   = $("#target").find('option:selected').val();

            if(source_val == target_val)
            {
                alert("You must choose two different languages");
            }else{
                $("#list-body").append(`
                <tr id="row_${count}">
                    <td>
                        ${source_lang}
                        <input type="hidden" value="${source_val}" name="lang[${count}][from_lang_id]">
                     </td>
                    <td>
                    ${target_lang}
                        <input type="hidden" value="${target_val}" name="lang[${count}][to_lang_id]">
                    </td>
                    <td>
                        <input type="number" class="form-control col-md-4" placeholder="number of words" required    name="lang[${count}][num_of_words]">
                    </td>
                    <td onclick="removeRow('row_${count}')" class="remove">Remove</td>
                </tr>
                `)
            }




        }
        function removeRow(row_id) {
            $(`#${row_id}`).remove();
        }


        $("#package_button").click(function(){

            var total = 0;
            if($("#complementary").is(":checked")){
                $("tbody > tr").each(function(){
                    console.log($(this).find("td:nth-child(3) input").val());
                    total = total + parseInt($(this).find("td:nth-child(3) input").val());
                });
                console.log(total);
                if(total > 1000)
                {
                    alert("Total Number Of Words must be less than 1000");
                    return false;

                }
            }

            $("#loader").show();

            $.ajax({
                /* the route pointing to the post function */
                url: $("#package_form").attr('action'),
                type: 'POST',
                /* send the csrf-token and the input to the controller */
                data: $("#package_form").serialize(),
                dataType: 'JSON',
                /* remind that 'data' is the response of the AjaxController */
                success: function (response) {


                    // $("#price").html(response.package.price);
                    // $("#discount_value").html(response.package.discount_percentage);
                    // $("#price_before").html(response.package.price_before_discount);
                    // $("#discount_amount").html(response.discount_amount);
                    // $("#word_price").html(response.word_price);

                    // //REmove dimmed
                     $("#package_id").val(response.package_id);
                    console.log(response.data);
                    $("#result").html(response.data);
                    $("#loader").hide();
                    var n = $(document).height();

                    $('html, body').animate({ scrollTop: n }, 50);
                    $("#package_confirm").removeAttr('disabled');

                    // var langs = response.langs;
                    // var langs_html = '';
                    // langs.forEach(function(element) {
                    //     langs_html = langs_html + `From Language : `+element['from_lang']+' / To Lanugage: '+element['to_lang'] +' / word price: ' + element['word_price'] + ' <br>';

                    // });
                    // $("#langs").html(langs_html);
                }
            });


          
        });

        $("#package_confirm").on('click',function(){
            if(confirm("Are you sure?"))
            {
                 $("#confirm_form").submit();
            
            }else{
                return false;
            }
        });



      $('input[type="checkbox"]').click(function(){
            if($(this).is(":checked")){
                $("#discount").hide();
            }
            else if($(this).is(":not(:checked)")){
                $("#discount").show();
            }
        });
    </script>
@endpush
