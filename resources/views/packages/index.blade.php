@extends('layouts.app')
@section("header")
    <h1>Packages
        <small>List All Packages</small>
    </h1>
@endsection
@push('styles')


@endpush
@section("content")
    @if(session()->has('message'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Alert!</h4>

            {{ session()->get('message') }}

        </div>
    @endif
    <div class="row">
        <!-- /.box -->
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">View All Packages</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>price</th>
                            <th>total words</th>
                            <th>Field of Text</th>
                            <th>languages</th>
                            <th>Package Type</th>
                            <th>Creation Date</th>
                            <th>Paymentd Status</th>
                            <th>Complementary Status</th>
                            <th>Payment link</th>
                            <th>Regenerate Link</th>
                            <th>Show</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($packages as $package)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$package->name}}</td>
                                <td>{{$package->price}} $</td>
                                <td>{{$package->total_num_of_words}}</td>
                                <td>{{$package->field_of_text}}</td>
                                <td>
                                    @if($package->languages)
                                        @foreach($package->languages as $language)
                                            {{$language->fromLang->name}} => {{$language->toLang->name}}
                                            <br>
                                        @endforeach
                                    @endif
                                </td>
                                <td>{{$package->type->name}}</td>
                                <td>
                                    {{$package->created_at}}
                                </td>
                                <td>{{$package->payment_status ? 'used' : 'not used'}}</td>
                                <td>{{$package->complementary_status ? 'used' : 'not used'}}</td>
                                <td>
                                    <a href="https://transnpack.com/payment.php?user_id={{$package->client_id}}&new_package_id={{$package->id}}" class="btn btn-success">view link</a>
                                </td>
                                <td>
                                    <a href="{{route('resend_email',$package->id)}}" class="btn btn-danger">Regenerate</a>
                                </td>
                                <td>
                                    <a href="{{route('packages.show',$package->id)}}"><i class="fa fa-eye"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    {!! $packages->links() !!}
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
@endsection

@push('scripts')

@endpush
