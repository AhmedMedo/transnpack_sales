<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Client;
class Package extends Model
{
    //
    public  $table='packages';
    public $timestamps = false;

    public function client()
    {
        return $this->belongsTo(Client::class , 'user_id');
    }

    public function GetUsagePercentage()
    {
        $usage_percentage = 100 - ceil(($this->current_word_count/$this->word_count) * 100);
        return $usage_percentage;
    }

    public function GetWordsConsumed()
    {
        ;
        return $this->word_count -$this->current_word_count ;
    }


}
