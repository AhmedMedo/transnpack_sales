<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class ProjectRequest extends Model
{
    //
    use LogsActivity;

    protected $guarded = ['id'];


   protected static $logAttributes = "*";

   protected static $logUnguarded = true;

   protected static $logOnlyDirty = true;


    public function service()
    {
        return $this->belongsTo(Service::class,'service_id');
    }

    public function Fromlanguage()
    {
        return $this->belongsTo(Language::class,'from_lang_id');

    }

    public function Tolanguage()
    {
        return $this->belongsTo(Language::class,'from_lang_id');

    }

    public function client()
    {
        return $this->belongsTo(AccountClient::class,'client_id');

    }

    public function AssginedFrom()
    {
        return $this->belongsTo(Manager::class,'assigned_by');

    }

    public function agent()
    {
        return $this->belongsTo(AccountClientAgent::class,'agent_id');

    }

    public function notes()
    {
        return $this->hasMany(ProjectRequestNotes::class , 'project_id');
    }

    public function assignes()
    {
        return $this->belongsToMany(Manager::class , 'project_request_notes','project_id','assigned_to');
    }

    public function documents()
    {
        return $this->hasMany(ProjectRequestFiles::class , 'project_id');
    }

}
