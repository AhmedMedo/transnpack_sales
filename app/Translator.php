<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Translator extends Authenticatable
{
    //
    protected $fillable = ['first_name','token','last_name','email','password','is_active','type','first_address','second_address','company_website',
        'company_name','company_address','country_id','city_id','state_id','timezone','state','zip_code','international_code','phone',
        'skype_id','whatsapp_number','field_of_specialty','available_for_rush_request','can_accept_work_at_weekends','cv_path',
        'cover_letter_path','acknowledgement_confirmation','email_verified_at','social_token','social_provider','last_login_at','status'];

    public function languages()
    {
        return $this->belongsToMany('App\Languages','translator_languages','translator_id','language_id');
    }
}
