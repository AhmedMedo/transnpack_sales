<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sales;
use Illuminate\Support\Str;
use App\Mail\ForgetPasswordMail;
use Mail;

class SalesController extends Controller
{
    //
    public function showLoginForm()
    {
        return view('auth.login');
    }

    public function login(Request $request)
    {
        $sales = Sales::whereMail($request->email)->where('roles','OPERATOR')->first();
        if ($sales) {
            $password = sha1(md5($request->password));
            if ($password == $sales->password) {
                \Auth::guard('sales')->login($sales);
                return redirect()->route('dashboard');
            } else {
                return redirect()->back()->withErrors(['password' => 'Wrong Password']);
            }
        } else {
            return redirect()->back()->withErrors(['email' => 'No user with this email']);
        }
    }


    public function logout()
    {
        \Auth::guard("sales")->logout();
        return redirect()->route('sales_login');
    }

    public function forget_password()
    {
        return view('auth.forget');

    }

    public function submit_forget_password(Request $request)
    {
        $sales = Sales::whereMail($request->email)->where('roles', 'OPERATOR')->first();
        if ($sales) {
            $password = Str::random(10);
           $sales->password =$password;
           $sales->save();
           Mail::to($sales->mail)->send(new ForgetPasswordMail($password,$sales));

        } else {
            return redirect()->back()->withErrors(['email' => 'No user with this email']);
        }

        return redirect()->route('login')->with('message','Check Your Email with your new password');

    }


}
