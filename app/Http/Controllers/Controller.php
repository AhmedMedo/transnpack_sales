<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public  function  ClientPassword($password , $email)
    {
        $salting = "transpack";
        $hash_email=md5($email);
        $hash_password=md5($password);
        $hash_salting=md5($salting);
        $random_token=$hash_email.$hash_password.$hash_salting;
        $final_password=md5($random_token);
        return $final_password;

    }
}
