<?php

namespace App\Http\Controllers\Operations;

use App\Manager;
use App\Service;
use App\Language;
use App\AccountClient;
use App\ProjectRequest;
use App\AccountClientAgent;
use App\ProjectRequestFiles;
use App\ProjectRequestNotes;
use Illuminate\Http\Request;
use App\ProjectDeileveryMails;
use App\Mail\ProjectAssignMail;
use App\Mail\UpdateProjectMail;
use App\ProjectRequestLanguage;
use App\Mail\ProjectCompleteMail;
use App\ProjectCompleteDeilevery;
use App\Http\Controllers\Controller;
use App\Mail\NewRequestMail;
use Auth;
use Illuminate\Support\Facades\Mail;
use Spatie\Activitylog\Models\Activity;
use App\Mail\ProjectCompleteMailWrongDeadLine;
use App\Mail\ProjectStatusMail;

class ProjectRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        if($request->input())
        {
            // dd($request->all());
            $projects = ProjectRequest::query()->where('assigned_by',Auth::guard('manager')->user()->id);
            if(!empty($request->project_id))
            {
                $projects->where('projectID','LIKE','%'.$request->project_id.'%');
            }
            if(!empty($request->from_lang_id))
            {
                $projects->where('from_lang_id',$request->from_lang_id);
            }
            if(!empty($request->to_lang_id))
            {
                $projects->where('to_lang_id',$request->to_lang_id);
            }
            if(!empty($request->service))
            {
                $projects->whereHas('service',function($q) use ($request){
                    $q->where('id',$request->service);
                });
            }
            if(!empty($request->from_date))
            {
                $projects->whereDate('created_at','>=',\Carbon\Carbon::parse($request->from_date)->format('Y-m-d'));
            }
            if(!empty($request->to_date))
            {
                $projects->whereDate('created_at','<=',\Carbon\Carbon::parse($request->to_date)->format('Y-m-d'));
            }
            if(!empty($request->client_name))
            {
                $projects->whereHas('client',function($q) use ($request){
                    $q->where('name','LIKE','%'.$request->client_name.'%');
                });
            }
            $projects = $projects->get();


        }else{
            $projects = ProjectRequest::where('assigned_by',Auth::guard('manager')->user()->id)->get();
        }



        $services = Service::all();
        $languages = Language::all();
        $clients = AccountClient::all();


        return view('operations.projects.index',compact(
            'projects',
            'services',
            'languages',
            'clients'
        ));
    }


    /**
     * Project Activity
     */
    public function project_activity($id)
    {
        $activites = Activity::where('subject_id',$id)->get();
        return view('operations.projects.activity',compact('activites'));

    }

    /**
     * Project Status Complete
     */

     public function project_compelete($id)
     {
        return view('operations.projects.project_status',compact('id'));
     }


     public function submit_project_compelete(Request $request , $id)
     {
        $project_delivery = new ProjectCompleteDeilevery();
        $project_delivery->project_id = $id;
        $project_delivery->delivery_quality = $request->delivery_quality;
        $project_delivery->is_deadline_met = $request->is_deadline_met;
        $project_delivery->comments = $request->comments;
        $project_delivery->save();

        $project = ProjectRequest::find($id);
        if($project_delivery->delivery_quality < 90)
        {

            Mail::to(['hany@qtrans.net','nermeen.yusuf@qtrans.net'])->send(new ProjectCompleteMail($project, $project_delivery->delivery_quality));

        }

        if($project_delivery->is_deadline_met == 0 )
        {

            Mail::to(['hany@qtrans.net','nermeen.yusuf@qtrans.net'])->send(new ProjectCompleteMailWrongDeadLine($project));

        }


        ProjectRequest::where('id',$id)->update(['status' => 'completed']);

        return redirect()->route('projects.index');

     }


     /**
      * Reaopn Project
      */

    public function reopen_project($id)
    {
        $project = ProjectRequest::find($id);
        $project->status = 'assigned';
        $project->save();

        return redirect()->back();

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $timezones=
        [
            '(GMT-12:00) International Date Line West',
            '(GMT-11:00) Midway Island, Samoa',
            '(GMT-10:00) Hawaii',
            '(GMT-09:00) Alaska',
            '(GMT-08:00) Pacific Time (US & Canada)',
            '(GMT-08:00) Tijuana, Baja California',
            '(GMT-07:00) Arizona',
            '(GMT-07:00) Chihuahua, La Paz, Mazatlan',
            '(GMT-07:00) Mountain Time (US & Canada)',
            '(GMT-06:00) Central America',
            '(GMT-06:00) Central Time (US & Canada)',
            '(GMT-06:00) Guadalajara, Mexico City, Monterrey',
            '(GMT-06:00) Saskatchewan',
            '(GMT-05:00) Bogota, Lima, Quito, Rio Branco',
            '(GMT-05:00) Eastern Time (US & Canada)',
            '(GMT-05:00) Indiana (East)',
            '(GMT-04:00) Atlantic Time (Canada)',
            '(GMT-04:00) Caracas, La Paz',
            '(GMT-04:00) Manaus',
            '(GMT-04:00) Santiago',
            '(GMT-03:30) Newfoundland',
            '(GMT-03:00) Brasilia',
            '(GMT-03:00) Buenos Aires, Georgetown',
            '(GMT-03:00) Greenland',
            '(GMT-03:00) Montevideo',
            '(GMT-02:00) Mid-Atlantic',
            '(GMT-01:00) Cape Verde Is.',
            '(GMT-01:00) Azores',
            '(GMT+00:00) Casablanca, Monrovia, Reykjavik',
            '(GMT+00:00) Greenwich Mean Time : Dublin, Edinburgh, Lisbon, London',
            '(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna',
            '(GMT+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague',
            '(GMT+01:00) Brussels, Copenhagen, Madrid, Paris',
            '(GMT+01:00) Sarajevo, Skopje, Warsaw, Zagreb',
            '(GMT+01:00) West Central Africa',
            '(GMT+02:00) Amman',
            '(GMT+02:00) Athens, Bucharest, Istanbul',
            '(GMT+02:00) Beirut',
            '(GMT+02:00) Cairo',
            '(GMT+02:00) Harare, Pretoria',
            '(GMT+02:00) Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius',
            '(GMT+02:00) Jerusalem',
            '(GMT+02:00) Minsk',
            '(GMT+02:00) Windhoek',
            '(GMT+03:00) Kuwait, Riyadh, Baghdad',
            '(GMT+03:00) Moscow, St. Petersburg, Volgograd',
            '(GMT+03:00) Nairobi',
            '(GMT+03:00) Tbilisi',
            '(GMT+03:30) Tehran',
            '(GMT+04:00) Abu Dhabi, Muscat',
            '(GMT+04:00) Baku',
            '(GMT+04:00) Yerevan',
            '(GMT+04:30) Kabul',
            '(GMT+05:00) Yekaterinburg',
            '(GMT+05:00) Islamabad, Karachi, Tashkent',
            '(GMT+05:30) Sri Jayawardenapura',
            '(GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi',
            '(GMT+05:45) Kathmandu',
            '(GMT+06:00) Almaty, Novosibirsk',
            '(GMT+06:00) Astana, Dhaka',
            '(GMT+06:30) Yangon (Rangoon)',
            '(GMT+07:00) Bangkok, Hanoi, Jakarta',
            '(GMT+07:00) Krasnoyarsk',
            '(GMT+08:00) Beijing, Chongqing, Hong Kong, Urumqi',
            '(GMT+08:00) Kuala Lumpur, Singapore',
            '(GMT+08:00) Irkutsk, Ulaan Bataar',
            '(GMT+08:00) Perth',
            '(GMT+08:00) Taipei',
            '(GMT+09:00) Osaka, Sapporo, Tokyo',
            '(GMT+09:00) Seoul',
            '(GMT+09:00) Yakutsk',
            '(GMT+09:30) Adelaide',
            '(GMT+09:30) Darwin',
            '(GMT+10:00) Brisbane',
            '(GMT+10:00) Canberra, Melbourne, Sydney',
            '(GMT+10:00) Hobart',
            '(GMT+10:00) Guam, Port Mor'
        ];

        $services = Service::all();
        $languages = Language::all();
        $clients = AccountClient::all();
        $agents = AccountClientAgent::all();
        $managers = Manager::all();
        return view('operations.projects.create',compact('timezones',
        'services',
        'languages',
        'clients',
        'agents',
        'managers'
    ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $project= new ProjectRequest();
        $project->service_id= $request->service;
        $project->projectID= $request->project_id;
        $project->is_certification_required= $request->has('is_certification_required') ? 1:0;
        $project->is_cat_tool_required= $request->has('is_cat_tool_required') ? 1:0;
        $project->software_name= $request->software_name;
        $project->is_retyping_required= $request->has('is_retyping_required') ? 1:0;
        $project->is_dtp_required= $request->has('is_dtp_required') ? 1:0;
        $project->number_of_pages= $request->number_of_pages;
        $project->is_formating_required= $request->has('is_formating_required') ? 1:0;
        $project->dtp_range= $request->dtp_range;
        $project->count= $request->count;
        $project->count_by= $request->count_by;
        $project->count_at= $request->count_at;
        $project->client_rate= $request->client_rate;
        $project->timezone = $request->timzone;
        $project->client_id= $request->client_id;
        $project->agent_id= $request->agent_id;
        $project->client_deadline= $request->client_deadline;
        $project->request_total= $request->request_total;
        $project->status='assigned';
        $project->from_lang_id = $request->from_lang_id;
        $project->to_lang_id = $request->to_lang_id;
        $project->assigned_by = \Auth::guard('manager')->user()->id;
        $project->save();

        //Notes
        if($request->has('assign'))
        {
            if(count($request->assign))
            {
                foreach($request->assign as $assign)
                {
                    $project_notes = new ProjectRequestNotes();
                    $project_notes->project_id = $project->id;
                    $project_notes->assigned_to = $assign['assigned_to'];
                    $project_notes->notes = $assign['notes'];
                    $project_notes->save();
                    //TODO:Send Email Notifications
                    Mail::to($project_notes->AssginedTo->email)->send(new ProjectAssignMail($project));

                }

            }
        }

        if($request->has('paths'))
        {
            if(count($request->paths))
            {
                foreach($request->paths as $path)
                {
                    $project_file=new ProjectRequestFiles();
                    $project_file->project_id = $project->id;
                    $project_file->path = $path;
                    $project_file->save();
                }
            }
        }

        Mail::to(['hany@qtrans.net','nermeen.yusuf@qtrans.net'])->send(new NewRequestMail($project));


        return response()->json(['status'=>1 , 'message'=>'done']);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $project = ProjectRequest::find($id);
        return view('operations.projects.show',compact('project'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $project = ProjectRequest::find($id);
        $timezones=
        [
            '(GMT-12:00) International Date Line West',
            '(GMT-11:00) Midway Island, Samoa',
            '(GMT-10:00) Hawaii',
            '(GMT-09:00) Alaska',
            '(GMT-08:00) Pacific Time (US & Canada)',
            '(GMT-08:00) Tijuana, Baja California',
            '(GMT-07:00) Arizona',
            '(GMT-07:00) Chihuahua, La Paz, Mazatlan',
            '(GMT-07:00) Mountain Time (US & Canada)',
            '(GMT-06:00) Central America',
            '(GMT-06:00) Central Time (US & Canada)',
            '(GMT-06:00) Guadalajara, Mexico City, Monterrey',
            '(GMT-06:00) Saskatchewan',
            '(GMT-05:00) Bogota, Lima, Quito, Rio Branco',
            '(GMT-05:00) Eastern Time (US & Canada)',
            '(GMT-05:00) Indiana (East)',
            '(GMT-04:00) Atlantic Time (Canada)',
            '(GMT-04:00) Caracas, La Paz',
            '(GMT-04:00) Manaus',
            '(GMT-04:00) Santiago',
            '(GMT-03:30) Newfoundland',
            '(GMT-03:00) Brasilia',
            '(GMT-03:00) Buenos Aires, Georgetown',
            '(GMT-03:00) Greenland',
            '(GMT-03:00) Montevideo',
            '(GMT-02:00) Mid-Atlantic',
            '(GMT-01:00) Cape Verde Is.',
            '(GMT-01:00) Azores',
            '(GMT+00:00) Casablanca, Monrovia, Reykjavik',
            '(GMT+00:00) Greenwich Mean Time : Dublin, Edinburgh, Lisbon, London',
            '(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna',
            '(GMT+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague',
            '(GMT+01:00) Brussels, Copenhagen, Madrid, Paris',
            '(GMT+01:00) Sarajevo, Skopje, Warsaw, Zagreb',
            '(GMT+01:00) West Central Africa',
            '(GMT+02:00) Amman',
            '(GMT+02:00) Athens, Bucharest, Istanbul',
            '(GMT+02:00) Beirut',
            '(GMT+02:00) Cairo',
            '(GMT+02:00) Harare, Pretoria',
            '(GMT+02:00) Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius',
            '(GMT+02:00) Jerusalem',
            '(GMT+02:00) Minsk',
            '(GMT+02:00) Windhoek',
            '(GMT+03:00) Kuwait, Riyadh, Baghdad',
            '(GMT+03:00) Moscow, St. Petersburg, Volgograd',
            '(GMT+03:00) Nairobi',
            '(GMT+03:00) Tbilisi',
            '(GMT+03:30) Tehran',
            '(GMT+04:00) Abu Dhabi, Muscat',
            '(GMT+04:00) Baku',
            '(GMT+04:00) Yerevan',
            '(GMT+04:30) Kabul',
            '(GMT+05:00) Yekaterinburg',
            '(GMT+05:00) Islamabad, Karachi, Tashkent',
            '(GMT+05:30) Sri Jayawardenapura',
            '(GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi',
            '(GMT+05:45) Kathmandu',
            '(GMT+06:00) Almaty, Novosibirsk',
            '(GMT+06:00) Astana, Dhaka',
            '(GMT+06:30) Yangon (Rangoon)',
            '(GMT+07:00) Bangkok, Hanoi, Jakarta',
            '(GMT+07:00) Krasnoyarsk',
            '(GMT+08:00) Beijing, Chongqing, Hong Kong, Urumqi',
            '(GMT+08:00) Kuala Lumpur, Singapore',
            '(GMT+08:00) Irkutsk, Ulaan Bataar',
            '(GMT+08:00) Perth',
            '(GMT+08:00) Taipei',
            '(GMT+09:00) Osaka, Sapporo, Tokyo',
            '(GMT+09:00) Seoul',
            '(GMT+09:00) Yakutsk',
            '(GMT+09:30) Adelaide',
            '(GMT+09:30) Darwin',
            '(GMT+10:00) Brisbane',
            '(GMT+10:00) Canberra, Melbourne, Sydney',
            '(GMT+10:00) Hobart',
            '(GMT+10:00) Guam, Port Mor'
        ];

        $services = Service::all();
        $languages = Language::all();
        $clients = AccountClient::all();
        $agents = AccountClientAgent::all();
        $managers = Manager::all();

        return view('operations.projects.edit',compact(
            'project',
            'services',
            'languages',
            'clients',
            'agents',
            'timezones',
            'managers'


        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $project= ProjectRequest::find($id);
        // $project->service_id= $request->service;
        $project->projectID= $request->project_id;
        $project->is_certification_required= $request->has('is_certification_required') ? 1:0;
        $project->is_cat_tool_required= $request->has('is_cat_tool_required') ? 1:0;
        $project->software_name= $request->software_name;
        $project->is_retyping_required= $request->has('is_retyping_required') ? 1:0;
        $project->is_dtp_required= $request->has('is_dtp_required') ? 1:0;
        $project->number_of_pages= $request->number_of_pages;
        $project->is_formating_required= $request->has('is_formating_required') ? 1:0;
        $project->dtp_range= $request->dtp_range;
        $project->count= $request->count;
        $project->count_by= $request->count_by;
        $project->count_at= $request->count_at;
        $project->client_rate= $request->client_rate;
        $project->client_id= $request->client_id;
        $project->agent_id= $request->agent_id;
        $project->client_deadline= $request->client_deadline;
        $project->request_total= $request->request_total;
        $project->from_lang_id = $request->from_lang_id;
        $project->to_lang_id = $request->to_lang_id;
        $project->client_guidelines = $request->client_guidelines;
        $project->save();

        // //Assing Lanugages
        // $project_lang = new ProjectRequestLanguage();
        // $project_lang->project_id = $project->id;
        // $project_lang->from_lang_id = $project->from_lang_id;
        // $project_lang->to_lang_id = $project->to_lang_id;
        // $project_lang->save();

        //Notes
        if($request->has('assign'))
        {
            if(count($request->assign))
            {
                $project->notes()->delete();
                foreach($request->assign as $assign)
                {
                    $project_notes = new ProjectRequestNotes();
                    $project_notes->project_id = $project->id;
                    $project_notes->assigned_to = $assign['assigned_to'];
                    $project_notes->notes = $assign['notes'];
                    $project_notes->save();

                }

            }
        }

        //Send Mail TO LC/PM
        Mail::to('hany@qtrans.net')->send(new UpdateProjectMail($project));

        return redirect()->route('projects.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $project = ProjectRequest::find($id);
        $project->notes()->delete();
        $project->documents()->delete();
        $project->delete();

        return redirect()->back();
    }

    public function upload_files(Request $request)
    {
        $path = $request->file('file')->store('uploads','public');
        // $full_path = public_path('storage'.DIRECTORY_SEPARATOR.$path);
        return response()->json(['path'=>$path]);
    }

    /**
     * Show Project Comments and Emails
     */
    public function ProjectComments()
    {
        return view('operations.projects.comments');
    }

    public function incoming_requests()
    {
        $projects = ProjectRequest::whereHas('notes',function($q){
               $q->where('assigned_to',\Auth::guard('manager')->user()->id);
        })->get();
        return view('operations.projects.requests.incoming',compact('projects'));
    }

    public function outgoing_requests()
    {
        $projects = ProjectRequest::where('assigned_by',\Auth::guard('manager')->user()->id)->get();
        return view('operations.projects.requests.outgoing',compact('projects'));
    }
    public function show_requests($id)
    {
        $project = ProjectRequest::find($id);
        return view('operations.projects.requests.show',compact('project'));

    }

    public function project_status(Request $request,$id)
    {
        $project = ProjectRequest::find($id);
        if($request->accept)
        {
            $project->status = 'accepted';
        }else{
            $project->status = 'rejected';
            $project->rejection_reason = $request->rejection_reason;
        }


        $project->save();

        Mail::to(['hany@qtrans.net','nermeen.yusuf@qtrans.net'])->send(new ProjectStatusMail($project));

        return redirect()->back();
    }

    public function project_new_deadline(Request $request,$id)
    {
        $project = ProjectRequest::find($id);
        $project->new_deadline = $request->new_deadline;
        $project->new_deadline_notes = $request->new_deadline_notes;
        $project->save();

        return redirect()->back();

    }

    public function delivery_mail($id)
    {
        return view('operations.projects.delivery_mail',compact('id'));
    }

    public function send_mail(Request $request,$id)
    {
        $project_delivery = new ProjectDeileveryMails();
        $project_delivery->project_id = $id;
        $project_delivery->to_email = $request->to_email;
        $project_delivery->subject = $request->subject;
        $project_delivery->cc_email = $request->cc_email;
        $project_delivery->body = $request->body;
        $project_delivery->save();
        // echo $request->content

        $project = ProjectRequest::find($id);
        $project->status = 'delivered';
        $project->save();
        //Send Email
        \Mail::send([], [], function($message) use($request)
        {
            $message->to('foo@example.com', 'John Smith')->subject('Welcome!')->setBody($request->body,'text/html');
        });

        return redirect()->back();

    }
}
