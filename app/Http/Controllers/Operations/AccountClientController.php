<?php

namespace App\Http\Controllers\Operations;

use App\Country;
use App\AccountClient;
use App\AccountClientFax;
use App\AccountClientAgent;
use App\AccountClientEmail;
use App\AccountClientPhone;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mail\NewClientMail;
use Mail;
class AccountClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->input())
        {
            $clients = AccountClient::query();
            if(!empty($request->client_id))
            {
                $clients->where('clientID',$request->client_id);
            }

            if(!empty($request->name))
            {
                $clients->where('name','LIKE','%'.$request->name.'%');
            }
            if(!empty($request->from_date))
            {
                $clients->whereDate('created_at','>=',\Carbon\Carbon::parse($request->from_date)->format('Y-m-d'));
            }
            if(!empty($request->to_date))
            {
                $clients->whereDate('created_at','<=',\Carbon\Carbon::parse($request->to_date)->format('Y-m-d'));
            }
            $clients = $clients->get();

        }else{
            $clients = AccountClient::all();


        }
        //
        return view('operations.clients.index',compact('clients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $timezones=
        [
            '(GMT-12:00) International Date Line West',
            '(GMT-11:00) Midway Island, Samoa',
            '(GMT-10:00) Hawaii',
            '(GMT-09:00) Alaska',
            '(GMT-08:00) Pacific Time (US & Canada)',
            '(GMT-08:00) Tijuana, Baja California',
            '(GMT-07:00) Arizona',
            '(GMT-07:00) Chihuahua, La Paz, Mazatlan',
            '(GMT-07:00) Mountain Time (US & Canada)',
            '(GMT-06:00) Central America',
            '(GMT-06:00) Central Time (US & Canada)',
            '(GMT-06:00) Guadalajara, Mexico City, Monterrey',
            '(GMT-06:00) Saskatchewan',
            '(GMT-05:00) Bogota, Lima, Quito, Rio Branco',
            '(GMT-05:00) Eastern Time (US & Canada)',
            '(GMT-05:00) Indiana (East)',
            '(GMT-04:00) Atlantic Time (Canada)',
            '(GMT-04:00) Caracas, La Paz',
            '(GMT-04:00) Manaus',
            '(GMT-04:00) Santiago',
            '(GMT-03:30) Newfoundland',
            '(GMT-03:00) Brasilia',
            '(GMT-03:00) Buenos Aires, Georgetown',
            '(GMT-03:00) Greenland',
            '(GMT-03:00) Montevideo',
            '(GMT-02:00) Mid-Atlantic',
            '(GMT-01:00) Cape Verde Is.',
            '(GMT-01:00) Azores',
            '(GMT+00:00) Casablanca, Monrovia, Reykjavik',
            '(GMT+00:00) Greenwich Mean Time : Dublin, Edinburgh, Lisbon, London',
            '(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna',
            '(GMT+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague',
            '(GMT+01:00) Brussels, Copenhagen, Madrid, Paris',
            '(GMT+01:00) Sarajevo, Skopje, Warsaw, Zagreb',
            '(GMT+01:00) West Central Africa',
            '(GMT+02:00) Amman',
            '(GMT+02:00) Athens, Bucharest, Istanbul',
            '(GMT+02:00) Beirut',
            '(GMT+02:00) Cairo',
            '(GMT+02:00) Harare, Pretoria',
            '(GMT+02:00) Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius',
            '(GMT+02:00) Jerusalem',
            '(GMT+02:00) Minsk',
            '(GMT+02:00) Windhoek',
            '(GMT+03:00) Kuwait, Riyadh, Baghdad',
            '(GMT+03:00) Moscow, St. Petersburg, Volgograd',
            '(GMT+03:00) Nairobi',
            '(GMT+03:00) Tbilisi',
            '(GMT+03:30) Tehran',
            '(GMT+04:00) Abu Dhabi, Muscat',
            '(GMT+04:00) Baku',
            '(GMT+04:00) Yerevan',
            '(GMT+04:30) Kabul',
            '(GMT+05:00) Yekaterinburg',
            '(GMT+05:00) Islamabad, Karachi, Tashkent',
            '(GMT+05:30) Sri Jayawardenapura',
            '(GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi',
            '(GMT+05:45) Kathmandu',
            '(GMT+06:00) Almaty, Novosibirsk',
            '(GMT+06:00) Astana, Dhaka',
            '(GMT+06:30) Yangon (Rangoon)',
            '(GMT+07:00) Bangkok, Hanoi, Jakarta',
            '(GMT+07:00) Krasnoyarsk',
            '(GMT+08:00) Beijing, Chongqing, Hong Kong, Urumqi',
            '(GMT+08:00) Kuala Lumpur, Singapore',
            '(GMT+08:00) Irkutsk, Ulaan Bataar',
            '(GMT+08:00) Perth',
            '(GMT+08:00) Taipei',
            '(GMT+09:00) Osaka, Sapporo, Tokyo',
            '(GMT+09:00) Seoul',
            '(GMT+09:00) Yakutsk',
            '(GMT+09:30) Adelaide',
            '(GMT+09:30) Darwin',
            '(GMT+10:00) Brisbane',
            '(GMT+10:00) Canberra, Melbourne, Sydney',
            '(GMT+10:00) Hobart',
            '(GMT+10:00) Guam, Port Mor'
        ];
        //
        $countries = Country::all();
        return view('operations.clients.create',compact('countries','timezones'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //        //Store
        $client = new AccountClient();
        $client->name = $request->name;
        $client->email = $request->client_email;
        $client->fax = $request->client_fax;
        $client->phone = $request->client_mobile;
        $client->agent_name = $request->agent_name;
        $client->agent_email = $request->agent_email;
        $client->clientID = rand(10000,99999);
        $client->lsp = $request->lsp;
        $client->country_id = $request->country_id;
        $client->address_1 = $request->address_1;
        $client->address_2 = $request->address_2;
        $client->zip_code = $request->zip_code;
        $client->timezone = $request->timezone;
        $client->save();

        //Save agents
        if($request->has('agents'))
        {
            if(count($request->agents))
            {
                foreach($request->agents as $agent)
                {
                    $account_agent = new AccountClientAgent();
                    $account_agent->client_id = $client->id;
                    $account_agent->name = $agent['name'];
                    $account_agent->email = $agent['email'];
                    $account_agent->save();
                }
            }

        }

                //Save agents
            if($request->has('email'))
            {
                if(count($request->email))
                {
                    foreach($request->email as $email)
                    {
                        $account_email = new AccountClientEmail();
                        $account_email->client_id = $client->id;
                        $account_email->email = $email;
                        $account_email->save();
                    }
                }
            }

            if($request->has('phone'))
            {
                if(count($request->phone))
                {
                    foreach($request->phone as $phone)
                    {
                        $account_phone = new AccountClientPhone();
                        $account_phone->client_id = $client->id;
                        $account_phone->phone = $phone;
                        $account_phone->save();
                    }
                }
            }

            if($request->has('fax'))
            {

                if(count($request->fax))
                {
                    foreach($request->fax as $fax)
                    {
                        $account_fax = new AccountClientFax();
                        $account_fax->client_id = $client->id;
                        $account_fax->fax = $fax;
                        $account_fax->save();
                    }
                }
            }

            //New Client Mail
            Mail::to(['hany@qtrans.net','nermeen.yusuf@qtrans.net'])->send(new NewClientMail($client));


        return redirect()->route('clients.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $timezones=
        [
            '(GMT-12:00) International Date Line West',
            '(GMT-11:00) Midway Island, Samoa',
            '(GMT-10:00) Hawaii',
            '(GMT-09:00) Alaska',
            '(GMT-08:00) Pacific Time (US & Canada)',
            '(GMT-08:00) Tijuana, Baja California',
            '(GMT-07:00) Arizona',
            '(GMT-07:00) Chihuahua, La Paz, Mazatlan',
            '(GMT-07:00) Mountain Time (US & Canada)',
            '(GMT-06:00) Central America',
            '(GMT-06:00) Central Time (US & Canada)',
            '(GMT-06:00) Guadalajara, Mexico City, Monterrey',
            '(GMT-06:00) Saskatchewan',
            '(GMT-05:00) Bogota, Lima, Quito, Rio Branco',
            '(GMT-05:00) Eastern Time (US & Canada)',
            '(GMT-05:00) Indiana (East)',
            '(GMT-04:00) Atlantic Time (Canada)',
            '(GMT-04:00) Caracas, La Paz',
            '(GMT-04:00) Manaus',
            '(GMT-04:00) Santiago',
            '(GMT-03:30) Newfoundland',
            '(GMT-03:00) Brasilia',
            '(GMT-03:00) Buenos Aires, Georgetown',
            '(GMT-03:00) Greenland',
            '(GMT-03:00) Montevideo',
            '(GMT-02:00) Mid-Atlantic',
            '(GMT-01:00) Cape Verde Is.',
            '(GMT-01:00) Azores',
            '(GMT+00:00) Casablanca, Monrovia, Reykjavik',
            '(GMT+00:00) Greenwich Mean Time : Dublin, Edinburgh, Lisbon, London',
            '(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna',
            '(GMT+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague',
            '(GMT+01:00) Brussels, Copenhagen, Madrid, Paris',
            '(GMT+01:00) Sarajevo, Skopje, Warsaw, Zagreb',
            '(GMT+01:00) West Central Africa',
            '(GMT+02:00) Amman',
            '(GMT+02:00) Athens, Bucharest, Istanbul',
            '(GMT+02:00) Beirut',
            '(GMT+02:00) Cairo',
            '(GMT+02:00) Harare, Pretoria',
            '(GMT+02:00) Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius',
            '(GMT+02:00) Jerusalem',
            '(GMT+02:00) Minsk',
            '(GMT+02:00) Windhoek',
            '(GMT+03:00) Kuwait, Riyadh, Baghdad',
            '(GMT+03:00) Moscow, St. Petersburg, Volgograd',
            '(GMT+03:00) Nairobi',
            '(GMT+03:00) Tbilisi',
            '(GMT+03:30) Tehran',
            '(GMT+04:00) Abu Dhabi, Muscat',
            '(GMT+04:00) Baku',
            '(GMT+04:00) Yerevan',
            '(GMT+04:30) Kabul',
            '(GMT+05:00) Yekaterinburg',
            '(GMT+05:00) Islamabad, Karachi, Tashkent',
            '(GMT+05:30) Sri Jayawardenapura',
            '(GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi',
            '(GMT+05:45) Kathmandu',
            '(GMT+06:00) Almaty, Novosibirsk',
            '(GMT+06:00) Astana, Dhaka',
            '(GMT+06:30) Yangon (Rangoon)',
            '(GMT+07:00) Bangkok, Hanoi, Jakarta',
            '(GMT+07:00) Krasnoyarsk',
            '(GMT+08:00) Beijing, Chongqing, Hong Kong, Urumqi',
            '(GMT+08:00) Kuala Lumpur, Singapore',
            '(GMT+08:00) Irkutsk, Ulaan Bataar',
            '(GMT+08:00) Perth',
            '(GMT+08:00) Taipei',
            '(GMT+09:00) Osaka, Sapporo, Tokyo',
            '(GMT+09:00) Seoul',
            '(GMT+09:00) Yakutsk',
            '(GMT+09:30) Adelaide',
            '(GMT+09:30) Darwin',
            '(GMT+10:00) Brisbane',
            '(GMT+10:00) Canberra, Melbourne, Sydney',
            '(GMT+10:00) Hobart',
            '(GMT+10:00) Guam, Port Mor'
        ];
        $client = AccountClient::find($id);
        $countries = Country::all();
        return view('operations.clients.edit',compact('countries','client','timezones'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());
        //Update
        $client = AccountClient::find($id);
        $client->name = $request->name;
        $client->email = $request->client_email;
        $client->fax = $request->client_fax;
        $client->phone = $request->client_mobile;
        $client->agent_name = $request->agent_name;
        $client->agent_email = $request->agent_email;
        $client->lsp = $request->lsp;
        $client->country_id = $request->country_id;
        $client->address_1 = $request->address_1;
        $client->address_2 = $request->address_2;
        $client->zip_code = $request->zip_code;
        $client->timezone = $request->timezone;
        $client->save();

        //Save agents
        if($request->has('agents'))
        {
            if(count($request->agents))
            {
                $client->agents()->delete();

                foreach($request->agents as $agent)
                {
                    $account_agent = new AccountClientAgent();
                    $account_agent->client_id = $client->id;
                    $account_agent->name = $agent['name'];
                    $account_agent->email = $agent['email'];
                    $account_agent->save();
                }
            }

        }

                //Save agents
            if($request->has('email'))
            {
                if(count($request->email))
                {
                    $client->emails()->delete();

                    foreach($request->email as $email)
                    {
                        $account_email = new AccountClientEmail();
                        $account_email->client_id = $client->id;
                        $account_email->email = $email;
                        $account_email->save();
                    }
                }
            }

            if($request->has('phone'))
            {
                if(count($request->phone))
                {
                    $client->phones()->delete();

                    foreach($request->phone as $phone)
                    {
                        $account_phone = new AccountClientPhone();
                        $account_phone->client_id = $client->id;
                        $account_phone->phone = $phone;
                        $account_phone->save();
                    }
                }
            }

            if($request->has('fax'))
            {

                if(count($request->fax))
                {
                    $client->faxes()->delete();

                    foreach($request->fax as $fax)
                    {
                        $account_fax = new AccountClientFax();
                        $account_fax->client_id = $client->id;
                        $account_fax->fax = $fax;
                        $account_fax->save();
                    }
                }
            }


        return redirect()->route('clients.index');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $client = AccountClient::find($id);
        $client->emails()->delete();
        $client->phones()->delete();
        $client->faxes()->delete();
        $client->delete();
        return redirect()->route('clients.index');

    }
}
