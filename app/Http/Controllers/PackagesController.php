<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Language;
use App\Services\PricingService;
use App\Client;
use App\UserPackages;
use App\PackageLanguages;
use App\PackageType;
use Illuminate\Support\Str;
use App\Mail\ClientPacakgeMail;
use App\Mail\ClientPasswordMail;
use Mail;
use DB;

class PackagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $packages = UserPackages::where('sales_id',\Auth::guard('sales')->user()->id)->where('confirmed',1)->orderBy('created_at','DESC')->paginate(10);
        return  view('packages.index',compact('packages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
//        dd(\Auth::guard('sales')->user());
        $languages = Language::all();
        $package_types = PackageType::all();
        return view('packages.create',compact('languages','package_types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $pricing = new PricingService();
        $price = $pricing->CalculatePrice($request);
        //Save CLient
        $password = Str::random(10);
        $client = Client::where('email',$request->client_email)->first();
        if(is_null($client))
        {
            \Session::put('pass', $password);

            $client = Client::create([
                'email' => $request->client_email,
                'stripe_id' => '',
                'avatar' => '',
                'activation_token' => '',
                'fullname' => $request->client_name,
                'password' => $this->ClientPassword($password, $request->client_email),
                'created_date' => date("j/n/Y - g:i a"),
                'activate_date' => date("j/n/Y - g:i a"),
            ]);
            // dd($client);
//            Mail::to($client->email)->send(new ClientPasswordMail($password,$client));
        }

        //Save Package
        $package_ID = Str::random(5);
        $package = UserPackages::create([
            'name'               => 'Package '.$package_ID  ,
            'package_ID'         => $package_ID,
            'package_token'      => Str::random('30'),
            'total_num_of_words' => $price['total_words'],
            'field_of_text'      => $request->domain_type,
            'price'              => $price['price'],
            'price_before_discount'=>$price['price_before_discount'],
            'client_id'          => $client->id ,
            'sales_id'           => \Auth::guard('sales')->user()->id,
            'discount_percentage' => $request->discount ?? 0,
            'package_type'        => $request->package_type,
            'is_complementary'    => $request->has('is_complementary') ? 1 : 0,
            'duration'            => $request->duration,
            'confirmed'           =>0
        ]);



        $package->package_payment_link = "https://transnpack.com/payment.php?user_id=" . $package->client_id . "&new_package_id=" . $package->id;
        $package->save();

        //Save Package Langues
        foreach ($request->lang as $lang)
        {
            $package_langauge = PackageLanguages::create([
                'package_id'        =>       $package->id,
                'from_language_id'  =>       $lang['from_lang_id'],
                'to_language_id'    =>       $lang['to_lang_id'],
                'num_of_words'      =>       $lang['num_of_words'],
                'word_price'        => $pricing->GetLanguageDomain($lang['from_lang_id'],$lang['to_lang_id'],$request->domain_type)
            ]);
        }

        //if is complrmentary
        if($request->has('is_complementary'))
        {
            $today_date = date("d F Y");
            $expiery_days = $package->duration *30 . ' days';

           $pck_id= DB::table('packages')->insertGetId([
                'package_token' => Str::random(30),
                'user_id'       => $client->id,
                'package_title' => $package->name,
                'package_type'  => $package->package_type,
                'from_languages'=> @$package->Fromlanguages()->pluck('name')->implode(","),
                'languages'     => @$package->Tolanguages()->pluck('name')->implode(","),
                'text_type'     => '',
                'current_word_count' => $package->total_num_of_words,
                'word_count'         => $package->total_num_of_words,
                'base_words'         =>   $package->total_num_of_words,
                'package_price'     => 1,
                'unix_expiry'       => time() + 2592000,
                'expire_date'       => date('d F Y', strtotime($today_date. ' +'. $expiery_days)),
                'created_date'      => $today_date,
                'is_new_package'    => 1,
                'new_package_id'    => $package->id,
                'duration'          => $package->duration

            ]);

            $package->package_payment_link = "https://transnpack.com/user/dashboard.php?user_id=" . $package->client_id . "&package_id=" . $pck_id;

            $package->save();


        }

//        Mail::to($client->email)->bcc('finance@transnpack.com')->send(new ClientPacakgeMail($package));
        // sleep(5);
        // Mail::to($client->email)->send(new ClientPasswordMail($password,$client));

//        return  redirect()->route('packages.index');
        $langs =[];
        foreach($package->languages as $pck)
        {
            $langs[]=[
                'from_lang'=>$pck->fromLang->name,
                'to_lang'  => $pck->ToLang->name,
                'num_of_words' =>$pck->num_of_words,
                'word_price'  => $pck->word_price
            ];

        }
        $view =view('packages.view_ajax',compact('package'))->render();
        return response()->json(['data'=>$view,'package_id'=>$package->id]);
        // return response()->json([
        //     'package'=>$package,
        //     'word_price' => $package->getWordPrice(),
        //     'discount_amount'=>$package->DiscountAmount(),
        //     'langs'=>$langs
        // ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $package = UserPackages::find($id);
        return view('packages.view',compact('package'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    /** Other Functions */
    public  function  ClientPassword($password , $email)
    {
        $salting = "transpack";
        $hash_email=md5($email);
        $hash_password=md5($password);
        $hash_salting=md5($salting);
        $random_token=$hash_email.$hash_password.$hash_salting;
        $final_password=md5($random_token);
        return $final_password;

    }


    public  function  get_langs(Request $request)
    {
            $language = Language::where('name','LIKE','%'.$request->from_lang.'%')->first();
            $to_langs="";
            if(is_null($language))
            {
                return response()->json(['status'=>0,'data'=>'']);
            }
            $to_languages =PackageLanguages::where('package_id',$request->new_package_id)
                                            ->where('from_language_id',$language->id)
                                            ->with('toLang')
                                            ->get()
                                            ->pluck('toLang.name','id')
                                            ->toArray();

            foreach($to_languages as $key => $value){
                $to_langs = $to_langs . "<option value='$value'>$value</option>";
            }
            $to_langs = "<option value='N/A' class='text-capitalize'>Choose Language</option>" . $to_langs;

            return response()->json(['status'=>1,'data'=>$to_langs]);
    }


    /**
     * REsend Email
     */
    public function resend_email($id)
    {
        $package = UserPackages::find($id);

        Mail::to($package->client->email)->bcc('finance@transnpack.com')->send(new ClientPacakgeMail($package));
        return redirect()->back()->with('message','Mail has been resend to the client');

    }


    /**
     * Confirm Pacakge
     */

    public  function confirmPackage(Request $request)
    {
        $package = UserPackages::find($request->package_id);
        $package->confirmed =1;
        $package->save();

        Mail::to([$package->client->email,\Auth::guard('sales')->user()->mail])->bcc('finance@transnpack.com')->send(new ClientPacakgeMail($package));

        if(\Session::has('pass'))
        {
            $password = \Session::get('pass');
            Mail::to($package->client->email)->send(new ClientPasswordMail($password,$package->client));
            \Session::forget('pass');
        }

        return  redirect()->route('packages.index')->with('message','Package has been created');

    }



}
