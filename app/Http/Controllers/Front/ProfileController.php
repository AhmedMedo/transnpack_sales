<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Service;
use App\Speciality;
use App\SupportedSoftwares;
use App\TranslatorService;
use App\TranslatorServiceDetails;
use Illuminate\Http\Request;
use App\Country;
use App\City;
use App\Language;
use App\Currency;
use App\PerUnit;
use App\Translator;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function showWizard()
    {
        //
        $translator = \Auth::guard('translator')->user();
        $countries = Country::all();
        $languages = Language::all();
        $services = Service::all();
        $currencies = Currency::all();
        $per_unit = PerUnit::all();
        $softwares =SupportedSoftwares::all();
        $specialities = Speciality::all();
        return view('front.profile',compact('translator','specialities','countries','languages','services','softwares','currencies','per_unit'));
    }



    /**
     * Load Country Cities By Ajax Request
     */

    public function get_cities(Request $request)
    {
        $countries = $request->country;
        // dd($countries);
        if($countries){
            $cities = City::where('country_id',$countries)->select('id','name')->get()->toArray();
        }else{
            return response()->json(['status'=>0,'data'=>[]]);
        }
        return response()->json(['status'=>1,'data'=>$cities]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function save_data(Request $request)
    {
//        dd($request->services);
        //
        $translator = \Auth::guard('translator')->user();

        //First Step Update
        $translator->first_address = $request->address;
        $translator->second_address = $request->address_2;
        $translator->country_id = $request->country_id;
        $translator->city_id = $request->city_id;
        $translator->zip_code = $request->postal_code;
        $translator->timezone = $request->timezone;
        $translator->skype_id = $request->skype_id;
        $translator->phone = $request->phone_number;
        $translator->whatsapp_number = $request->whats_app;
        $translator->bio = $request->bio;
        $translator->save();


        //Add Languages
        if($request->languages)
        {
            $translator->languages()->sync($request->languages);
        }

        if($request->services)
        {
            foreach ($request->services as $service)
            {
                $translation_service = new TranslatorServiceDetails();
                $translation_service->translator_id = $translator->id;
                $translation_service->service_id = $service['id'];
                $translation_service->languages = json_encode($service['languages']);
                if(array_key_exists('is_sworn',$service))
                {
                    $translation_service->is_swarn_translator = 1;

                }
                $translation_service->currency_id = $service['currency_id'];
                $translation_service->per_unit_id = $service['per_unit_id'];
                $translation_service->rate= $service['rate'];
                switch ($service['id']) {
                    case "1": // Translation
                        break;
                    case "2": //Proofreading
                        break;
                    case "3": // Media Transcription
                        if(array_key_exists('support_transcription_tools',$service))
                        {
                            $translation_service->support_transcription_tools = 1;

                        }
                         if(array_key_exists('softwares',$service))
                         {
                             $translation_service->transcription_supported_softwares = json_encode($service['softwares']);
                         }
                        break;
                    case "4": // Media Subtitling
                        if(array_key_exists('support_subtitiling_tools',$service))
                        {
                            $translation_service->support_subtitling_tools = 1;

                        }
                        if(array_key_exists('softwares',$service))
                        {
                            $translation_service->subtitling_supported_softwares = json_encode($service['softwares']);
                        }
                        break;
                    case "5": // MTPE
                        break;
                    case "6": //DTP
                        if(array_key_exists('support_dtp_tools',$service))
                        {
                            $translation_service->support_dtp_tools = 1;

                        }
                        if(array_key_exists('softwares',$service))
                        {
                            $translation_service->dtp_supported_softwares = json_encode($service['softwares']);
                        }
                        break;
                    case "7": //Editing

                        if(array_key_exists('provide_lqa',$service))
                        {
                            $translation_service->provide_lqa = 1;

                        }
                        break;
                    case "8": //Interpreting
                        if(array_key_exists('available_for_face_to_face_interpretation',$service))
                        {
                            $translation_service->available_for_face_to_face_interpreting = 1;

                        }

                        if(array_key_exists('willing_to_travel',$service))
                        {
                            $translation_service->willing_to_travel = 1;

                        }

                        if(array_key_exists('available_for_video_interpreting',$service))
                        {
                            $translation_service->available_for_video_interpreting = 1;

                        }
                        if(array_key_exists('available_for_phone_interpreting',$service))
                        {
                            $translation_service->available_for_phone_interpreting = 1;

                        }
                        if(array_key_exists('can_support_asl_interpretation',$service))
                        {
                            $translation_service->can_support_asl_interpretation = 1;

                        }

                        if(array_key_exists('support_bsl_interpreting_services',$service))
                        {
                            $translation_service->can_support_bsl_interpretation = 1;

                        }
                        if(array_key_exists('provide_text_translation',$service))
                        {
                            $translation_service->text_translation_during_meetings = 1;

                        }
                        if(array_key_exists('provide_meeting_minutes',$service))
                        {
                            $translation_service->provide_meeting_minutes_during_meetings = 1;

                        }
                        if(array_key_exists('provide_meeting_minutes',$service))
                        {
                            $translation_service->provide_meeting_minutes_during_meetings = 1;

                        }
                        break;
                    case "9": // Typing

                        if(array_key_exists('support_certain_typing_softwares',$service))
                        {
                            $translation_service->support_typing_sofwares = 1;

                        }
                        $translation_service->typing_supported_softwares = $service['softwares'];
                        break;
                    default:
                        echo "Your favorite color is neither red, blue, nor green!";
                }

                $translation_service->save();

            }
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function logout()
    {
        auth()->guard('translator')->logout();
        return redirect()->route('home');
    }
}
