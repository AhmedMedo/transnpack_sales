<?php

namespace App\Http\Controllers\Front\Auth;

use App\Http\Controllers\Controller;
use App\Mail\ConfirmationMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Client;
use App\Translator;
use Illuminate\Support\Str;

class RegisterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        //
        if(in_array($request->user_type,['client_company','client_person']))
        {
            $email_rules = ['required', 'email', 'unique:users'];
        }else{

            $email_rules = ['required', 'email', 'unique:translators'];

        }

        $userData = array(
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'password' => $request->password,
            'password_confirmation' => $request->password_confirmation,
        );
        $rules = array(
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => $email_rules,
            'password' => 'min:6|required_with:password_confirmation|same:password_confirmation',
            'password_confirmation' => 'min:6'
        );

        $messages = [];

        $validator = Validator::make($userData, $rules, $messages);


        if ($validator->fails()) {
            return \Response::json(array(
                'fail' => true,
                'errors' => $validator->getMessageBag()->toArray()
            ));
        } else {

            if(in_array($request->user_type,['client_company','client_person']))
            {
                //Add New User
                Client::create([
                    'fullname' => $request->first_name .' '.$request->last_name,
                    'email'    => $request->email,
                    'password' => $this->ClientPassword($request->password , $request->email),
                    'stripe_id'=>'',
                    'avatar' => '',
                    'activation_token'=>'',
                    'activate_date'=>'',
                    'created_date'=>\Carbon\Carbon::now()->toDateTimeString(),
                    'type'=>($request->user_type == 'client_person') ? 'user':'company',
                    'company_name'=>$request->company_name,
                    'company_address'=>$request->company_address,
                    'company_website'=>$request->company_website,
                    'phone_number'=>$request->phone_number
                ]);

                return  response()->json([
                    'status'=>1,
                    'url'=>route('home')
                ]);

            }else{

                //Add New Translator
                $translator=  Translator::create([
                    'first_name'     =>    $request->first_name,
                    'last_name'      =>    $request->last_name,
                    'email'          =>    $request->email,
                    'password'       =>    \Hash::make('12345678'),
                    'token'          =>    Str::random(40),
                    'company_name'   =>    $request->company_name,
                    'company_address'=>    $request->company_address,
                    'company_website'=>    $request->company_website,
                    'phone_number'   =>    $request->phone_number

                ]);

                //Send Confirmation Email
//                \Mail::to($translator->email)->send(new ConfirmationMail($translator));

                \Auth::guard('translator')->login($translator);
                return  response()->json([
                    'status' => 1,
                    'url'    => route('profile.wizard')
                ]);

            }

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function verifiyToken($token)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.

     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }




}
