<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    //
    public  $table='languages';
    protected  $fillable  = ['name'];

    public  function  groups()
    {
        return $this->belongsToMany('App\Group','groups_languages','language_id','group_id');
    }

    public function ToLanguages()
	{
	  return $this->belongsToMany('App\Language', 'package_languages','from_language_id', 'to_language_id');
	}

	// Same table, self referencing, but change the key order
	public function FromLanguages()
	{
	  return $this->belongsToMany('App\Language', 'package_languages', 'to_language_id', 'to_language_id');
	}

}
