<?php

namespace App;

use App\AccountClientAgent;
use Illuminate\Database\Eloquent\Model;

class AccountClient extends Model
{
    //

    public function agents()
    {
        return $this->hasMany(AccountClientAgent::class,'client_id');
    }

    public function emails()
    {
        return $this->hasMany(AccountClientEmail::class,'client_id');
    }

    public function phones()
    {
        return $this->hasMany(AccountClientPhone::class,'client_id');
    }

    public function faxes()
    {
        return $this->hasMany(AccountClientFax::class,'client_id');
    }
}
