<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EnglishGroupPricing extends Model
{
    //
    public  $table='english_group_pricings';

    protected  $fillable = ['group_id','general_domain','technical_domain','medical_domain'];
}
