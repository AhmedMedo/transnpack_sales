<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ClientPasswordMail extends Mailable
{
    use Queueable, SerializesModels;

    public $password;
    public $user;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($password,$user)
    {
        //
        $this->password = $password;
        $this->user = $user;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $password = $this->password;
        $user = $this->user;
        return $this->subject('Dashboard Password')->view('email.password',compact('password','user'));
    }
}
