<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ClientPacakgeMail extends Mailable
{
    use Queueable, SerializesModels;

    public $package;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($package)
    {
        //
        $this->package = $package;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $package = $this->package;
        if($package->is_complementary)
        {
            return $this->subject('Activate your TransnPack Package')->view('email.complementary', compact('package'));

        }else{
            return $this->subject('Activate your TransnPack Package')->view('email.client', compact('package'));

        }
        
    }
}
