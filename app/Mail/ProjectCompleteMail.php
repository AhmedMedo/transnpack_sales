<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ProjectCompleteMail extends Mailable
{
    use Queueable, SerializesModels;


    public $project;
    public $percentage;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($project , $percentage)
    {
        //
        $this->project = $project;
        $this->percentage = $percentage;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $project = $this->project;
        $percentage = $this->percentage;

        return $this->view('operations.emails.complete_email',compact('project','percentage'));
    }
}
