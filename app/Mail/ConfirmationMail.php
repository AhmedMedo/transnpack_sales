<?php

namespace App\Mail;

use App\Translator;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Queue\SerializesModels;

class ConfirmationMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var \App\Translator
     */
    public $translator;


    /**
     * Create a new message instance.
     *
     * @param Translator $translator
     */
    public function __construct(Translator $translator)
    {
        $this->translator = $translator;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $message = (new MailMessage)
            ->greeting($this->translator->first_name)
            ->line('Welcome')
            ->action('confirm', route('confirm.email',['token'=>$this->translator->token]));

        return $this->markdown('vendor.notifications.email', $message->data())
            ->subject('Confirm');
    }
}
