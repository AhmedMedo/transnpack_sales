<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    //
    public  $table = 'users';
    protected  $fillable = ['stripe_id','avatar','password','activation_token','fullname','email','created_date','activate_date','type','company_name','company_website','company_address','phone_number'];
    public $timestamps = false;

    public function getFullnameAttribute($value)
    {
        return ucwords($value);

    }






}
