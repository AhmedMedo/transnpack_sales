<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPackages extends Model
{
    //
    protected  $fillable  = ['name','package_ID','confirmed','package_token','is_complementary','total_num_of_words','duration','field_of_text','price','price_before_discount','discount_percentage','client_id','sales_id','package_type','package_payment_link'];

    public function getPackageIDAttribute($value)
    {
        return strtoupper($value);

    }

    public function setPackageIDAttribute($value)
    {
        $this->attributes['package_ID'] = strtoupper($value);
    }

    public function getPriceBeforeDiscountAttribute($value)
    {
        return round($value,2);

    }

    public function getPriceAttribute($value)
    {
        return round($value,2);

    }
    
    public  function  languages()
    {
        return $this->hasMany('App\PackageLanguages','package_id');
    }

    public  function type()
    {
        return $this->belongsTo('App\PackageType','package_type');
    }

    public function client()
    {
        return $this->belongsTo('App\Client','client_id');
    }

    public function Fromlanguages()
    {
        return $this->belongsToMany('App\Language','package_languages','package_id','from_language_id')->distinct();
    }

    public function Tolanguages()
    {
        return $this->belongsToMany('App\Language','package_languages','package_id','to_language_id')->distinct();
    }

    public function getWordPrice()
    {
        return round($this->price/$this->total_num_of_words,3);
    }

    public  function  DiscountAmount()
    {
        $discount = 0;
        if($this->discount_percentage > 0) {
            $discount = $this->price_before_discount - $this->price;
        }

        return number_format($discount,2);

    }




}
