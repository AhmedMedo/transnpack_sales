<?php
namespace App\Services;
use App\Language;
use App\Group;
use App\GroupToGroupPricing;
use App\EnglishGroupPricing;
use kcfinder\dir;

Class PricingService
{

    public   function CalculatePrice($request)
    {
        //Check if one of the language is english
//            dd($request->lang);



        $price = 0;
        $total_words = 0;
        $price_before_discount = 0;
        foreach ($request->lang as $lang)
        {
            if($lang['from_lang_id'] == 1)
            {
                $group_id = $this->GetLangGroup($lang['to_lang_id']);

                $domain = $this->GetEnglishDomainValue($group_id , $request->domain_type);

            }elseif ($lang['to_lang_id'] == 1)
            {

                $group_id = $this->GetLangGroup($lang['from_lang_id']);
                $domain = $this->GetEnglishDomainValue($group_id , $request->domain_type);

            }else{

                $domain = $this->GetGroupToGroupDomainValue($lang['from_lang_id'], $lang['to_lang_id'] , $request->domain_type);

            }

            $price = $price + ($domain * $lang['num_of_words'] );
            $total_words = $total_words + $lang['num_of_words'];
        }

            if($request->package_type == 2)
            {
                $price = $price/2;
            }

           if($request->package_type == 3)
           {
               $price = $price + ($price/2);
           }

            if($request->discount != 0 || $request->discount != NULL)
            {
                $price_before_discount = $price;
                $discount_percentage =100 - $request->discount;
                $price = $price * ($discount_percentage/100);
            }

        return ['price' =>  ($request->has('is_complementary')) ? 0: round($price,2) ,
            'total_words' => $total_words,
            'price_before_discount' =>round($price_before_discount,2)
        ];
    }

    private function GetEnglishDomainValue($group_id , $domain_type)
    {
        $domain = EnglishGroupPricing::where('group_id',$group_id)->first();
        if(!is_null($domain))
        {
            return $domain->$domain_type;
        }
        return 0;
    }

    private function GetGroupToGroupDomainValue($from_lang_id , $to_lang_id ,$domain_type)
    {
        $from_group_id = $this->GetLangGroup($from_lang_id);
        $to_group_id   = $this->GetLangGroup($to_lang_id);
        $domain = GroupToGroupPricing::where('from_group_id',$from_group_id)
                                      ->where('to_group_id',$to_group_id)
                                      ->first();
        if(!is_null($domain))
        {
            return $domain->$domain_type;

        }
        return 0;
    }

    private function GetLangGroup($lang_id)
    {
        $lang = Language::find($lang_id);
        if($lang)
        {
            $group_id = $lang->groups()->first()->id;
        }else{
            $group_id = 0;
        }

        return $group_id;


    }

    public function GetLanguageDomain($from_lang_id , $to_lang_id,$domain_type)
    {
        if($from_lang_id == 1)
            {
                $group_id = $this->GetLangGroup($to_lang_id);

                $domain = $this->GetEnglishDomainValue($group_id , $domain_type);

            }elseif ($to_lang_id == 1)
            {

                $group_id = $this->GetLangGroup($from_lang_id);
                $domain = $this->GetEnglishDomainValue($group_id , $domain_type);

            }else{

                $domain = $this->GetGroupToGroupDomainValue($from_lang_id, $to_lang_id , $domain_type);

            }

            return number_format($domain,3);
    }




}

