<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Sales extends Authenticatable
{
    //
    protected $connection = 'mysql2';
    public  $table ='mirrormx_customer_chat_user';
    public $timestamps = false;

    /**
     * @param $value
     */
    public  function  setPasswordAttribute($value)
    {
        $this->attributes['password'] = sha1(md5($value));
    }

}
