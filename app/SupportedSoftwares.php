<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SupportedSoftwares extends Model
{
    //
    protected $fillable=['name'];
}
