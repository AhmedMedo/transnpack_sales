<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackageLanguages extends Model
{
    //
    protected  $fillable =['package_id','from_language_id','to_language_id','num_of_words','word_price'];

    public  function fromLang()
    {
        return $this->belongsTo('App\Language' ,'from_language_id');
    }

    public  function toLang()
    {
        return $this->belongsTo('App\Language','to_language_id');
    }

    public function getWordPriceAttribute($value)
    {
        return number_format($value,3);

    }
}
