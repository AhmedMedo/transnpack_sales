<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupToGroupPricing extends Model
{
    //
    public  $table='group_to_group_pricings';

    protected  $fillable = ['from_group_id','to_group_id','general_domain','technical_domain','medical_domain'];

}
