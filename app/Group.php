<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Language;

class Group extends Model
{
    //
    protected  $fillable=['name'];

    public  function  languages()
    {
        return $this->belongsToMany('App\Language','groups_languages','group_id','language_id');
    }

    public  function attachByName(array $languages)
    {
        foreach ($languages as $language)
        {
            $find = Language::whereName($language)->first();
            if(!is_null($find))
            {
                if(!in_array($find->id,$this->languages()->pluck('language_id')->toArray()))
                {
                    $this->languages()->attach([$find->id]);
                }
            }
        }


    }

}
