<?php

use Illuminate\Database\Seeder;
use App\PackageType;
class PackageTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        PackageType::insert([
            ['name' => 'Translation'],
            ['name' => 'Proofreading'],
            ['name' => 'TEP'],
        ]);
    }
}
