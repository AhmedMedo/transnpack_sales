<?php

use Illuminate\Database\Seeder;
use App\Manager;
class ManagerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $managers=[
            [
                'name' =>'Hany Roshdy',
                'email' =>'hany@qtrans.net',
                'password'  =>\Hash::make('12345678'),
                'role'       =>'ceo'
            ],
            [
                'name' =>'Nermeen Yusuf',
                'email' =>'nermeen.yusuf@qtrans.net',
                'password'  =>\Hash::make('12345678'),
                'role'       =>'pm'
            ],
            [
                'name' =>'Ahmed Alaa',
                'email' =>'ahmed.alaa@zone1.ca',
                'password'  =>\Hash::make('12345678'),
                'role'       =>'translator'
            ]
        ];
        foreach($managers as $manager)
        {
            Manager::firstOrCreate($manager);

        }

    }
}
