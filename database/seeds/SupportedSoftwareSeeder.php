<?php

use Illuminate\Database\Seeder;
use App\SupportedSoftwares;
class SupportedSoftwareSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        SupportedSoftwares::firstOrCreate(['name' =>'Trados (v. 2019 / 2017 / 2015/ older version
']);
        SupportedSoftwares::firstOrCreate(['name' =>'SDL TMS']);
        SupportedSoftwares::firstOrCreate(['name' =>'XTM']);
        SupportedSoftwares::firstOrCreate(['name' =>'MemoQ']);
        SupportedSoftwares::firstOrCreate(['name' =>'Catalyst']);
        SupportedSoftwares::firstOrCreate(['name' =>'Passolo']);
        SupportedSoftwares::firstOrCreate(['name' =>'Google translator']);
        SupportedSoftwares::firstOrCreate(['name' =>'Xbench']);
        SupportedSoftwares::firstOrCreate(['name' =>'Memsource']);
        SupportedSoftwares::firstOrCreate(['name' =>'Across']);
        SupportedSoftwares::firstOrCreate(['name' =>'Transfix']);
        SupportedSoftwares::firstOrCreate(['name' =>'Matecat']);


    }
}
