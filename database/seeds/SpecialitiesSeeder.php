<?php

use Illuminate\Database\Seeder;
use App\Speciality;

class SpecialitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $specialities = 'Aerospace,Agriculture,Archaeology,Arms,Automotive,Broadcasting,Business,Chemical industry,Computer and information technology,Construction,Data Processing,Defense,Development,Ecology,Education,Electrical power,Electronics,Energy,Engineering,Entertainment,Film,Financial services,Fishing,Food and beverage,Fruit production,Health care,History,Hospitality,Information systems,Insurance,Internet,Jornalism,Legal,Life Sciences,Manufacturing,Marketing,Mass media,Mining,Music,News media,NGOs,Petroleum,Pharmaceutical,Politics,Programming,Publishing,Pulp and paper,Security,Service sector,Shipbuilding,Software,Software Engineering,Steel industry,Systems,Telecommunications,Tobacco,Transport,Travel,World Wide Web,Gaming';
        $specialities_array = explode(',',$specialities);
        foreach ($specialities_array as $speciality)
        {
            Speciality::firstOrCreate(['name' =>$speciality]);
        }
    }
}
