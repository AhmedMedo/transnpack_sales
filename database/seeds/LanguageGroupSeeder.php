<?php

use Illuminate\Database\Seeder;
use App\Language;
use App\Group;

class LanguageGroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $languages =['English','German', 'Italian', 'French', 'Spanish', 'Portuguese' , 'Arabic', 'Farsi', 'Hebrew', 'Hindi', 'Indonesian',
            'Japanese', 'Korean', 'Simplified Chinese', 'Traditional Chinese', 'Thai', 'Turkish', 'Vietnamese', 'Tagalog' ,
            'Croatian', 'Czech', 'Dutch', 'Danish', 'Greek', 'Hungarian', 'Norwegian', 'Romanian', 'Polish', 'Russian', 'Ukrainian', 'Slovene', 'Finnish', 'Swedish', 'Icelandic', 'Estonian',
            'Latvian', 'Lithuanian', 'Polish', 'Slovak', 'Albanian', 'Bosnian', 'Bulgarian', 'Catalan', 'Serbian', 'Armenian', 'French (CA)',
            'Afrikaans', 'Amharic', 'Bengali', 'Bulgarian', 'Burmese', 'Cambodian (Khmer)', 'Cantonese', 'Dari', 'Filipino', 'Gujarati', 'Haitian Creole', 'Hausa',
            'Hmong', 'Kannada', 'Karen', 'Kurdish', 'Lao', 'Lingala', 'Luo', 'Malay', 'Marathi', 'Marshallese', 'Mongolian', 'Pashto', 'Punjabi', 'Sinhalese', 'Swahili', 'Tamil', 'Urdu'];
        $groups = ['Group 1' , 'Group 2' , 'Group 3' , 'Group 4'];

        foreach ($languages as $language)
        {
            Language::FirstOrCreate(['name' => $language]);

        }

        foreach ($groups as $group)
        {
            Group::FirstOrCreate(['name' => $group]);
        }

        Group::find(1)->attachByName(['German', 'Italian', 'French', 'Spanish','Portuguese']);
        Group::find(2)->attachByName(['Arabic', 'Farsi', 'Hebrew', 'Hindi', 'Indonesian','Japanese', 'Korean', 'Simplified Chinese', 'Traditional Chinese', 'Thai', 'Turkish', 'Vietnamese', 'Tagalog']);
        Group::find(3)->attachByName(['Croatian', 'Czech', 'Dutch', 'Danish', 'Greek', 'Hungarian', 'Norwegian', 'Romanian', 'Polish', 'Russian', 'Ukrainian', 'Slovene', 'Finnish', 'Swedish', 'Icelandic', 'Estonian',
            'Latvian', 'Lithuanian', 'Polish', 'Slovak', 'Albanian', 'Bosnian', 'Bulgarian', 'Catalan', 'Serbian', 'Armenian', 'French (CA)']);
        Group::find(4)->attachByName(['Afrikaans', 'Amharic', 'Bengali', 'Bulgarian', 'Burmese', 'Cambodian (Khmer)', 'Cantonese', 'Dari', 'Filipino', 'Gujarati', 'Haitian Creole', 'Hausa',
            'Hmong', 'Kannada', 'Karen', 'Kurdish', 'Lao', 'Lingala', 'Luo', 'Malay', 'Marathi', 'Marshallese', 'Mongolian', 'Pashto', 'Punjabi', 'Sinhalese', 'Swahili', 'Tamil', 'Urdu']);
    }
}
