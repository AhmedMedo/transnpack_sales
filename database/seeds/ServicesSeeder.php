<?php

use Illuminate\Database\Seeder;
use App\Service;

class ServicesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $services = [
            ['name' => 'Translation', 'slug_name' => 'translation'],
            ['name' => 'Proofreading', 'slug_name' => 'proofreading'],
            ['name' => 'Media Transcription', 'slug_name' => 'media_transcription'],
            ['name' => 'Media Subtitling', 'slug_name' => 'media_subtitling'],
            ['name' => 'MTPE (Machine Translation Post Editing)', 'slug_name' => 'MTPE'],
            ['name' => 'DTP (Desktop Publishing)', 'slug_name' => 'DTP'],
            ['name' => 'Editing', 'slug_name' => 'editing'],
            ['name'=>'Interpreting' , 'slug_name' => 'interpreting'],
            ['name'=>'Typing','slug_name'=>'typing']
        ];
        foreach ($services as $service) {
            Service::firstOrCreate($service);
        }
    }
}
