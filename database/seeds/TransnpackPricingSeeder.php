<?php

use Illuminate\Database\Seeder;
use App\EnglishGroupPricing;
use App\GroupToGroupPricing;

class TransnpackPricingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //EnglishGroupPricing
        EnglishGroupPricing::FirstOrCreate(['group_id' =>1,'general_domain' =>'0.15','technical_domain' =>'0.18','medical_domain'=>'0.22']);
        EnglishGroupPricing::FirstOrCreate(['group_id' =>2,'general_domain' =>'0.18','technical_domain' =>'0.21','medical_domain'=>'0.25']);
        EnglishGroupPricing::FirstOrCreate(['group_id' =>3,'general_domain' =>'0.2','technical_domain' =>'0.23','medical_domain'=>'0.27']);
        EnglishGroupPricing::FirstOrCreate(['group_id' =>4,'general_domain' =>'0.25','technical_domain' =>'0.28','medical_domain'=>'0.32']);

        //GroupToGroupPricing
        GroupToGroupPricing::FirstOrCreate(['from_group_id' =>1 , 'to_group_id'=>1,'general_domain' =>'0.15','technical_domain' =>'0.18','medical_domain'=>'0.22']);
        GroupToGroupPricing::FirstOrCreate(['from_group_id' =>1 , 'to_group_id'=>2,'general_domain' =>'0.25','technical_domain' =>'0.28','medical_domain'=>'0.32']);
        GroupToGroupPricing::FirstOrCreate(['from_group_id' =>1 , 'to_group_id'=>3,'general_domain' =>'0.25','technical_domain' =>'0.28','medical_domain'=>'0.32']);
        GroupToGroupPricing::FirstOrCreate(['from_group_id' =>1 , 'to_group_id'=>4,'general_domain' =>'0.3','technical_domain' =>'0.33','medical_domain'=>'0.37']);

        GroupToGroupPricing::FirstOrCreate(['from_group_id' =>2 , 'to_group_id'=>1,'general_domain' =>'0.25','technical_domain' =>'0.28','medical_domain'=>'0.32']);
        GroupToGroupPricing::FirstOrCreate(['from_group_id' =>2 , 'to_group_id'=>2,'general_domain' =>'0.25','technical_domain' =>'0.28','medical_domain'=>'0.32']);
        GroupToGroupPricing::FirstOrCreate(['from_group_id' =>2 , 'to_group_id'=>3,'general_domain' =>'0.25','technical_domain' =>'0.28','medical_domain'=>'0.32']);
        GroupToGroupPricing::FirstOrCreate(['from_group_id' =>2 , 'to_group_id'=>4,'general_domain' =>'0.3','technical_domain' =>'0.33','medical_domain'=>'0.37']);

        GroupToGroupPricing::FirstOrCreate(['from_group_id' =>3 , 'to_group_id'=>1,'general_domain' =>'0.25','technical_domain' =>'0.28','medical_domain'=>'0.32']);
        GroupToGroupPricing::FirstOrCreate(['from_group_id' =>3 , 'to_group_id'=>2,'general_domain' =>'0.25','technical_domain' =>'0.28','medical_domain'=>'0.32']);
        GroupToGroupPricing::FirstOrCreate(['from_group_id' =>3 , 'to_group_id'=>3,'general_domain' =>'0.25','technical_domain' =>'0.28','medical_domain'=>'0.32']);
        GroupToGroupPricing::FirstOrCreate(['from_group_id' =>3 , 'to_group_id'=>4,'general_domain' =>'0.3','technical_domain' =>'0.33','medical_domain'=>'0.37']);

        GroupToGroupPricing::FirstOrCreate(['from_group_id' =>4 , 'to_group_id'=>1,'general_domain' =>'0.25','technical_domain' =>'0.28','medical_domain'=>'0.32']);
        GroupToGroupPricing::FirstOrCreate(['from_group_id' =>4 , 'to_group_id'=>2,'general_domain' =>'0.25','technical_domain' =>'0.28','medical_domain'=>'0.32']);
        GroupToGroupPricing::FirstOrCreate(['from_group_id' =>4 , 'to_group_id'=>3,'general_domain' =>'0.25','technical_domain' =>'0.28','medical_domain'=>'0.32']);
        GroupToGroupPricing::FirstOrCreate(['from_group_id' =>4 , 'to_group_id'=>4,'general_domain' =>'0.3','technical_domain' =>'0.33','medical_domain'=>'0.37']);




    }
}
