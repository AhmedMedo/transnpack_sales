<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_requests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('projectID')->nullable();
            $table->integer('service_id')->nullable();
            $table->boolean('is_certification_required')->default(0);
            $table->boolean('is_cat_tool_required')->default(0);
            $table->boolean('is_retyping_required')->default(0);
            $table->boolean('is_dtp_required')->default(0);
            $table->boolean('is_formating_required')->default(0);
            $table->string('software_name')->nullable();
            $table->string('number_of_pages')->nullable();
            $table->string('dtp_range')->nullable();
            $table->string('count')->nullable();
            $table->string('count_by')->nullable();
            $table->string('count_at')->nullable();
            $table->string('client_rate')->nullable();
            $table->string('client_deadline')->nullable();
            $table->string('timezone')->nullable();
            $table->string('request_total')->nullable();
            $table->integer('client_id')->nullable();
            $table->integer('agent_id')->nullable();
            $table->integer('from_lang_id')->nullable();
            $table->integer('to_lang_id')->nullable();
            $table->string('status')->nullable();
            $table->string('number_of_files')->nullable();
            $table->string('assinged_to')->nullable();
            $table->longText('client_guidelines')->nullable();
            $table->integer('assigned_by')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_requests');
    }
}
