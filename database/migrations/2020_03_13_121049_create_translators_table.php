<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTranslatorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('translators', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email');
            $table->string('password');
            $table->boolean('is_active')->default(0);
            $table->enum('type',['freelancer','pm','agency'])->default('freelancer');
            $table->string('first_address')->nullable();
            $table->string('second_address')->nullable();
            $table->string('company_website')->nullable();
            $table->string('company_name')->nullable();
            $table->string('company_address')->nullable();
            $table->integer('country_id')->nullable();
            $table->integer('city_id')->nullable();
            $table->integer('state_id')->nullable();
            $table->string('timezone')->nullable();
            $table->string( 'state')->nullable();
            $table->string('zip_code')->nullable();
            $table->string('international_code')->nullable();
            $table->string('phone')->nullable();
            $table->string('skype_id')->nullable();
            $table->string('whatsapp_number')->nullable();
            $table->integer('field_of_specialty')->nullable();
            $table->boolean('available_for_rush_request')->default(0);
            $table->boolean('can_accept_work_at_weekends')->default(0);
            $table->string('cv_path')->nullable();
            $table->string('cover_letter_path')->nullable();
            $table->boolean('acknowledgement_confirmation')->default(0);
            $table->timestamp('email_verified_at')->nullable();
            $table->text('social_token')->nullable();
            $table->enum('social_provider',['facebook','google'])->nullable();
            $table->timestamp('last_login_at')->nullable();
            $table->boolean('status')->default(0);
            $table->boolean('accept_paypal')->default(0);
            $table->boolean('accept_bank_transfer')->default(0);
            $table->boolean('accept_volunteer_work')->default(0);
            $table->longText('bio')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('translators');
    }
}
