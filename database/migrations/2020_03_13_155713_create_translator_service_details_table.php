<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTranslatorServiceDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('translator_service_details', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->integer('translator_id')->nullable();
            $table->integer('service_id')->nullable();
            $table->text('languages_pairs')->nullable()->comment('json for source and target languages');
            $table->text('languages')->nullable()->comment('used for languages in media transcription and typing');
            $table->integer('currency_id')->nullable();
            $table->integer('per_unit_id')->nullable();
            $table->string('rate')->nullable();
            $table->boolean('available_for_face_interpretation')->nullable();
            $table->boolean('willing_to_travel')->default(0);
            $table->boolean('support_transcription_tools')->default(0);
            $table->boolean('support_subtitling_tools')->default(0);
            $table->boolean('support_dtp_tools')->default(0);
            $table->boolean('provide_lqa')->default(0);
            $table->boolean('available_for_face_to_face_interpreting')->default(0);
            $table->boolean('available_for_video_interpreting')->default(0);
            $table->boolean('available_for_phone_interpreting')->default(0);
            $table->boolean('can_support_asl_interpretation')->default(0);
            $table->boolean('can_support_bsl_interpretation')->default(0);
            $table->boolean('text_translation_during_meetings')->default(0);
            $table->boolean('provide_meeting_minutes_during_meetings')->default(0);
            $table->boolean('is_rate_negotiable')->default(0);
            $table->boolean('is_swarn_translator')->default(0);
            $table->boolean('can_support_cat_tool')->default(0);
            $table->boolean('can_support_certain_typing_software')->default(0);
            $table->text('transcription_supported_softwares')->nullable();
            $table->text('subtitling_supported_softwares')->nullable();
            $table->text('dtp_supported_softwares')->nullable();
            $table->text('subtitling_extension_supported')->nullable();
            $table->text('supported_softwares')->nullable();
            $table->text('other_softwares')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('translator_service_details');
    }
}
