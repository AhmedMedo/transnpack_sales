<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::group(['namespace' =>'Front'],function (){

    Route::get('/','HomeController@index')->name('home');
    Route::get('/login','LoginController@showLoginForm')->name('home.login');
    Route::post('register','Auth\RegisterController@register')->name('front.register');
    Route::get('confirm/{token}','Auth\RegisterController@verifiyToken')->name('confirm.email');
    Route::get('/profile','ProfileController@showWizard')->name('profile.wizard');
    Route::get('/logout','ProfileController@logout')->name('logout');
    Route::post('get_cities','ProfileController@get_cities')->name('get_cities');
    Route::post('save_data','ProfileController@save_data')->name('save_data');

});









//Sales Dashboard Routes
Route::group(['prefix'=>'sales'],function (){

    Route::get('login', 'SalesController@showLoginForm')->name('login');
    Route::get('forget_password', 'SalesController@forget_password')->name('forget_password');
    Route::post('forget_password', 'SalesController@submit_forget_password')->name('forget_password.submit');
    Route::post('login', 'SalesController@login')->name('sales_login');
    Route::get('logout', 'SalesController@logout')->name('sales_logout');
    Route::get('dashboard','DashbaordController@index')->name('dashboard');
    Route::resource('packages','PackagesController');
    Route::post('package_confirm','PackagesController@confirmPackage')->name('packages.confirm');
    Route::get('resend_email/{id}','PackagesController@resend_email')->name('resend_email');
    Route::get('reports', 'ReportController@index')->name('reports');

});

Route::post('get_langs','PackagesController@get_langs');


Route::group(['prefix'=>'operations','namespace'=>'Operations'],function (){
    Route::get('login','LoginController@showLoginForm');
    Route::post('login','LoginController@login')->name('operations.login');


    Route::group(['middleware'=>'IsManager'],function(){

        Route::get('logout','LoginController@logout')->name('operations.logout');
        Route::resource('clients','AccountClientController');

        Route::resource('projects','ProjectRequestController');
        Route::get('reopen_project/{id}','ProjectRequestController@reopen_project')->name('projects.reopen_project');

        Route::get('project_activity/{id}','ProjectRequestController@project_activity')->name('operations.project_activity');
        Route::get('project_compelete/{id}','ProjectRequestController@project_compelete')->name('operations.project_compelete');
        Route::post('submit_project_compelete/{id}','ProjectRequestController@submit_project_compelete')->name('operations.submit_project_compelete');
        Route::post('upload_files','ProjectRequestController@upload_files')->name('operations.upload_files');
        Route::get('delivery_mail/{id}','ProjectRequestController@delivery_mail')->name('operations.delivery_mail');
        Route::post('send_mail/{id}','ProjectRequestController@send_mail')->name('operations.send_mail');

        Route::get('project_comments','ProjectRequestController@ProjectComments')->name('operations.projects.comments');
        Route::get('incoming_requests','ProjectRequestController@incoming_requests')->name('operations.incoming_requests');
        Route::get('show_requests/{id}','ProjectRequestController@show_requests')->name('operations.show_incoming_requests');
        Route::post('project_status/{id}','ProjectRequestController@project_status')->name('operations.project_status');
        Route::post('project_new_deadline/{id}','ProjectRequestController@project_new_deadline')->name('operations.project_new_deadline');
        Route::post('accept_new_deadline/{id}','ProjectRequestController@accept_new_deadline')->name('operations.accept_new_deadline');

        Route::get('outgoing_requests','ProjectRequestController@outgoing_requests')->name('operations.outgoing_requests');

    });

});
