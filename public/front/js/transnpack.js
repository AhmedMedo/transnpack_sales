$(document).ready(function(){
    
    $(window).scroll(function(){
		if ($(this).scrollTop() > 400) {
			$('.scrollToTop').fadeIn(1000);
		} else {
			$('.scrollToTop').fadeOut(1000);
        }
        
	});
	
	//Click event to scroll to top
	$('.scrollToTop').click(function(){
		$('html, body').animate({scrollTop : 0},900);
		return false;
    });
    
    $(".qoute-btns").click(function(){
        var q_btn = $(this).attr("id");

        $(".qoute-btns").removeClass("active");
        $("#" + q_btn).addClass("active");

        $(".qoute-container").fadeOut(300);
        $("." + q_btn).delay(320).fadeIn(300);
    });

    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();
        
            reader.onload = function(e) {
                $('.avatar-large').css({"background":"url("+e.target.result+") center no-repeat", "background-size":"cover"});
                $("#img-sr").val(e.target.result);
            }
        
            reader.readAsDataURL(input.files[0]);
        }
    }
      
    $("#edit-avatar").change(function() {
        readURL(this);
    });
    
    $(".packages-link,  .packagepop-btn").click(function(e){
        //$(".packages-pricing").slideDown(250);
        //$("body").css("overflow-y", "hidden");
        //e.preventDefault();
    });
    
    $(".package-card").click(function(){
        var select_id = $(this).attr("id");
        $(".package-card").removeClass("s-pack");
        $(this).addClass("s-pack");
        $(".select-package").fadeOut(50);
        $("." + select_id).fadeIn(50);

        $("#package-vl").val(select_id);

        $(".extra-buttons").animate({"bottom": "0px"}, 300);   

    });

    

    $(".cancel-package").click(function(){
        location.replace("/");
    });

    $(".packs").click(function(){
        var type_id = $(this).attr("id");
        $(".packs").removeClass("active-lang");
        $(this).addClass("active-lang");

        $(".ty").fadeOut(200);
        setTimeout(function(){
            $("." + type_id).css("display","inline-flex");
        },300);

        

        $("#package-type-vl").val(type_id);
    });

    if($("#pac").length){
        $(".packages-pricing").slideDown(350);
        $("body").css("overflow-y", "hidden");
    }
    
    if($("#upg").length){
        $(".overview, .profile, .payments, .finished-requests").fadeOut(300);
        $(".create-request").delay(350).fadeIn(450);
        $(".pages-btns").removeClass("list-active");
    }

    $("#req-not-dismiss").click(function(){
        $("#req-not-modal").fadeOut(350);
        
        setTimeout(function(){
            $("#req-not-modal").remove();
        },400);
    });
    
    $("#signup-form").submit(function(event){
        var recaptcha = $("#g-recaptcha-response").val();

        if (recaptcha === "") {
            $("html, body").animate({ scrollTop: 0 }, 400);
            event.preventDefault();
            $(".alert").text("*Prove you are not a robot by check the reCaptcha checkbox");
            $(".alert").fadeIn(300);
        }
        else{
            var fname=$("#fullname").val();
            var email=$("#email").val();
            var pass=$("#password").val();
            var cpass=$("#confirm-password").val();
            var approve=$("#customCheck").is(":checked");

            if(fname == ""){
                $("html, body").animate({ scrollTop: 0 }, 400);
                $(".alert").text("*Write your Full name");
                $(".alert").fadeIn(300);
                event.preventDefault();
            }
            else if(email == ""){
                $("html, body").animate({ scrollTop: 0 }, 400);
                $(".alert").text("*Write your Email");
                $(".alert").fadeIn(300);
                event.preventDefault();
            } 
            else if(pass == ""){
                $("html, body").animate({ scrollTop: 0 }, 400);
                $(".alert").text("*Write your Password");
                $(".alert").fadeIn(300);
                event.preventDefault();
            }
            else if(cpass == ""){
                $("html, body").animate({ scrollTop: 0 }, 400);
                $(".alert").text("*Write your Confirm Password");
                $(".alert").fadeIn(300);
                event.preventDefault();
            }
            else if(approve==false){
                $("html, body").animate({ scrollTop: 0 }, 400);
                $(".alert").text("*You need to approve our terms of service");
                $(".alert").fadeIn(300);
                event.preventDefault();
            }
            else if(fname.length < 2){
                $("html, body").animate({ scrollTop: 0 }, 400);
                $(".alert").text("*Full name cannot be less than 2 letters");
                $(".alert").fadeIn(300);
                event.preventDefault();
            }
            else if(pass.length < 8){
                $("html, body").animate({ scrollTop: 0 }, 400);
                $(".alert").text("*Password cannot be less than 8 letters");
                $(".alert").fadeIn(300);
                event.preventDefault();
            }
            else if(pass != cpass){
                $("html, body").animate({ scrollTop: 0 }, 400);
                $(".alert").text("*Password and Confirm Password not matched");
                $(".alert").fadeIn(300);
                event.preventDefault();
            }
        }
    });

    $("#login-form").submit(function(event){
        var email=$("#email").val();
        var pass=$("#password").val();
        
        if(email == ""){
            $(".alert").text("*Write your Email");
            $(".alert").fadeIn(300);
            event.preventDefault();
        } 
        else if(pass == ""){
            $(".alert").text("*Write your Password");
            $(".alert").fadeIn(300);
            event.preventDefault();
        }
        else if(pass.length < 6){
            $(".alert").text("*Password cannot be less than 6 letters");
            $(".alert").fadeIn(300);
            event.preventDefault();
        }
    });

    $("#payment-login-form").submit(function(){
        event.preventDefault();
        var e=$("#email").val();
        var p=$("#password").val();
        
        if(e == ""){
            $(".alert").text("*Write your Email");
            $(".alert").fadeIn(300);
        } 
        else if(p == ""){
            $(".alert").text("*Write your Password");
            $(".alert").fadeIn(300);
        }
        else if(p.length < 6){
            $(".alert").text("*Password cannot be less than 6 letters");
            $(".alert").fadeIn(300);
        }
        else{
            $.post("../inc/check_login_payment.php",{email:e, password:p},function(data){
                if(data == 1){
                    location.replace("payment.php");
                }
                else{
                    $(".alert-danger").text(data).css("display","block");
                }
            });
        }
    });

    $("#reset-form").submit(function(event){
        var email=$("#email").val();

        if(email == ""){
            $(".alert").text("*Write your Email");
            $(".alert").fadeIn(300);
            event.preventDefault();
        }
    });

    /*$("#contactus-form").submit(function(event){
        var recaptcha = $("#g-recaptcha-response").val();
        if (recaptcha === "") {
            event.preventDefault();
            $(".contactus-alert").text("*Prove you are not a robot by check the reCaptcha checkbox");
            $(".contactus-alert").fadeIn(300);
        }
    });*/

    $("#overview, #profile-cancel, #req-can").click(function(){
        $(".profile, .payments, .finished-requests, .create-request").fadeOut(300);
        $(".overview").delay(350).fadeIn(450);
        $(".req-alert").delay(350).hide();
        $("#uploading-process").delay(350).text("");
        $("#req-title").val("");
    });

    $("#profile").click(function(){
        $(".profile").delay(350).fadeIn(450);
    });

    $("#payments").click(function(){
        $(".profile, .overview, .finished-requests, .create-request").fadeOut(300);
        $(".payments").delay(350).fadeIn(450);
    });

    $(".c-projects").click(function(event){
        $(".overview, .profile, .payments, .create-request").fadeOut(300);
        $(".finished-requests").delay(350).fadeIn(450);
        $(".pages-btns").removeClass("list-active");
        event.preventDefault();
    });

    $(".request-btn").click(function(event){
        
        $('html, body').animate({scrollTop : 0},300);


        $.post("../remove.php", {ex:'xx'},function(){
            $("#loadedfiles").text("");
            $("#filesWords").text("-");
            $("#filesDate").text("-");
            $("#overview").addClass("list-active");
            $("#base-w").val("0");
        });

        var reqi=$(this).attr("id");
        $("#pck").val(reqi);

        if(!$(this).hasClass("proof")){
            $(".fileUpload").html("<div class='text-center mx-auto'><i class='fas fa-cloud-upload-alt mr-2'></i>Drag and Drop your files here<br>Or Click to add files</div><input id='u-file' type='file' class='u-files' name='sortpic[]' multiple>");
        }
        else{
            $(".fileUpload").html("<div class='text-center mx-auto'><i class='fas fa-cloud-upload-alt mr-2'></i>Drag and Drop your <strong class='text-uppercase'>source</strong> files here<br>Or Click to add files</div><input id='u-proof-file' type='file' class='u-files' name='sortpic[]' multiple>");
        }

        $.post("../user/get_languages.php", {pck:reqi}, function(data) {
            $("#req-target").html(data.target);
            if(data.source != "")
            {
                $("#soruce_lang").show();
                $("#req-source").removeAttr('disabled');
                $("#req-source").html(data.source);
                $("#new_package_id").val(data.new_package_id);
            }else{
                $("#soruce_lang").hide();
                $("#req-source").attr('disabled','disabled');
                $("#req-source").html('');
                $("#new_package_id").val('0');
                
            }
        });


        $(".profile, .overview, .finished-requests, .payments").fadeOut(300);
        $(".create-request").delay(350).fadeIn(450);
        $(".pages-btns").removeClass("list-active");
        event.preventDefault();
    });
    
    $("#req-source").on('change',function(e){
            $.ajax({
            url: "https://transnpack.com/transnpack_sales/public/get_langs",
            type: "post",
            data: {from_lang:this.value,new_package_id:$("#new_package_id").val()} ,
            success: function (response) {
                if(response.status == 1)
                {
                    $("#req-target").html(response.data);
                    
                }
    
               // You will get response from your PHP page (what you echo or print)
            },
            error: function(jqXHR, textStatus, errorThrown) {
               console.log(textStatus, errorThrown);
            }
        });
        
    });

    $("#profile-cancel").click(function(){
        $(".pages-btns").removeClass("list-active");
        $("#overview").addClass("list-active");
    });

    $(".pages-btns").click(function(){
        if(!$(this).hasClass("list-active")){
            $(".pages-btns").removeClass("list-active");
            $(this).addClass("list-active");
        }
    });

    $(".see-more-btn").click(function(){
        var table = $(this).attr("id");
        var cont = $(this).text();
        if(cont=="Show more"){
            $(this).text("Show less");
        }
        else{
            $(this).text("Show more");
        }

        $("."+table).slideToggle(500);
    });

    $("#all-p").click(function(){
        $(".all-p").fadeIn(400);
    });

    $("#completed-p").click(function(){
        $(".in-progress-p, .pending-p").fadeOut(300);
        $(".completed-p").delay(300).fadeIn(400);
    });
    
    $("#in-progress-p").click(function(){
        $(".completed-p, .pending-p").fadeOut(300);
        $(".in-progress-p").delay(300).fadeIn(400);
    });

    $("#pending-p").click(function(){
        $(".in-progress-p, .completed-p").fadeOut(300);
        $(".pending-p").delay(300).fadeIn(400);
    });

    $(".table-btns").click(function(){
        var tabs=$(this).attr("id");

        if($(this).hasClass("all-p-tab")){
            $(".all-p"+tabs).fadeIn(400);
        }
        else if($(this).hasClass("in-progress-p-tab")){
            $(".all-p"+tabs).fadeOut(300);
            $(".in-progress-p"+tabs).fadeIn(400);
        }
        if($(this).hasClass("completed-p-tab")){
            $(".all-p"+tabs).fadeOut(300);
            $(".completed-p"+tabs).fadeIn(400);
        }
        if($(this).hasClass("pending-p-tab")){
            $(".all-p"+tabs).fadeOut(300);
            $(".pending-p"+tabs).fadeIn(400);
        }

        if(!$(this).hasClass("active")){
            $(".table-tabs"+tabs).removeClass("active");
            $(this).addClass("active");
        }


        
    });
    
    $(".stat-fa").click(function(event){
        $(".modal").fadeOut(300);
        $(".payment-status").delay(350).slideUp(400);
        event.preventDefault();
    });

    

    $(".fileUpload").on("change", "#u-file", function() {
        var loadedFiles = $("#loadedfiles").html();
        
        var file_data = document.getElementById("u-file");
        var files = file_data.files;
        var form_data = new FormData();

        if(files.length < 11){
            
            for (i = 0; i < files.length; i++) {
                form_data.append('file' + i, files[i]);
            }
            
            
            $.ajax({
                xhr: function() {
                    var xhr = new window.XMLHttpRequest();
                
                    xhr.upload.addEventListener("progress", function(evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = evt.loaded / evt.total;
                            percentComplete = parseInt(percentComplete * 100);
                    
                            if (percentComplete === 100) {
                                $("#uploading-process").text("Counting File(s) Words...Please wait");
                            }
                            else{
                                $("#uploading-process").text("Uploading... " + percentComplete + "%");
                            }
                    
                        }
                    }, false);
                
                    return xhr;
                },
                url: '../upload.php',
                dataType: 'text',
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                success: function(res){
                    var res_data=res.split("-_-");
                    var files=res_data[0];
                    //var words=res_data[1];
                    var dates=res_data[2];
                    var docs=res_data[3];
                    var new_words=0;

                    $("#docs").val(docs);
                    $("#uploading-process").text("");
                    $("#loadedfiles").append(files);

                    $(".files_words").each(function(){
                        var each = $(this).val();
                        
                        if(each == ""){
                            each = 0;
                        }
                        
                        new_words = parseInt(new_words)+parseInt(each);
                    });
                    $("#filesDate").text(dates);
                    $("#filesWords").text(new_words);
                    $("#base-w").val(new_words);
                }
            });
        }
        else{
            $("#uploading-process").text("You cannot upload more than 10 files per request");
        }

        $("#u-file").val("");
    });


    $(".fileUpload").on("change", "#u-proof-file", function() {
        var loadedFiles = $("#loadedfiles").html();
        
        var file_data = document.getElementById("u-proof-file");
        var files = file_data.files;
        var form_data = new FormData();

        if(files.length < 11){
            
            for (i = 0; i < files.length; i++) {
                form_data.append('file' + i, files[i]);
            }
            
            $.ajax({
                xhr: function() {
                    var xhr = new window.XMLHttpRequest();
                
                    xhr.upload.addEventListener("progress", function(evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = evt.loaded / evt.total;
                            percentComplete = parseInt(percentComplete * 100);
                    
                            if (percentComplete === 100) {
                                $("#uploading-process").text("Uploading... " + percentComplete + "% ...Please wait");
                            }
                            else{
                                $("#uploading-process").text("Uploading... " + percentComplete + "%");
                            }
                    
                        }
                    }, false);
                
                    return xhr;
                },
                url: '../upload-proof.php',
                dataType: 'text',
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,                         
                type: 'post',
                success: function(res){
                    var res_data=res.split("-_-");
                    var files=res_data[0];
                    //var words=res_data[1];
                    //var dates=res_data[2];
                    //var docs=res_data[3];
                    
                    //var w = $("#base-w").val();
                    
                    //$("#base-w").val(parseInt(w) + parseInt(words));

                    //$("#docs").val(docs);
                    $("#uploading-process").text("");
                    $("#loadedfiles").append(files);
                    //$("#filesWords").text(words);
                    //$("#filesDate").text(dates);
                }
            });
        }
        else{
            $("#uploading-process").text("You cannot upload more than 10 files per request");
        }

        $("#u-proof-file").val("");
    });
    

    $("#loadedfiles").on("change", ".ta-files", function() {
        
        var target_id = $(this).attr("id");
        
        var file_data = document.getElementById(target_id);
        var files = file_data.files;

        var form_data = new FormData();

        form_data.append('file0', files[0]);
        form_data.append('file_id', target_id);

        $.ajax({
            xhr: function() {
                var xhr = new window.XMLHttpRequest();
            
                xhr.upload.addEventListener("progress", function(evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;
                        percentComplete = parseInt(percentComplete * 100);
                
                        if (percentComplete === 100) {
                            $("#uploading-process").text("Counting File(s) Words...Please wait");
                        }
                        else{
                            $("#uploading-process").text("Uploading... " + percentComplete + "%");
                        }
                
                    }
                }, false);
            
                return xhr;
            },
            url: '../upload-target.php',
            dataType: 'text',
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,                         
            type: 'post',
            success: function(res){
                var res_data=res.split("-_-");
                var file_name=res_data[0];
                var words=res_data[1];
                var dates=res_data[2];
                var docs=res_data[3];
                var files_words=res_data[4];
                var hid=res_data[5];
                var new_words = 0;
                

                $("#" + target_id + "-w").html(words);
                $("#"+ hid).val(files_words);

                $(".files_words").each(function(){
                    new_words = parseInt(new_words)+parseInt($(this).val());
                });
                
                $("#filesWords").text(new_words);
                $("#base-w").val(new_words);

                $("#docs").val(docs);

                $("#uploading-process").text("");
                
                $("#" + target_id + "-b").html(file_name).css("background-color","#0d0");
                

                $("#filesDate").text(dates);
            }
        });

        $(this).val("");
    });

    

    
    $("#loadedfiles").on("click", ".rm-files", function(){
        var rmvid = $(this).attr("id");
        //var fwords = $("#"+ rmvid +"-w").val();
        
        $(this).parent().parent().fadeOut(300).remove();

        var words = 0;
        
        $(".files_words").each(function(){
            words = parseInt(words)+parseInt($(this).val());
        });

        $.post("../remove.php", {rm:rmvid}, function(data){
            
            
            $("#base-w").val(words);
            $("#filesWords").text(words);

            
            var r_data=data.split("-_-");
            var dilevery_date=r_data[1];

            if(words == 0){
                $("#filesWords").text("-");
            }
            
            $("#filesDate").text(dilevery_date);
        });
    });




    $("#req-can").click(function(){
        $.post("../remove.php", {ex:'xx'},function(data){
            $("#loadedfiles").text("");
            $("#filesWords").text("-");
            $("#filesDate").text("-");
            $("#overview").addClass("list-active");
            $("#base-w").val("0");
            $(".uploading-process").text("");
        });
    });

    $("#req-sub").click(function(){
        var title = $("#req-title").val();
        var source = $("#req-source").val();
        var target = $("#req-target").val();
        var docs = $("#docs").val();
        var pck = $("#pck").val();
        var d=0;
        var ids=vals="";

        $(".fi-text").each(function(){
            if(ids==""){
                ids = $(this).attr("id");
                vals = $(this).val();
            }
            else{
                ids = ids+","+$(this).attr("id");
                vals = vals+","+$(this).val();
            }
        });
        
        console.log(title);
        if(title != "" && title != null){
            if(target != "N/A"){
                $.post("../request.php", {t:title,source:source,l:target,pc:pck,doc:d,inputs:docs,inputs_ids:ids,inputs_vals:vals}, function(data){
                    if(data!=""){
                        console.log(data!="");
                        $(".req-alert").html(data);
                        $(".req-alert").fadeIn(300);
                    }
                    else{
                        $(".req-alert").fadeOut(300);
                        location.replace("dashboard.php");
                    }
                });
            }
            else{
                $(".req-alert").html("Choose a target language");
                $(".req-alert").fadeIn(300);
            }
        }
        else{
            $(".req-alert").html("Project Title is required");
            $(".req-alert").fadeIn(300);
        }


    });



    $("#loadedfiles").on("keyup", ".fi-text", function(){
        var fi_id = $(this).attr("id");
        var words = 0;
        
        $(".files_words").each(function(){
            var each = $(this).val();
            if(each == ""){
                each = 0;
            }


            words = parseInt(words)+parseInt(each);

        });

        $("#"+fi_id.substring(1)).addClass("btn-warning").removeClass("btn-saved").text("SAVE");
        
        $("#"+fi_id+"-w").val(words);
        $("#filesWords").text(words);
        $("#base-w").val(words);

        
        
        
    });


    $(".circle").click(function(){
        $(".circle-contents").slideToggle(300);
    });

    $("#usrRS").click(function(){
        var email = $("#usrEmail").val();
        if(email!=""){
            $.post("../inc/check_reset.php", {e:email}, function(data){
                $("#form-out").html(data);
                setTimeout(function(){
                    window.location.href = "https://transnpack.com/login.php";
                }, 1000);
            });
        }
        else{
            $("#form-out").html("<div class='alert alert-danger d-block'>*Write your email</div>");
        }
    });



    $("#edit-profile-form").submit(function(e){
        e.preventDefault();

        var formData = new FormData(this);
        
        $.ajax({
            
            url: '../inc/update_profile.php', // point to server-side PHP script 
            dataType: 'text',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: formData,                         
            type: 'post',
            success: function(res){
                if(res==1){
                    
                    var img = $("#img-sr").val();
                    var fn = $("#fullname").val();
                    var em = $("#email").val();
                    
                    $("#old-password").val("");
                    $("#new-password").val("");
                    $("#confirm-password").val("");
                    
                    $(".user-name, #nav-name, .profile-name").text(fn);
                    $("#nav-email").text(em);
                    
                    if(img!=""){
                        $('.avatar-large, .avatar, .circle').css("background", "url("+img+") center no-repeat");
                        $('.avatar-large, .avatar, .circle').css("background-size", "cover");
                    }

                    $("#edit-error").html("<div class='alert alert-success text-center rounded-0 profile-alert mt-3'>Information updated successfully</div>");

                    setTimeout(function(){
                        $("#edit-error").html("");
                    }, 3000);
                }
                else{
                    $("#edit-error").html(res);
                }
            }
        });

    });
    


    $(".c-req").click(function(){
        var reqid = $(this).attr("id");
        $("#r-canreq").val(reqid);
    });

    $(".y-canreq").click(function(){
        $("#canreq").fadeOut(200);
        var req = $("#r-canreq").val();
        $.post("/user/cancel_request.php", {"reqid":req}, function(data){
            if(data==1){
                location.replace("/user/dashboard.php");
            }
        });
    });
    
    $(".n-canreq").click(function(){
        $("#r-canreq").val("");
    });

    $(".refund-package").click(function(){
        var refid = $(this).attr("id");
        $("#r-refpack").val(refid);
    });

    $(".y-refpack").click(function(){
        $("#ref-package").fadeOut(200);
        var ref = $("#r-refpack").val();
        $.post("/user/refund_package.php", {"refid":ref}, function(data){
            if(data==1){
                location.replace("/user/dashboard.php");
            }
        });
    });

    $(".n-refpack").click(function(){
        $("#r-refpack").val("");
    });
    

    $("#loadedfiles").on("click", ".set-words", function(){
        var fileId=$(this).attr("id");
        var fileWords=$("#f"+fileId).val();
        
        $(this).addClass("btn-saved").removeClass("btn-warning").text("SAVED");

        $.post("../save_image.php", {"imgId":fileId, "imgWords":fileWords}, function(data){
            
            if(data!=0){
                $("#filesDate").text(data);
            }
        });
        
    });



    $("body").tooltip({ selector: '[data-toggle=tooltip]' });

    


});

